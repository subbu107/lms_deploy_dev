var express = require('express');
var log = require('../services/log');
var router = express.Router();

var services = require("../services"),
    config = require('../config'),
    jwt = require('express-jwt'),
    jasonWebToken = require("jsonwebtoken");

var jwtCheck = jwt({
    secret: config.secret
});


var secretCallback = function(req, payload, done) {

    var secret = config.secret;

    if (req.headers.clientid.toString() === config.azureApiClientId.toString()) {
        secret = config.azureApiClientSecret;
    } else if (req.headers.clientid.toString() === config.trayAppClientId.toString()) {
        secret = config.trayAppClientSecret;
    }

    done(null, secret);

};

var jwtCheckIncludingHeader = jwt({
    secret: secretCallback,
    credentialsRequired: false,
    getToken: function fromHeaderOrQuerystring(req) {
        log.logger.info("getting token from header");
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
});

router.use('/api/test', jwtCheckIncludingHeader);
router.use('/api/holidaylist', jwtCheckIncludingHeader);
router.use('/api/dashboard', jwtCheck);
router.use('/api/locationList', jwtCheck);
router.use('/api/compOffHistory', jwtCheck);
router.use('/api/leaveHistory', jwtCheck);
router.use('/api/leaveDetails', jwtCheck);
router.use('/api/leaveStatusHistory', jwtCheck);
router.use('/api/employeeLeaveHistory', jwtCheck);
router.use('/api/workFromHomeHistory', jwtCheck);
router.use('/api/recentLeaveHistory', jwtCheck);
router.use('/api/recentHolidaylist', jwtCheck);
router.use('/api/compOffsToApprove', jwtCheck);
router.use('/api/leavesToApprove', jwtCheck);
router.use('/api/workFromHomesToApprove', jwtCheck);
router.use('/api/updateWorkFromHomeStatus', jwtCheck);
router.use('/api/updateLeaveStatus', jwtCheck);
router.use('/api/cancelCompOff', jwtCheck);
router.use('/api/monthlyLeaveCredit', jwtCheck);
router.use('/api/deleteMonthlyLeaveCredit', jwtCheck);
router.use('/api/cancelLeave', jwtCheck);
router.use('/api/cancelWorkFromHome', jwtCheck);
router.use('/api/approveCompOffs', jwtCheck);
router.use('/api/updateCompOffStatus', jwtCheck);
router.use('/api/userListing', jwtCheck);
router.use('/api/user', jwtCheck);
router.use('/api/applyLeave', jwtCheck);
router.use('/api/applyCompOff', jwtCheck);
router.use('/api/leaveRequestReport', jwtCheck);
router.use('/api/leaveSummaryReport', jwtCheck);
router.use('/api/leaveBalanceReport', jwtCheck);
router.use('/api/compOffSummaryReport', jwtCheck);
router.use('/api/newJoineeReport', jwtCheck);
router.use('/api/employeeExitReport', jwtCheck);
router.use('/api/lossOfPayReport', jwtCheck);
router.use('/api/leaveEncashmentReport', jwtCheck);
router.use('/api/compOffExpiryReport', jwtCheck);
router.use('/api/applyCompOffSubmit', jwtCheck);
router.use('/api/dashboardSummaryForUser', jwtCheck);
router.use('/api/validDays', jwtCheck);
router.use('/api/employeeStatusList', jwtCheck);
router.use('/api/addEmployee', jwtCheck);
router.use('/api/updateEmployee', jwtCheck);
router.use('/api/addLeaveCredit', jwtCheck);
router.use('/api/updateLeaveCredit', jwtCheck);
router.use('/api/leaveCredits', jwtCheck);
router.use('/api/leaveBalances', jwtCheck);
router.use('/api/leaveTypeList', jwtCheck);
router.use('/api/leaveCreditTypeList', jwtCheck);
router.use('/api/leaveCredit', jwtCheck);
router.use('/api/compOffSummaryForUser', jwtCheck);
router.use('/api/employeeLeaveAccrualHistory', jwtCheck);
router.use('/api/employeeLeaveUpdatedHistory', jwtCheck);
router.use('/api/getActiveEmployeesEmails', jwtCheck);
router.use('/api/markAttendance', jwtCheck);

router.use('/api/attendance', jwtCheck);
router.use('/api/attendanceStatusName',jwtCheck);

router.use('/api/employeeRestrictedHolidays', jwtCheck);
router.use('/api/setEmployeeRestrictedHolidays', jwtCheck);


/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', {
        title: 'LMS'
    });
});

router.post('/api/clientauth', function(req, res) {
    log.logger.info("Client ID/secret auth");
    if ((config.azureApiClientId === req.body.clientId && config.azureApiClientSecret === req.body.clientSecret) ||
        (config.trayAppClientId === req.body.clientId && config.trayAppClientSecret === req.body.clientSecret)) {

        var token = jasonWebToken.sign({
            clientId: req.body.clientId
        }, req.body.clientSecret, {
            expiresIn: 9000
        });
        res.status(201).send({
            token: token
        });
    } else {
        res.status(401).send({
            message: "Invalid clientId or secret",
        });
    }

});

router.get('/api/test', function(req, res) {
    log.logger.info('Test API');
    if (!req.user) {
        return res.sendStatus(401);
    }

    res.set('Content-type', 'application/json');
    var response = {
        data: 'Test success',
        isError: false
    };
    res.send(response);
});

router.get('/api/compOffsToApprove', function(req, res) {
    log.logger.info('compOffsToApprove : calling service');
    services.compOff.getcompOffsToApprove(req, res);
});

router.get('/api/leavesToApprove', function(req, res) {
    log.logger.info('leavesToApprove : calling service');
    services.leave.getLeavesToApprove(req, res);
});

router.post('/api/approveCompOffs', function(req, res) {
    log.logger.info('approveCompOffs : calling service');
    services.compOff.approveCompOffs(req, res);
});

router.post('/api/updateCompOffStatus', function(req, res) {
    log.logger.info('updateCompOffStatus : calling service');
    services.compOff.updateCompOffStatus(req, res);
});

router.get('/api/leaveRequestReport', function(req, res) {
    log.logger.info('leaveRequestReport : calling service');
    services.reporting.getLeaveRequestReport(req, res);
});

router.get('/api/leaveSummaryReport', function(req, res) {
    log.logger.info('leaveSummaryReport : calling service');
    services.reporting.getLeaveSummaryReport(req, res);
});

router.get('/api/compOffSummaryReport', function(req, res) {
    log.logger.info('compOffSummaryReport : calling service');
    services.reporting.getCompOffSummaryReport(req, res);
});

router.get('/api/leaveBalanceReport', function(req, res) {
    log.logger.info('leaveBalanceReport : calling service');
    services.reporting.getLeaveBalanceReport(req, res);
});

router.get('/api/newJoineeReport', function(req, res) {
    log.logger.info('newJoineeReport : calling service');
    services.reporting.getNewJoineeReport(req, res);
});

router.get('/api/employeeExitReport', function(req, res) {
    log.logger.info('employeeExitReport : calling service');
    services.reporting.getEmployeeExitReport(req, res);
});

router.get('/api/lossOfPayReport', function(req, res) {
    log.logger.info('lossOfPayReport : calling service');
    services.reporting.getLossOfPayReport(req, res);
});

router.get('/api/leaveEncashmentReport', function(req, res) {
    log.logger.info('leaveEncashmentReport : calling service');
    services.reporting.getLeaveEncashmentReport(req, res);
});

router.get('/api/compOffExpiryReport', function(req, res) {
    log.logger.info('compOffExpiryReport : calling service');
    services.reporting.getCompOffExpiryReport(req, res);
});

router.get('/api/userListing', function(req, res) {
    log.logger.info('userListing : calling service');
    services.employee.getEmployees(req, res);
});

router.get('/api/user', function(req, res) {
    log.logger.info('user : calling service');
    services.employee.getEmployee(req, res);
});

router.post('/api/updateLeaveStatus', function(req, res) {
    log.logger.info('updateLeaveStatus : calling service');
    services.leave.updateLeaveStatus(req, res);
});

router.post('/api/cancelCompOff', function(req, res) {
    log.logger.info('cancelCompOff : calling service');
    services.compOff.cancelCompOff(req, res);
});

router.post('/api/monthlyLeaveCredit', function(req, res) {
    log.logger.info('monthlyLeaveCredit : calling service');
    services.leaveCredit.performMonthlyLeaveCredit(req, res);
});

router.post('/api/deleteMonthlyLeaveCredit', function(req, res) {
    log.logger.info('deleteMonthlyLeaveCredit : calling service');
    services.leaveCredit.deleteMonthlyLeaveCredit(req, res);
});

router.post('/api/cancelLeave', function(req, res) {
    log.logger.info('cancelLeave : calling service');
    services.leave.cancelLeave(req, res);
});

router.post('/api/cancelWorkFromHome', function(req, res) {
    log.logger.info('cancelWorkFromHome : calling service');
    services.workFromHome.cancelWorkFromHome(req, res);
});

router.get('/api/workFromHomesToApprove', function(req, res) {
    log.logger.info('workFromHomesToApprove : calling service');
    services.workFromHome.getWorkFromHomesToApprove(req, res);
});

router.post('/api/updateWorkFromHomeStatus', function(req, res) {
    log.logger.info('updateWorkFromHomeStatus : calling service');
    services.workFromHome.updateWorkFromHomeStatus(req, res);
});

router.get('/api/locationList', function(req, res) {
    log.logger.info('locationList : calling service');
    services.location.getLocations(req, res);
});

router.get('/api/employeeStatusList', function(req, res) {
    log.logger.info('employeeStatusList : calling service');
    services.employee.getEmployeeStatuses(req, res);
});


router.get('/api/location', function(req, res) {
    log.logger.info('location : calling service');
    services.location.getLocation(req, res);
});

router.get('/api/holidaylist', function(req, res) {
    log.logger.info('holidaylist : calling service');
    services.holidaylist.getHolidaysForUser(req, res);
});

router.get('/api/recentHolidaylist', function(req, res) {
    log.logger.info('recentHolidaylist : calling service');
    services.holidaylist.getRecentHolidaysForUser(req, res);
});

router.get('/api/compOffHistory', function(req, res) {
    log.logger.info('compOffHistory : calling service');
    services.compOff.getEmployeeCompOffs(req, res);
});

router.get('/api/leaveHistory', function(req, res) {
    log.logger.info('leaveHistory : calling service');
    services.employeeLeave.getLeaveHistory(req, res);
});

router.get('/api/employeeLeaveHistory', function(req, res) {
    log.logger.info('employeeLeaveHistory : calling service');
    services.leave.getEmployeeLeaves(req, res);
});

router.get('/api/leaveDetails', function(req, res) {
    log.logger.info('leaveDetails : calling service');
    services.leave.getLeaveDetails(req, res);
});

router.get('/api/leaveStatusHistory', function(req, res) {
    log.logger.info('leaveStatusHistory : calling service');
    services.leave.getLeaveStatusHistory(req, res);
});

router.get('/api/employeeLeaveAccrualHistory', function(req, res) {
    log.logger.info('employeeLeaveAccrualHistory : calling service');
    services.leaveCredit.getEmployeeLeaveAccruals(req, res);
});

router.get('/api/employeeLeaveUpdatedHistory', function(req, res) {
    log.logger.info('employeeLeaveUpdatedHistory : calling service');
    services.leaveCredit.getLeaveUpdatedForEmployee(req, res);
});

router.get('/api/workFromHomeHistory', function(req, res) {
    log.logger.info('workFromHomeHistory : calling service');
    services.workFromHome.getEmployeeWorkFromHomes(req, res);
});

router.get('/api/recentLeaveHistory', function(req, res) {
    log.logger.info('recentLeaveHistory : calling service');
    services.employeeLeave.getRecentLeaveHistory(req, res);
});

router.get('/api/leaveTypesWithBalance', function(req, res) {
    log.logger.info('leaveTypesWithBalance : calling service');
    services.referenceData.getleaveTypesWithBalance(req, res);
});

router.post('/api/applyLeave', function(req, res) {
    log.logger.info('applyLeave : calling service');
    services.employeeLeave.applyLeave(req, res);
});

router.get('/api/validDays', function(req, res) {
    log.logger.info('validDays : calling service');
    services.employeeLeave.GetValidDays(req, res);
});

router.get('/api/validCompOffDays', function(req, res) {
    log.logger.info('validCompOffDays : calling service');
    services.employeeLeave.GetValidCompOffDays(req, res);
});

router.get('/api/dashboardSummaryForUser', function(req, res) {
    log.logger.info('dashboardSummaryForUser : calling service');
    services.dashboard.GetDashboardSummaryForUser(req, res);
});

router.get('/api/compOffSummaryForUser', function(req, res) {
    log.logger.info('compOffSummaryForUser : calling service');
    services.compOff.GetCompOffSummaryForUser(req, res);
});

router.post('/api/applyCompOffSubmit', function(req, res) {
    log.logger.info('applyCompOffSubmit : calling service');
    services.employeeLeave.applyCompOff(req, res);
});

router.get('/api/compOffs', function(req, res) {
    log.logger.info('compOffs : calling service');
    services.employeeLeave.getEmployeeCompOffs(req, res);
});

router.get('/api/searchEmployee', function(req, res) {
    log.logger.info('searchEmployee : calling service');
    services.employee.searchEmployee(req, res);
});

router.get('/api/employeeLeaveBalance', function(req, res) {
    log.logger.info('employeeLeaveBalance : calling service');
    services.employeeLeave.GetEmployeeLeaveBalance(req, res);
});

router.post('/api/applyWFH', function(req, res) {
    log.logger.info('applyWFH : calling service');
    services.workFromHome.applyWFH(req, res);
});

router.post('/api/addEmployee', function(req, res) {
    log.logger.info('addEmployee : calling service');
    services.employee.createEmployee(req, res);
});

router.post('/api/updateEmployee', function(req, res) {
    log.logger.info('updateEmployee : calling service');
    services.employee.updateEmployee(req, res);
});

router.post('/api/addLeaveCredit', function(req, res) {
    log.logger.info('addLeaveCredit : calling service');
    services.leaveCredit.createLeaveCredit(req, res);
});

router.post('/api/updateLeaveCredit', function(req, res) {
    log.logger.info('updateLeaveCredit : calling service');
    services.leaveCredit.updateLeaveCredit(req, res);
});

router.post('/api/login', function(req, res) {
    log.logger.info('login : calling service');
    services.login.formBasedLogin(req, res);
});

router.get('/api/leaveCredits', function(req, res) {
    log.logger.info('leaveCreditList : calling service');
    services.leaveCredit.getLeaveCredits(req, res);
});

router.get('/api/leaveBalances', function(req, res) {
    log.logger.info('leaveBalances : calling service');
    services.leaveCredit.getLeaveBalances(req, res);
});

router.get('/api/leaveTypeList', function(req, res) {
    log.logger.info('leaveTypeList : calling service');
    services.leaveType.getLeaveTypes(req, res);
});

router.get('/api/leaveCreditTypeList', function(req, res) {
    log.logger.info('leaveCreditTypeList : calling service');
    services.leaveCreditType.getLeaveCreditTypes(req, res);
});

router.get('/api/leaveCredit', function(req, res) {
    log.logger.info('leaveCredit : calling service');
    services.leaveCredit.getLeaveCredit(req, res);
});

router.get('/api/getActiveEmployeesEmails', function(req, res) {
    log.logger.info('getActiveEmployeesEmails : calling service');
    services.employee.getActiveEmployeesEmails(req, res);
});

router.get('/api/hasUserLoggedInToday', function(req, res) {
    log.logger.info('hasUserLoggedInToday : calling service');
    services.employeeAttendance.hasUserLoggedInToday(req, res);
});

router.post('/api/markAttendance', function(req, res) {
    log.logger.info('markAttendance : calling service');
    services.employeeAttendance.markAttendance(req, res);
});

router.get('/api/attendance', function(req, res) {
    log.logger.info('getAttendance : calling service');
    services.employeeAttendance.getAttendance(req, res);
});

router.post('/api/updateAttendance', function(req, res) {
    log.logger.info('updateAttendance : calling service');
    services.employeeAttendance.updateAttendance(req, res);
});

router.get('/api/employeeRestrictedHolidays', function(req, res) {
    log.logger.info('employeeRestrictedHolidays : calling service');
    services.holidaylist.getSelectedRestrictedHolidaysForUser(req, res);
});

router.post('/api/setEmployeeRestrictedHolidays', function(req, res) {
    log.logger.info('setEmployeeRestrictedHolidays : calling service');
    services.holidaylist.setRestrictedHolidayForUser(req, res);
});

router.get('/api/leaveRequestsOnADay', function(req, res) {
    log.logger.info('leaveRequestsOnADay : calling service');
    services.employeeLeave.GetLeaveRequestsForEmployeeOnADay(req, res);
});

router.get('/api/attendanceStatusName',function(req,res){
     log.logger.info('attendanceStatus : calling service');
     services.attendanceStatus.GetAttendanceStatusName(req, res);
});


module.exports = router;