(function(services){

   services.login = require('./login');
   services.compOff = require('./compOff');
   services.workFromHome = require('./workFromHome');
   services.holidaylist = require('./holidaylist');
   services.employeeLeave = require('./employeeLeave');
   services.employee = require('./employee');
   services.referenceData = require('./referenceData');
   services.dashboard = require('./dashboard');
   services.util = require('./util');
   services.reporting = require('./reporting');
   services.location = require('./location');
   services.leave = require('./leave');
   services.log = require('./log');
   services.executionContext = require('./executionContext');
   services.leaveCredit = require('./leaveCredit');
   services.leaveType = require('./leaveType');
   services.employeeAttendance=require('./employeeAttendance');
   services.leaveCreditType = require('./leaveCreditType');
   services.calenderOperations = require('./calenderOperations');
   services.attendanceStatus = require('./attendanceStatus');
})(module.exports);
