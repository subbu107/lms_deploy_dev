(function (service) {

    var dao = require('../data/dao'),
        util = require('./util'),
        executionContext = require('./executionContext'),
        log = require('./log'),
        mustache = require('mustache'),
        emailNotifications = require('./emailNotifications'),
        config = require("../config"),
        dateFormat = require('dateformat');

    service.getRecentLeaveHistory = function (req, resp) {
        try {
            dao.employeeLeave.getRecentLeaveHistory({
                UserId: executionContext.getEffectiveUserId(req, resp)
            }).then(function (data) {
                util.sendSuccessResponse(data, resp);
            }, function (err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    service.getLeaveHistory = function (req, resp) {

        try {
            dao.employeeLeave.getEmployeeWithLeaveHistory({
                UserId: executionContext.getEffectiveUserId(req, resp)
            }).then(function (employee) {
                var leaveRequestDetails = [];

                if (employee.LeaveRequests.length > 0) {
                    employee.LeaveRequests.forEach(function (leaveRequest) {
                        leaveRequest.LeaveDurations.forEach(function (leaveDuration) {
                            var leaveRequestDetail = {};
                            leaveRequestDetail.Id = leaveRequest.id;
                            leaveRequestDetail.LeaveStatus = leaveRequest.LeaveStatus.Name;
                            leaveRequestDetail.LeaveType = leaveRequest.LeaveType.Name;
                            leaveRequestDetail.LeaveDate = leaveDuration.LeaveDate;
                            leaveRequestDetail.IsHalfDay = (leaveDuration.LeaveHours === 4);
                            leaveRequestDetail.DayType = (leaveDuration.LeaveHours === 4) ? 'Half Day' : 'Full Day';
                            leaveRequestDetail.Comments = leaveRequest.Comments;
                            leaveRequestDetails.push(leaveRequestDetail);
                        });

                    });
                }

                util.sendSuccessResponse(leaveRequestDetails, resp);
            }, function (err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(err, resp);
        }
    };

    service.getEmployeeCompOffs = function (req, resp) {

        try {
            dao.employeeLeave.getEmployeeCompOffs({
                EmployeeId: executionContext.getEffectiveUserId(req, resp)
            }).then(function (compOffDetails) {
                util.sendSuccessResponse(compOffDetails, resp);
            }, function (err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);
        }
    };

    service.applyLeave = function (req, res) {

        try {
            var user = util.getUserFromRequest(req, res);
            var userId = util.getUserIdToBeUsed(req, res);

            var leaveReq = {
                EmployeeId: userId,
                LeaveTypeId: req.body.selectedLeaveType.LeaveTypeId,
                LeaveStatusId: 0,
                Comments: req.body.comments,
                LeaveDays: req.body.leaveDays,
                isAdminRequest: user.IsAdmin
            }
            return dao.referenceData.getLeaveStatusByName({
                Name: 'Applied'
            }).
            then(function (leaveStatus) {
                leaveReq.LeaveStatusId = leaveStatus[0].id;
                var transactionPromise = dao.employeeLeave.CreateLeaveRequest(leaveReq);
                transactionPromise.then(function () {
                    dao.employee.getEmployee({
                        id: userId
                    }).then(function (userApplingForLeave) {

                        var subject = mustache.render(emailNotifications.subjectTemplate, {
                            user: userApplingForLeave.FullName,
                            leaveType: 'leave'
                        });

                        var noOfDaysLeaveApplied = 0;
                        for (var i = 0; i < req.body.leaveDays.length; ++i) {
                            noOfDaysLeaveApplied += (req.body.leaveDays[i].isHalfDay ? 0.5 : 1);
                        }

                        var messageBody = mustache.render(emailNotifications.messageTemplate, {
                            user: userApplingForLeave.FullName,
                            Days: req.body.leaveDays,
                            DayRow: function () {
                                return "<tr><td>" + dateFormat(this.leaveDay, 'dd-mmm-yyyy') + "</td><td>" + (this.isHalfDay == true ? 'Half Day' : 'Full Day') + "</td></tr>";
                            },
                            noOfDays: noOfDaysLeaveApplied,
                            comments: req.body.comments,
                            leaveType: 'leave',
                            leaveTypeUpperCase: 'Leave'
                        });

                        var userLocation = userApplingForLeave.Location.id;
                        var spiderooo;
                        if (userLocation === 1) {
                            spiderooo = config.bangalorecc;
                        } else {
                            spiderooo = config.punecc;
                        }

                        emailNotifications.sendMessage(subject, userApplingForLeave.OfficialEmailId, req.body.emailRecipients, userApplingForLeave.OfficialEmailId + ";" + spiderooo, messageBody);

                    });
                });
                return transactionPromise;
            }).then(function (leaveRequest) {
                util.sendSuccessResponse(leaveRequest, res);
            }, function (err) {
                log.logger.info('failed to create leave request ' + err);
                util.sendErrorResponse(err, res);
            });
        } catch (e) {
            log.logger.info('error : ', e)
            util.sendErrorResponse(e, resp);
        }
    }

    service.applyCompOff = function (req, res) {

        try {
            var userId = util.getUserIdToBeUsed(req, res);

            var compOffReq = {
                EmployeeId: userId,
                CompOffStatusId: 0,
                Comments: req.body.comments,
                CompOffDays: req.body.compOffDays,
            }

            return dao.referenceData.getCompOffStatusByName({
                Name: 'Applied'
            }).
            then(function (compOffStatus) {
                compOffReq.CompOffStatusId = compOffStatus[0].id;
                var transactionPromise = dao.employeeLeave.CreateCompOffRequest(compOffReq);

                transactionPromise.then(function () {
                    dao.employee.getEmployee({
                        id: userId
                    }).then(function (userApplingForCompOff) {

                        var subject = mustache.render(emailNotifications.subjectTemplate, {
                            user: userApplingForCompOff.FullName,
                            leaveType: 'comp-off'
                        });

                        var userLocation = userApplingForCompOff.Location.id;
                        var spiderooo;
                        if (userLocation === 1) {
                            spiderooo = config.bangalorecc;
                        } else {
                            spiderooo = config.punecc;
                        }

                        var noOfDaysCompOffApplied = 0;
                        for (var i = 0; i < req.body.compOffDays.length; ++i) {
                            noOfDaysCompOffApplied += (req.body.compOffDays[i].IsHalfDay ? 0.5 : 1);
                        }

                        var messageBody = mustache.render(emailNotifications.messageTemplate, {
                            user: userApplingForCompOff.FullName,
                            Days: req.body.compOffDays,
                            DayRow: function () {
                                return "<tr><td>" + dateFormat(this.CompOffDay, 'dd-mmm-yyyy') + "</td><td>" + (this.IsHalfDay == true ? 'Half Day' : 'Full Day') + "</td></tr>";
                            },
                            noOfDays: noOfDaysCompOffApplied,
                            comments: req.body.comments,
                            leaveType: 'comp-off',
                            leaveTypeUpperCase: 'Comp-Off'
                        });

                        emailNotifications.sendMessage(subject, userApplingForCompOff.OfficialEmailId, req.body.emailRecipients, userApplingForCompOff.OfficialEmailId + ";" + spiderooo, messageBody);

                    });
                });

                return transactionPromise;
            }).then(function (compOffRequest) {
                util.sendSuccessResponse(compOffRequest, res);
            }, function (err) {
                log.logger.info('failed to create Comp-Off request ' + err);
                util.sendErrorResponse(err, res);
            });
        } catch (e) {
            log.logger.info('error : ', e)
            util.sendErrorResponse(e, resp);
        }
    }

    service.GetValidDays = function (req, res) {
        try {
            var userId = util.getUserIdToBeUsed(req, res);

            dao.employeeLeave.GetValidDays({
                EmployeeId: userId,
                StartDate: req.param('startDate'),
                EndDate: req.param('endDate'),
                LeaveType: req.param('leaveType')
            }).then(function (validDays) {
                util.sendSuccessResponse(validDays, res);
            }, function (err) {
                log.logger.info('failed to get valid days ' + err);
                util.sendErrorResponse(err, res);
            });
        } catch (e) {
            util.sendErrorResponse(e, res);
        }
    };

    service.GetEmployeeLeaveBalance = function (req, res) {
        try {
            var userId = util.getUserIdToBeUsed(req, res);

            dao.employeeLeave.GetEmployeeLeaveBalance({
                EmployeeId: userId,
                LeaveTypeId: req.param('leaveTypeId')
            }).then(function (results) {
                util.sendSuccessResponse(results[0], res);
            }, function (err) {
                util.sendErrorResponse(err, res);
            })
        } catch (e) {
            util.sendErrorResponse(e, res);
        }
    };

    service.GetValidCompOffDays = function (req, res) {
        var user = util.getUserFromRequest();
        var userId = util.getUserIdToBeUsed(req, res);

        try {
            dao.employeeLeave.GetValidCompOffDays({
                EmployeeId: userId,
                IsAdminRequest: user.IsAdmin,
                StartDate: req.param('startDate'),
                EndDate: req.param('endDate')
            }).then(function (validCompOffDays) {
                util.sendSuccessResponse(validCompOffDays, res);
            }, function (err) {
                log.logger.info('failed to get valid leave days ' + err);
                util.sendErrorResponse(err, res);
            });
        } catch (e) {
            util.sendErrorResponse(e, res);
        }

    };



    service.GetLeaveRequestsForEmployeeOnADay = function (req, res) {
        var dt = req.param('dateToCheck');
        dao.employeeLeave.GetLeaveDurationsForEmployeeOnADay({
            EmployeeId: executionContext.getEffectiveUserId(req, res),
            DateToCheck: req.param('dateToCheck')
        }).then(function (leaveDurations) {
            var leaveRequestIds = [];
            leaveDurations.forEach(function (leaveDuration) {
                leaveRequestIds.push(leaveDuration.LeaveRequestId);
            });
            dao.employeeLeave.GetLeaveRequestByIds(leaveRequestIds).then(function (leaveRequests) {
                util.sendSuccessResponse(leaveRequests, res);
            }, function (err) {
                log.logger.info('failed to get valid leave days ' + JSON.stringify(err));
                util.sendErrorResponse(err, res);
            });

        }, function (err) {
            log.logger.info('failed to get valid leave days ' + JSON.stringify(err));
            util.sendErrorResponse(err, res);
        });
    };

})(module.exports);