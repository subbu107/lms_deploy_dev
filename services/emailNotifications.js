(function(emailNotifications) {

  var helper = require('sendgrid').mail,
      sg = require('sendgrid')(process.env.SENDGRID_API_KEY || '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'),
      log = require('./log'),
      config = require("../config");

  emailNotifications.subjectTemplate= "{{user}} has applied for {{leaveType}}.";
  emailNotifications.messageTemplate = "<html><head><style>table {border-collapse: collapse; width: 100%;} th, td {text-align: left;padding: 8px;}"
  + " tr:nth-child(even){background-color: #f2f2f2} th {background-color: #1678C1;color: white;}</style></head><body>"+
  "<p>Hi,</p>"+
  "<p>I have appplied for {{noOfDays}} day(s) {{leaveType}}. Below are the {{leaveType}} details.</p>"+
  "<p><b>Note:</b>&nbsp;{{comments}}</p>"+
  "<p><table><thead><tr><th>Date</th><th>{{leaveTypeUpperCase}} Duration</th></tr></thead><tbody>"+
  "{{#Days}}{{{DayRow}}}{{/Days}}</tbody></table></p>"+
  "<p>Thanks,</p>"+
  "<p>{{user}}</p>"+
  "</body></html>";
  emailNotifications.statusChangeSubjectTemplate= "Your {{leaveType}} has been {{status}}.";
  emailNotifications.statusChangeMessageTemplate = "<html><body>"+
  "<p>Hi {{user}},</p>"+
  "<p>Your {{leaveType}} No. {{number}} has been {{status}}.</p>"+
  "<p><b>Note:</b>&nbsp;{{comments}}</p>"+
  "<p>Thanks,</p>"+
  "<p>{{adminUser}}</p>"+
  "</body></html>";

  emailNotifications.sendMessage = function(subject, from, toList, cc, message){
    if(config.emails == "off")
      return;

    try{
      var mail = new helper.Mail();
      email = new helper.Email(from, from);
      mail.setFrom(email);

      mail.setSubject(subject);

      var personalization = new helper.Personalization();

      var tempToEmails = [];

      //Add only unique set of email ids
      for (var i = 0; i < toList.length; ++i) {
        if(i > 1 && tempToEmails.indexOf(toList[i]) != -1) {
          continue;
        }
        tempToEmails.push(toList[i].toLowerCase());
        var email = new helper.Email(toList[i]);
        personalization.addTo(email);
      }

      //we should ensure uniqueness accross cc and to addresses
      if(cc != null && tempToEmails.indexOf(cc) == -1) {
        personalization.addCc(new helper.Email(cc));
      }

      mail.addPersonalization(personalization);

      var content = new helper.Content("text/plain", subject);
      mail.addContent(content);
      content = new helper.Content("text/html", message);
      mail.addContent(content);

      var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
      });

      sg.API(request, function(error, response) {
        if(error){
          log.logger.info("SendGrid API returned Error:" + error);
          log.logger.info("Request:" + JSON.stringify(request));
        }
        log.logger.info(response.statusCode);
        log.logger.info(response.body);
        log.logger.info(response.headers);
      });
    }catch(e){
      log.logger.info("Error sending email:" + e);
      log.logger.info("subject:"+subject + ", from:" + from + ", toList:" + JSON.stringify(toList) + ", cc:" + cc + ", message:" + message);
    }
  }

})(module.exports);
