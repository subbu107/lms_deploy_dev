(function(leave) {

    var dao = require('../data/dao'),
        log = require('./log'),
        executionContext = require('./executionContext'),
        util = require('./util'),
        mustache = require('mustache'),
        emailNotifications = require('./emailNotifications');

    leave.getLeavesToApprove = function(req, resp) {
        log.logger.info('Service : getLeavesToApprove');
        util.validateAdminUser(req, resp);
        try {
            dao.leave.getLeavesToApprove({
                locationId: req.param('selectedLocationId')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    leave.updateLeaveStatus = function(req, resp) {
        log.logger.info('Service : updateLeaveStatus');
        util.validateAdminUser(req, resp);
        try {

            var user = util.getUserFromRequest(req, resp);

            dao.leave.updateLeaveStatus({
                    SelectedIds: req.param('selectedIds'),
                    Status: req.param('status'),
                    EmployeeId: user.id,
                    Comments: req.param('comments')
                })
                .then(function(data) {
                    var leaveRequestIds = req.param('selectedIds');
                    dao.employee.getEmployee({id:user.id}).then(function(approvingUser){
                      for (var i = 0; i < leaveRequestIds.length; i++) {
                        var empDetails = dao.leave.getEmployeeDetailsOfALeaveRequest({leaveRequestId: leaveRequestIds[i]}).then(function (emp){

                          var subject = mustache.render(emailNotifications.statusChangeSubjectTemplate, {user:approvingUser.FullName, leaveType: 'leave', status: req.param('status')});

                          var commentsByApprover = req.param('comments');
                          commentsByApprover = commentsByApprover != null && commentsByApprover.length > 0 ?  commentsByApprover: 'None';
                          var messageBody = mustache.render(emailNotifications.statusChangeMessageTemplate,{
                            adminUser: approvingUser.FullName,
                            status: req.param('status'),
                            number: emp[0].leaveRequestId,
                            comments: commentsByApprover,
                            leaveType: 'leave',
                            user: emp[0].FirstName
                          });

                          emailNotifications.sendMessage(subject, approvingUser.OfficialEmailId, [emp[0].OfficialEmailId], null, messageBody);
                        });
                      }
                    });

                    util.sendSuccessResponse(data, resp);
                }, function(err) {
                    util.sendErrorResponse(err, resp);
                });
        } catch (e) {
            util.sendErrorResponse(e, resp);
        }
    };

    leave.getEmployeeLeaves = function(req, resp) {
        log.logger.info('leave->getEmployeeLeaves');
        try {
            dao.leave.getEmployeeLeaves({
                userId: executionContext.getEffectiveUserId(req, resp)
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    leave.getLeaveDetails = function(req, resp) {
        log.logger.info('leave->getLeaveDetails');
        var leaveForUserId = executionContext.getEffectiveUserId(req, resp);
        var isAdmin = executionContext.isAdmin(req, resp);

        try {
            dao.leave.getLeaveDetails({
                leaveRequestId: req.param('leaveRequestId'),
                leaveForUserId: leaveForUserId
            }).then(function(data) {
                if (!data || data.length <= 0) {
                    util.sendErrorResponse("Not found!", resp);
                } else if (!isAdmin && leaveForUserId != data[0].EmployeeId) {
                    log.logger.error('leave->getLeaveDetails:Unauthorized access' + leaveForUserId);
                    util.sendErrorResponse("Unauthorized access!", resp);
                } else {
                    var fillHeader = false;

                    var leaveDetails = {};
                    leaveDetails.leaveDays = [];
                    data.forEach(function(row) {
                        if (fillHeader == false) {
                            leaveDetails.leaveRequestId = row.LeaveRequestId;
                            leaveDetails.employeeId = row.EmployeeId;
                            leaveDetails.appliedOn = row.CreatedOn;
                            leaveDetails.leaveStatus = row.LeaveStatus;
                            leaveDetails.leaveType = row.LeaveType;

                            fillHeader = true;
                        }

                        leaveDetails.leaveDays.push({
                            leaveDate: row.LeaveDate,
                            leaveHours: row.LeaveHours,
                            minCompOffDate: row.MinCompOffDate,
                            maxCompOffDate: row.MaxCompOffDate,
                            compOffHours: row.CompOffHours
                        });
                    });

                    util.sendSuccessResponse(leaveDetails, resp);
                }
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    leave.getLeaveStatusHistory = function(req, resp) {
        log.logger.info('leave->getLeaveStatusHistory');
        var leaveForUserId = executionContext.getEffectiveUserId(req, resp);
        var isAdmin = executionContext.isAdmin(req, resp);

        try {
            dao.leave.getLeaveStatusHistory({
                leaveRequestId: req.param('leaveRequestId'),
                leaveForUserId: leaveForUserId
            }).then(function(data) {
                if (!data || data.length <= 0) {
                    util.sendErrorResponse("Not found!", resp);
                } else if (!isAdmin && leaveForUserId != data[0].EmployeeId) {
                    log.logger.error('leave->getLeaveDetails:Unauthorized access' + leaveForUserId);
                    util.sendErrorResponse("Unauthorized access!", resp);
                } else {
                    var fillHeader = false;

                    var leaveStatusHistory = {};
                    leaveStatusHistory.statusEntries = [];
                    data.forEach(function(row) {
                        if (fillHeader == false) {
                            leaveStatusHistory.leaveRequestId = row.LeaveRequestId;
                            leaveStatusHistory.employeeId = row.EmployeeId;

                            fillHeader = true;
                        }

                        leaveStatusHistory.statusEntries.push({
                            statusDate: row.StatusDate,
                            leaveStatus: row.LeaveStatus,
                            changedBy: row.ChangedBy,
                            comments: row.Comments
                        });
                    });

                    util.sendSuccessResponse(leaveStatusHistory, resp);
                }
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    leave.cancelLeave = function(req, resp) {
        try {
            var user = util.getUserFromRequest(req, resp);

            dao.leave.updateLeaveStatus({
                    SelectedIds: req.param('leaveRequestId'),
                    Status: 'Canceled',
                    EmployeeId: user.id
                })
                .then(function(data) {
                    util.sendSuccessResponse(data, resp);
                }, function(err) {
                    util.sendErrorResponse(err, resp);
                });
        } catch (e) {
            util.sendErrorResponse(e, resp);
        }
    };


})(module.exports);
