(function (attendanceStatus) {

    var dao = require('../data/dao'),
        util = require('./util');

    attendanceStatus.GetAttendanceStatusName = function (req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.attendanceStatus.GetAttendanceStatusName()
                .then(function (data) {
                    util.sendSuccessResponse(data, resp);
                }, function (err) {
                    util.sendErrorResponse(err, resp);
                });

        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

})(module.exports);