(function(reporting) {

    var dao = require('../data/dao'),
        log = require('./log'),
        calenderOperations = require('./calenderOperations'),
        util = require('./util');

    reporting.getLeaveRequestReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getLeaveRequestReport({
                locationId: req.param('selectedLocationId')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    reporting.getLeaveSummaryReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getLeaveSummaryReport({
                locationId: req.param('selectedLocationId'),
                fromDate: calenderOperations.getStartDateOfMonthOrDefault(req.param('fromMonth')),
                toDate: calenderOperations.getEndDateOfMonthOrDefault(req.param('toMonth'))
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    reporting.getCompOffSummaryReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getCompOffSummaryReport({
                locationId: req.param('selectedLocationId'),
                fromDate: calenderOperations.getStartDateOfMonthOrDefault(req.param('fromMonth')),
                toDate: calenderOperations.getEndDateOfMonthOrDefault(req.param('toMonth'))
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    reporting.getLeaveBalanceReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getLeaveBalanceReport({
                locationId: req.param('selectedLocationId')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    reporting.getNewJoineeReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getNewJoineeReport({
                locationId: req.param('selectedLocationId'),
                fromDate: calenderOperations.getStartDateOfMonthOrDefault(req.param('fromMonth')),
                toDate: calenderOperations.getEndDateOfMonthOrDefault(req.param('toMonth'))
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    reporting.getEmployeeExitReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getEmployeeExitReport({
                locationId: req.param('selectedLocationId'),
                fromDate: calenderOperations.getStartDateOfMonthOrDefault(req.param('fromMonth')),
                toDate: calenderOperations.getEndDateOfMonthOrDefault(req.param('toMonth'))
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    reporting.getLossOfPayReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getLossOfPayReport({
                locationId: req.param('selectedLocationId'),
                fromDate: calenderOperations.getStartDateOfMonthYearOrDefault(req.param('fromMonth'), req.param('year')),
                toDate: calenderOperations.getEndDateOfMonthYearOrDefault(req.param('toMonth'), req.param('year'))        
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    reporting.getLeaveEncashmentReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getLeaveEncashmentReport({
                locationId: req.param('selectedLocationId')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    reporting.getCompOffExpiryReport = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.reporting.getCompOffExpiryReport({  
                locationId: req.param('selectedLocationId')              
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

})(module.exports);
