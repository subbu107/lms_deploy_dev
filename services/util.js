(function(util) {

    var jwt = require("jsonwebtoken"),
        log = require('./log');

    util.sendSuccessResponse = function(data, resp) {
        resp.set('Content-type', 'application/json');
        var response = {
            data: data,
            isError: false
        };
        resp.send(response);
    };

    util.isCustomDatabaseError = function(err) {
        if (err != null && err.parent != null && err.parent.class != null) {
            //16 is the custom error severity set in the stored procedures
            if (err.parent.class = 16)
                return true;
        }
        return false;
    }

    util.sendErrorResponse = function(err, resp) {
        log.logger.info('error : ', err);
        resp.set('Content-type', 'application/json');
        var errorToBePassed = err;

        if (util.isCustomDatabaseError(err)) {
            errorToBePassed = err.message;
        }

        var response = {
            data: null,
            error: errorToBePassed,
            isError: true
        };
        resp.send(response);
    }

    util.getUserFromRequest = function(req, res) {
        var token = req.param('token');
        var user = jwt.decode(token);

        if (!user) {
            token = req.headers.authorization.split(' ')[1];
            user = jwt.decode(token);
        }

        if (!user) {
            return res.status(400).send("Logged-in user data is missing.");
        }

        return user;
    }

    util.getUserIdToBeUsed = function(req, res) {
        var user = util.getUserFromRequest(req, res);
        var userId = user.IsAdmin && req.param('selectedUserId') ? req.param('selectedUserId') : user.id;
        return userId;
    }

    util.validateAdminUser = function(req, resp) {
        log.logger.info('Validate Admin');
        var user = util.getUserFromRequest(req, resp);
        if (!user.IsAdmin) {
            log.logger.info('Unauthorized request error' + user.id);
            return resp.status(401).send("Unauthorized access.");
        }
    }
})(module.exports);
