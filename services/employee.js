(function(service) {

    var dao = require('../data/dao'),
        util = require('./util'),
        log = require('./log'),
        dateFormat = require('dateformat');

    service.getActiveEmployeesEmails = function(req, resp){
        try{
          log.logger.info('Service : getActiveEmployeesEmails');
          var userId = util.getUserIdToBeUsed(req, resp);
          dao.employee.getActiveEmployeesEmails({id: userId})
          .then(function(data) {
            util.sendSuccessResponse(data, resp);
          }, function(err) {
            util.sendErrorResponse(err, resp);
          });
        }catch(e){
          util.sendErrorResponse(e, resp);
        }
    };

    service.getEmployee = function(req, resp) {
        try {
            dao.employee.getEmployee({
                id: req.param('id')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);
        }
    };

    service.getEmployees = function(req, resp) {
        log.logger.info('Service : getEmployees');
        util.validateAdminUser(req, resp);
        try {
            dao.employee.getEmployees({
                locationId: req.param('selectedLocationId')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    service.getEmployeeStatuses = function(req, resp) {
        log.logger.info('Service : getEmployeeStatuses');
        util.validateAdminUser(req, resp);
        try {
            dao.employee.getEmployeeStatuses().then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    service.searchEmployee = function(req, res) {
        log.logger.info("services->searchEmployee , searchString : " + req.param('searchString'));
        util.validateAdminUser(req, res);
        try {
            dao.employee.searchEmployee({
                SearchString: req.param('searchString')
            }).then(function(employeeSearchResults) {
                util.sendSuccessResponse(employeeSearchResults, res);
            }, function(err) {
                log.logger.info('failed to search for employee ' + err);
                util.sendErrorResponse(err, res);
            });
        } catch (e) {
            log.logger.info('failed to search for employee ' + err);
            util.sendErrorResponse(err, res);
        }
    };

    service.buildEmployeeObject = function(req) {
        var employee = {
            id: req.body.id,
            FirstName: req.body.FirstName,
            MiddleName: req.body.MiddleName,
            LastName: req.body.LastName,
            UserName: req.body.UserName,
            LocationId: req.body.Location.id,
            EmployeeStatusId: req.body.EmployeeStatus.id,
            IsAdmin: req.body.IsAdmin,
            IsFemale: req.body.IsFemale,
            OfficialEmailId: req.body.OfficialEmailId,
            NTLogin:req.body.NTLogin
        };

        if (req.body.DateOfBirth)
            employee.DateOfBirth = new Date(req.body.DateOfBirth);
        if (req.body.DateOfJoining)
            employee.DateOfJoining = new Date(req.body.DateOfJoining);
        if (req.body.DateOfExit)
            employee.DateOfExit = new Date(req.body.DateOfExit);
        return employee;
    }

    service.createEmployee = function(req, res) {
        var user = util.getUserFromRequest(req, res);
        util.validateAdminUser(req, res);
        try {
            var employee = service.buildEmployeeObject(req);
            employee.CreatedBy = user.id;

            dao.employee.create(employee).then(function(employee) {
                util.sendSuccessResponse(employee, res);
            }, function(err) {
                util.sendErrorResponse(err, res);
            });
        } catch (e) {
            util.sendErrorResponse(e, res);
        }
    };

    service.updateEmployee = function(req, res) {
        util.validateAdminUser(req, res);
        var user = util.getUserFromRequest(req, res);
        try {
            var employee = service.buildEmployeeObject(req);
            employee.UpdatedBy = user.id;
            dao.employee.update(employee).then(function(employee) {
                util.sendSuccessResponse(employee, res);
            }, function(err) {
                util.sendErrorResponse(err, res);
            });
        } catch (e) {
            util.sendErrorResponse(e, res);
        }
    };

})(module.exports);
