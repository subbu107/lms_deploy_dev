(function(app) {
  var express = require("express"),
    _ = require("lodash"),
    config = require("../config"),
    jwt = require("jsonwebtoken"),
    passport = require('passport'),
    WindowsLiveStrategy = require('passport-windowslive').Strategy;

  var dao = require('../data/dao');
  var log = require('./log');

  app.router = express.Router();

  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(obj, done) {
    done(null, obj);
  });

  passport.use(new WindowsLiveStrategy({
      clientID: config.windowsLiveClientId,
      clientSecret: config.windowsLiveClientSecret,
      callbackURL: config.windowsLiveCallBackUrl
    },
    function(accessToken, refreshToken, profile, done) {
      // asynchronous verification, for effect...
      process.nextTick(function() {
        // the user's Windows Live profile is returned to represent the logged-in user
        return done(null, profile);
      });

    }
  ));


  function createToken(employee) {
    log.logger.info('createToken');
    return jwt.sign({
      id: employee.id,
      FirstName: employee.FirstName,
      IsAdmin: employee.IsAdmin,
      LocationId : employee.LocationId
    }, config.secret, {
      expiresIn: 9000
    });
  }

  app.router.get('/auth/windowslive',
    passport.authenticate('windowslive', {
      scope: ['wl.signin', 'wl.emails']
    }),
    function(req, res) {
      // The request will be redirected to Windows Live for authentication, so
      // this function will not be called.
    });


  app.router.get('/auth/windowslive/callback',
    passport.authenticate('windowslive', {
      failureRedirect: '/#/signin?errorMessage=FAILURE_AT_WINDOWS_LIVE'
    }),
    function(req, res) {
      var userEmail = req.user.emails[0].value;
      log.logger.info('Authentication success at live end' + userEmail);

      dao.employee.getEmployeeMinimalByUserName({
        UserName: userEmail
      }).then(function(employee) {
        if (employee.length < 1) {
          if(!userEmail.endsWith('spiderlogic.com'))
            res.redirect('/#/signin?errorMessage=USER_EMAIL_NON_SPIDER');
          else
            res.redirect('/#/signin?errorMessage=EMP_INVALID');
        } else if (employee.length > 1) {
          res.redirect('/#/signin?errorMessage=EMP_MULTIPLE');
        } else if (employee[0].EmployeeStatusMinimal.Name != 'Active') {
          res.redirect('/#/signin?errorMessage=INACTIVE_USER');
        } else {
          var token = createToken(employee[0]);
          res.setHeader('Authorization', 'Bearer ' + token);
          res.redirect('/#/roundTrip?token=' + token);
        }
      });
    });


  app.formBasedLogin = function(req, res) {
    log.logger.info("Inside formBasedLogin");

    if (!req.body.username || !req.body.password) {
      return res.status(400).send("You must send the username and the password");
    }

    if (req.body.password != "LM3P@ss") {
      return res.status(400).send("Username or password is incorrect");
    }

    try {
      dao.employee.getEmployeeMinimalByUserName({
        UserName: req.body.username
      }).then(function(employee) {

        if (!employee || employee.length === 0) {
          log.logger.info("Could not get employee");
          res.status(401).send({
            message: "Invalid user. Please contact admin to register.",
          });
          return '';
        } else if (employee.length > 1) {
          log.logger.info("More than one employee");
          res.status(401).send({
            message: "Invalid users. Please contact admin to register.",
          });
          return '';
        } else if (employee[0].EmployeeStatusMinimal.Name != 'Active') {
          log.logger.info("Inactive employee");
          res.status(401).send({
            message: "Inactive employee. ABC.",
          });
          return '';
        } else {
          res.status(201).send({
            token: createToken(employee[0])
          });
        }
      }, function(err) {
        return res.status(401).send({
          message: "The username or password don't match",
          user: err
        });
      });
    } catch (e) {
      return res.status(401).send({
        message: "The username or password don't match",
        user: employee
      });
    }
  };

})(module.exports);
