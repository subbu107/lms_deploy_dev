(function (service) {

  var dao = require('../data/dao'),
    jwt = require("jsonwebtoken"),
    util = require('./util'),
    executionContext = require('./executionContext'),
    log = require('./log');

  service.getHolidaysForUser = function (req, resp) {
    log.logger.info('service->getHolidaysForUser');
    
    dao.holidayList.getHolidaysForUser({
      UserId: executionContext.getEffectiveUserId(req, resp)
    }).then(function (data) {
      dao.holidayList.getSelectedRestrictedHolidaysForUser({
        UserId: executionContext.getEffectiveUserId(req, resp)
      }).then(function (restrictedHoliday) {
        if (restrictedHoliday !== null) {
          data.push({
            Name: restrictedHoliday.Name,
            Description: restrictedHoliday.Name +  '(Restricted)',
            ActualDate: restrictedHoliday.ActualDate,
            IsRestricted: true
          });

        }
        data.sort(function (a, b) {
          return new Date(a.ActualDate) - new Date(b.ActualDate);
        });
        util.sendSuccessResponse(data, resp);
      }, function (err) {
        util.sendErrorResponse(err, resp);
      });

    }, function (err) {
      util.sendErrorResponse(err, resp);
    });

  };

  service.getRecentHolidaysForUser = function (req, resp) {
    log.logger.info('service -> getRecentHolidaysForUser');

    try {
      dao.holidayList.getRecentHolidaysForUser({
        UserId: executionContext.getEffectiveUserId(req, resp)
      }).then(function (data) {
        util.sendSuccessResponse(data, resp);
      }, function (err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);;
    }
  };

  service.getAllHolidaysForLocation = function (req, resp) {
    log.logger.info('calling dao');
    dao.holidayList.getAllHolidaysForLocation({
      LocationName: req.param('locationName'),
      Year: req.param('year')
    }).then(function (data) {
      util.sendSuccessResponse(data, resp);
    }, function (error) {
      util.sendErrorResponse(e, resp);
    });
  };

  service.getSelectedRestrictedHolidaysForUser = function (req, resp) {
    log.logger.info('calling dao');
    dao.holidayList.getSelectedRestrictedHolidaysForUser({
      UserId: executionContext.getEffectiveUserId(req, resp)
    }).then(function (data) {
      util.sendSuccessResponse(data, resp);
    }, function (error) {
      util.sendErrorResponse(error, resp);
    });
  };

  service.setRestrictedHolidayForUser = function (req, resp) {
    log.logger.info('calling dao');
    dao.holidayList.setRestrictedHolidayForUser({
      UserId: executionContext.getEffectiveUserId(req, resp),
      RestrictedHolidayId: req.body.RestrictedHolidayId
    }).then(function (data) {
      util.sendSuccessResponse(data, resp);
    }, function (error) {
      util.sendErrorResponse(error, resp);
    });
  };


})(module.exports);