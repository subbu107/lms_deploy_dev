(function(calenderOperations) {

    var log = require('./log');

    calenderOperations.getStartDateOfMonthOrDefault = function(requestParam) {
        var month = calenderOperations.getMonthOrDefaultFromRequest(requestParam);
        return calenderOperations.getStartDateOfMonth(month);
    }

    calenderOperations.getStartDateOfMonthYearOrDefault = function(requestParam1, requestParam2){
        var month = calenderOperations.getMonthOrDefaultFromRequest(requestParam1);
        var year = calenderOperations.getYearOrDefaultFromRequest(requestParam2);
        return calenderOperations.getStartDateOfMonthYear(month, year);
    }

    calenderOperations.getStartDateOfMonth = function(month) {
        var currentDate = new Date();
        return month + '/01/' + currentDate.getFullYear();
    }

    calenderOperations.getStartDateOfMonthYear = function(month, year){
        var currentDate = new Date();
        return month + '/01/' + year;
    }

    calenderOperations.getEndDateOfMonthOrDefault = function(requestParam) {
        var month = calenderOperations.getMonthOrDefaultFromRequest(requestParam);
        return calenderOperations.getEndDateOfMonth(month);
    }

    calenderOperations.getEndDateOfMonthYearOrDefault = function(requestParam1, requestParam2) {
        var month = calenderOperations.getMonthOrDefaultFromRequest(requestParam1);
        var year = calenderOperations.getYearOrDefaultFromRequest(requestParam2);
        return calenderOperations.getEndDateOfMonthYear(month, year);
    }

    calenderOperations.getEndDateOfMonth = function(month) {
        var endDate = null;
        var currentDate = new Date();
        switch (month) {
            case 2:
                endDate = calenderOperations.getEndDateOfFebruary(currentDate.getFullYear());
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                endDate = month + '/31/' + currentDate.getFullYear();
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                endDate = month + '/30/' + currentDate.getFullYear();
                break;
        }
        return endDate;
    }

    calenderOperations.getEndDateOfMonthYear = function(month, year) {
        var endDate = null;
        var currentDate = new Date();
        switch (month) {
            case 2:
                endDate = calenderOperations.getEndDateOfFebruary(year);
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                endDate = month + '/31/' + year;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                endDate = month + '/30/' + year;
                break;
        }
        return endDate;
    }

    calenderOperations.getEndDateOfFebruary = function(year) {
        switch (year) {
            case 2016:
            case 2020:
            case 2024:
                return '02/29/' + year;                
            case 2014:
            case 2015:
            case 2017:
            case 2018:
            case 2019:
            case 2021:
            case 2022:
            case 2023:
                return '02/28/' + year;                
        }
    }

    calenderOperations.getMonthOrDefaultFromRequest = function(requestParam1) {
        var currentDate = new Date();
        var monthToBeReturned = null;

        if (requestParam1 == null)
            monthToBeReturned = currentDate.getMonth() + 1;
        else {
            var monthFromRequest = parseInt(requestParam1);

            if (monthFromRequest == null)
                monthToBeReturned = currentDate.getMonth() + 1;
            else {
                monthToBeReturned = monthFromRequest;
            }
        }

        return monthToBeReturned;
    }

    calenderOperations.getYearOrDefaultFromRequest = function(requestParam2) {
        var currentDate = new Date();
        var yearToBeReturned = null;

        if (requestParam2 == null)
            yearToBeReturned = currentDate.getFullYear();
        else {
            var yearFromRequest = parseInt(requestParam2);

            if (yearFromRequest == null)
                yearToBeReturned = currentDate.getFullYear();
            else {
                yearToBeReturned = yearFromRequest;
            }
        }

        return yearToBeReturned;
    }

})(module.exports);
