(function(leaveCredit) {

    var dao = require('../data/dao'),
        log = require('./log'),
        executionContext = require('./executionContext'),
        util = require('./util');

    leaveCredit.getLeaveCredit = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            dao.leaveCredit.getLeaveCredit({
                id: req.param('id')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    }

    leaveCredit.getLeaveCredits = function(req, resp) {
        log.logger.info('Service : getLeaveCredits');
        util.validateAdminUser(req, resp);
        try {
            dao.leaveCredit.getLeaveCredits({
                locationId: req.param('selectedLocationId')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    leaveCredit.getLeaveBalances = function(req, resp) {
        log.logger.info('Service : getLeaveBalances');
        util.validateAdminUser(req, resp);
        try {
            dao.leaveBalance.getLeaveBalances({
                locationId: req.param('selectedLocationId')
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    leaveCredit.performMonthlyLeaveCredit = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            var user = util.getUserFromRequest(req, resp);

            dao.leaveCredit.performMonthlyLeaveCredit({
                    numberOfLeavesCredited: req.param('numberOfLeavesCredited'),
                    forceInsert: req.param('forceInsert'),
                    doNotRepeatForEmployee: req.param('doNotRepeatForEmployee'),
                    loggedInUserId: user.id
                })
                .then(function(data) {
                    util.sendSuccessResponse(data, resp);
                }, function(err) {
                    util.sendErrorResponse(err, resp);
                });
        } catch (e) {
            util.sendErrorResponse(e, resp);
        }
    };

    leaveCredit.deleteMonthlyLeaveCredit = function(req, resp) {
        util.validateAdminUser(req, resp);
        try {
            var user = util.getUserFromRequest(req, resp);

            dao.leaveCredit.deleteMonthlyLeaveCredit({
                    deleteForMonth: req.param('selectedMonth'),
                    loggedInUserId: user.id
                })
                .then(function(data) {
                    util.sendSuccessResponse(data, resp);
                }, function(err) {
                    util.sendErrorResponse(err, resp);
                });
        } catch (e) {
            util.sendErrorResponse(e, resp);
        }
    };

    leaveCredit.createLeaveCredit = function(req, res) {
        util.validateAdminUser(req, res);
        var user = util.getUserFromRequest(req, res);
        dao.leaveCredit.create({
            id: req.body.id,
            EmployeeId: req.body.EmployeeId,
            LeaveCreditTypeId: req.body.LeaveCreditType.id,
            LeaveTypeId: req.body.LeaveType.id,
            CreditedLeave: req.body.CreditedLeave,
            Description: req.body.Description,
            CreatedBy: user.id
        }).then(function(leaveCredit) {
            util.sendSuccessResponse(leaveCredit, res);
        }, function(err) {
            util.sendErrorResponse(err, res);
        });
    };

    leaveCredit.updateLeaveCredit = function(req, res) {
        util.validateAdminUser(req, res);
        var user = util.getUserFromRequest(req, res);
        try {
            dao.leaveCredit.update({
                id: req.body.id,
                EmployeeId: req.body.EmployeeId,
                LeaveCreditTypeId: req.body.LeaveCreditType.id,
                LeaveTypeId: req.body.LeaveType.id,
                CreditedLeave: req.body.CreditedLeave,
                Description: req.body.Description,
                UpdatedBy: user.id
            }).then(function(leaveCredit) {
                util.sendSuccessResponse(leaveCredit, res);
            }, function(err) {
                util.sendErrorResponse(err, res);
            });
        } catch (e) {
            util.sendErrorResponse(e, res);
        }
    };

    leaveCredit.getEmployeeLeaveAccruals = function(req, resp) {
        try {
            dao.leaveCredit.getEmployeeLeaveAccruals({
                userId: executionContext.getEffectiveUserId(req, resp)
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

    leaveCredit.getLeaveUpdatedForEmployee = function(req, resp) {
        try {
            dao.leaveCredit.getLeaveUpdatedForEmployee({
                userId: executionContext.getEffectiveUserId(req, resp)
            }).then(function(data) {
                util.sendSuccessResponse(data, resp);
            }, function(err) {
                util.sendErrorResponse(err, resp);
            });
        } catch (e) {
            util.sendErrorResponse(e, resp);;
        }
    };

})(module.exports);