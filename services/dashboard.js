(function(dashboard) {

  var dao = require('../data/dao'),
  jwt = require("jsonwebtoken"),
  util = require('./util'),
  executionContext = require('./executionContext'),
  log = require('./log');


  dashboard.GetDashboardSummaryForUser = function(req, resp) {
    log.logger.info('Service -> GetDashboardSummaryForUser');
    try {
      dao.dashboard.GetDashboardSummaryForUser({
          UserId: executionContext.getEffectiveUserId(req, resp)
        })
        .then(function(data) {
          util.sendSuccessResponse(data, resp);
        }, function(err) {
          util.sendErrorResponse(err, resp);
        });
    } catch (e) {
      util.sendErrorResponse(err, resp);;
    }

  };

})(module.exports);
