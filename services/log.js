// Logger configuration
//
// Log levels (in descending order): error, warn, info, verbose, debug, silly
//
// Example usage:
//    var logger = require('./log'); logger.
//    logger.log('debug', 'Debug message');
//    logger.log('error', 'Error message');
//    logger.error('Error message');
//    logger.info('Info message');
//    logger.warn('Warning message');


(function(service) {

  var winston = require('winston');
  //var config = require('./config');

  var logLevel = 'debug';

  var logFile = ( '../logs/node_debug.log');
  var exceptionFile = ( '../logs/exceptions.log');

  service.logger = new(winston.Logger)({
    transports: [
      new(winston.transports.Console)({
        level: logLevel,
        json: false,
        timestamp: true
      }),
      new winston.transports.File({
        level: logLevel,
        json: false,
        'timestamp': function() {return new Date().toString(); },
        filename: logFile
      })
    ],
    exceptionHandlers: [
      new(winston.transports.Console)({
        json: false,
        timestamp: true
      }),
      new winston.transports.File({
        filename: exceptionFile,
        json: false
      })
    ],
    exitOnError: false
  });


})(module.exports);
