(function(compOff) {

  var dao = require('../data/dao'),
  log = require('./log'),
  executionContext = require('./executionContext')
  util = require('./util'),
  mustache = require('mustache'),
  emailNotifications = require('./emailNotifications');

  compOff.getcompOffsToApprove = function(req, resp) {
    log.logger.info('Service : getcompOffsToApprove');
    util.validateAdminUser(req, resp);
    try {
      dao.compOff.getcompOffsToApprove(
        {locationId: req.param('selectedLocationId')}
      ).then(function(data) {
        util.sendSuccessResponse(data, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);
    }
  };

  compOff.approveCompOffs = function(req, resp) {
    log.logger.info('Service : approveCompOffs');
    util.validateAdminUser(req, resp);
    try {
      dao.compOff.approveCompOffs({
          SelectedIds: req.param('selectedIds')
        })
        .then(function(data) {
          util.sendSuccessResponse(data, resp);
        }, function(err) {
          util.sendErrorResponse(err, resp);
        });
    } catch (e) {
      util.sendErrorResponse(e, resp);
    }
  };

  compOff.updateCompOffStatus = function(req, resp) {
    log.logger.info('Service : updateCompOffStatus');
    util.validateAdminUser(req, resp);

    try {
      var user = util.getUserFromRequest(req, resp);

      dao.compOff.updateCompOffStatus({
          SelectedIds : req.param('selectedIds'),
          Status : req.param('status'),
          EmployeeId : user.id,
          Comments : req.param('comments')
        })
        .then(function(data) {
          var compOffRequestIds = req.param('selectedIds');
          dao.employee.getEmployee({id:user.id}).then(function(approvingUser){
            for (var i = 0; i < compOffRequestIds.length; i++) {
              var empDetails = dao.compOff.getEmployeeDetailsOfACompOffRequest({compOffRequestId: compOffRequestIds[i]}).then(function (emp){

                var subject = mustache.render(emailNotifications.statusChangeSubjectTemplate, {user:approvingUser.FullName, leaveType: 'comp-off', status: req.param('status')});

                var commentsByApprover = req.param('comments');
                commentsByApprover = commentsByApprover != null && commentsByApprover.length > 0 ?  commentsByApprover: 'None';
                var messageBody = mustache.render(emailNotifications.statusChangeMessageTemplate,{
                  adminUser: approvingUser.FullName,
                  status: req.param('status'),
                  number: emp[0].compOfftId,
                  comments: commentsByApprover,
                  leaveType: 'comp-off',
                  user: emp[0].FirstName
                });

                emailNotifications.sendMessage(subject, approvingUser.OfficialEmailId, [emp[0].OfficialEmailId], null, messageBody);
              });
            }
          });
          util.sendSuccessResponse(data, resp);
        }, function(err) {
          util.sendErrorResponse(err, resp);
        });
    } catch (e) {
      util.sendErrorResponse(e, resp);
    }
  };

  compOff.getEmployeeCompOffs = function(req, resp) {
    log.logger.info('compOff->getEmployeeCompOffs');

    try {
      dao.compOff.getEmployeeCompOffs(
        {userId: executionContext.getEffectiveUserId(req, resp)}
      ).then(function(data) {
        util.sendSuccessResponse(data, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);
    }
  };

  compOff.cancelCompOff = function(req, resp) {
    try {
      var user = util.getUserFromRequest(req, resp);

      dao.compOff.updateCompOffStatus({
          SelectedIds : req.param('compOffRequestId'),
          Status : 'Canceled',
          EmployeeId : user.id
        })
        .then(function(data) {
          util.sendSuccessResponse(data, resp);
        }, function(err) {
          util.sendErrorResponse(err, resp);
        });
    } catch (e) {
      util.sendErrorResponse(e, resp);
    }
  };

  compOff.GetCompOffSummaryForUser = function(req, resp) {
    log.logger.info('Service -> GetCompOffSummaryForUser');
    try {
      dao.compOff.GetCompOffSummaryForUser({
          UserId: executionContext.getEffectiveUserId(req, resp)
        })
        .then(function(data) {
          util.sendSuccessResponse(data, resp);
        }, function(err) {
          util.sendErrorResponse(err, resp);
        });
    } catch (e) {
      util.sendErrorResponse(err, resp);
    }

  };

})(module.exports);
