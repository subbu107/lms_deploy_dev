(function(workFromHome) {

  var dao = require('../data/dao'),
    log = require('./log'),
    util = require('./util');

  workFromHome.getWorkFromHomesToApprove = function(req, resp) {
    log.logger.info('Service : getWorkFromHomesToApprove');

    try {
      dao.workFromHome.getWorkFromHomesToApprove({
        locationId: req.param('locationId')
      }).then(function(data) {
        util.sendSuccessResponse(data, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);;
    }
  };

  workFromHome.updateWorkFromHomeStatus = function(req, resp) {
    log.logger.info('Service : updateWorkFromHomeStatus');

    try {
      dao.workFromHome.updateWorkFromHomeStatus({
          SelectedIds: req.param('selectedIds'),
          Status: req.param('status')
        })
        .then(function(data) {
          util.sendSuccessResponse(data, resp);
        }, function(err) {
          util.sendErrorResponse(err, resp);
        });
    } catch (e) {
      util.sendErrorResponse(e, resp);
    }
  };



  workFromHome.applyWFH = function(req, res) {
    log.logger.info('service -> applyWFH');

    try {
      var userId = util.getUserIdToBeUsed(req, res);
      log.logger.info('calling dao');
      log.logger.info('req.body.wfhDays count: ' + req.body.days.length);

      var wfhReq = {
        EmployeeId: userId,
        WFHStatusId: 0,
        Comments: req.body.comments,
        WfhDays: req.body.days,
      }

      return dao.referenceData.getWFHStatusByName({
        Name: 'Applied'
      }).
      then(function(wfhStatus) {
        wfhReq.WFHStatusId = wfhStatus[0].id;
        return dao.workFromHome.CreateWFHRequest(wfhReq);
      }).then(function(wfhRequest) {
        log.logger.info('service->created WFH request');
        util.sendSuccessResponse(wfhRequest, res);
      }, function(err) {
        log.logger.info('failed to create WFH request ' + err);
        util.sendErrorResponse(err, res);
      });
    } catch (e) {
      log.logger.info('error : ', e)
      util.sendErrorResponse(e, resp);
    }
  };

  workFromHome.getEmployeeWorkFromHomes = function(req, resp) {
    log.logger.info('workFromHome->getEmployeeWorkFromHomes');
    var userId = util.getUserIdToBeUsed(req, resp);

    try {
      dao.workFromHome.getEmployeeWorkFromHomes({
        userId: userId
      }).then(function(data) {
        util.sendSuccessResponse(data, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);;
    }
  };

  workFromHome.cancelWorkFromHome = function(req, resp) {
    try {
      var user = util.getUserFromRequest(req, resp);
      dao.workFromHome.updateWorkFromHomeStatus({
          SelectedIds: req.param('workFromHomeRequestId'),
          Status: 'Canceled',
          EmployeeId: user.id
        })
        .then(function(data) {
          util.sendSuccessResponse(data, resp);
        }, function(err) {
          util.sendErrorResponse(err, resp);
        });
    } catch (e) {
      util.sendErrorResponse(e, resp);
    }
  };

})(module.exports);
