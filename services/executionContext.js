(function(service) {

    var jwt = require("jsonwebtoken"),
        log = require('./log'),
        util = require('./util');

    service.getEffectiveUserId = function(req, resp) {
        var loggedInUser = service.getLoggedInUser(req, resp);
        var impersonatedUserIdFromRequest = -1;
        if (loggedInUser.IsAdmin) {
            impersonatedUserIdFromRequest = req.param('impersonatedUserId');
        }
        if (impersonatedUserIdFromRequest <= 0) {
            return loggedInUser.id;
        } else {
            return impersonatedUserIdFromRequest;
        }
    };

    service.isAdmin = function(req, resp) {
        var loggedInUser = service.getLoggedInUser(req, resp);
        if (loggedInUser.IsAdmin) {
            return true;
        }
        return false;
    };

    service.getLoggedInUser = function(req, resp) {
        var token = req.param('token');
        var user = jwt.decode(token);

        if (!user) {
            token = req.headers.authorization.split(' ')[1];
            user = jwt.decode(token);
        }

        if (!user) {
            return resp.status(400).send("Logged-in user data is missing.");
        } else {
            return user;
        }
    };

})(module.exports);
