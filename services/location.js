(function(location) {

  var dao = require('../data/dao'),
  log = require('./log'),
  util = require('./util');

  location.getLocations = function(req, resp) {
    log.logger.info('Service : getLocations');

    try {
      dao.location.getLocations().then(function(data) {
        util.sendSuccessResponse(data, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);;
    }
  };


  location.getLocation = function(req, resp) {
    log.logger.info('Service : getLocation');
    log.logger.info('id : ' + req.param('id'));

    try {
      log.logger.info("calling dao");

      dao.location.getLocation({
        id: req.param('id')
      }).then(function(data) {
        util.sendSuccessResponse(data, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);;
    }
  };


})(module.exports);
