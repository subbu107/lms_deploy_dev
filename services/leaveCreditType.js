(function(leaveCreditType) {

  var dao = require('../data/dao'),
  log = require('./log'),
  util = require('./util');

  leaveCreditType.getLeaveCreditTypes = function(req, resp) {
    util.validateAdminUser(req, resp);
    try {
      dao.leaveCreditType.getLeaveCreditTypes().then(function(data) {
        util.sendSuccessResponse(data, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);;
    }
  };

})(module.exports);
