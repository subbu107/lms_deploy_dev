(function(referenceData) {
  var dao = require('../data/dao'),
    util = require('./util'),
    log = require('./log');

  referenceData.getleaveTypesWithBalance = function(req, resp) {
    var user = util.getUserFromRequest(req, resp);
    var employeeId = util.getUserIdToBeUsed(req, resp);
    try {
      dao.referenceData.getleaveTypesWithBalance({
        EmployeeId: employeeId,
        IsAdmin: user.IsAdmin,
        AdminEmployeeId: user.id
      }).then(function(leaveTypes) {
        util.sendSuccessResponse(leaveTypes, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);
    }

  }
})(module.exports);
