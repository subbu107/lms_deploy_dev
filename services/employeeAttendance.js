(function (employeeAttendance) {

  var dao = require('../data/dao'),
    log = require('./log'),
    util = require('./util');
  log = require('./log');

  employeeAttendance.hasUserLoggedInToday = function (req, resp) {

    try {
      console.log('getUserAttendance:hitting Before');

      dao.employeeAttendance.hasUserLoggedInToday({
        req: req.param('NTLogin')
      }).then(function (data) {
        if (data != '') {
          var dataResponse = {
            "hasUserLoggedIn": true
          };
          util.sendSuccessResponse(dataResponse, resp)
        }
        else {
          var dataResponse = {
            "hasUserLoggedIn": false
          };
          util.sendSuccessResponse(dataResponse, resp)
        }
        //util.sendSuccessResponse(data, resp);
        console.log('getUserAttendance:hitting' + data);

      }, function (err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);;
    }
  };

  employeeAttendance.markAttendance = function (req, res) {
    var user = util.getUserFromRequest(req, res);
    //var currentDate =  new Date();
    var data = {
      EmpId: user.id,
      FirstHalf: req.body.FirstHalf,
      SecondHalf: req.body.SecondHalf,
      Comments: req.body.Comments,
      Date: req.body.Date,
      CreatedOn: req.body.CreatedOn,
      CreatedBy: user.id
    };
    try {
      log.logger.info('markAttendance for User' + JSON.stringify(data));
      dao.employeeAttendance.markAttendance(data).then(function (data) {
        util.sendSuccessResponse(data, res)
      }, function (err) {
        util.sendErrorResponse(err);
      });
    } catch (e) {
      util.sendErrorResponse(e, res);
    }
  };

  employeeAttendance.getAttendance = function (req, res) {
    var user = util.getUserFromRequest(req, res);
    //var currentDate=new Date();
    try {
      console.log('getAttendance:hitting Before');

      dao.employeeAttendance.getAttendance({
        id: user.id,
        Date: req.param('Date')
      }).then(function (data) {
        if (data != '') {
          util.sendSuccessResponse(data, res)
        }
        else {
          var dataResponse = {
            "hasUserLoggedIn": false
          };
          util.sendSuccessResponse(dataResponse, res)
        }
        //util.sendSuccessResponse(data, resp);
        console.log('getAttendance:hitting' + data);

      }, function (err) {
        util.sendErrorResponse(err, res);
      });
    } catch (e) {
      util.sendErrorResponse(e, res);;
    }
  };

  employeeAttendance.updateAttendance = function (req, res) {
    var user = util.getUserFromRequest(req, res);
    var data = {
      EmpId: user.id,
      FirstHalf: req.body.FirstHalf,
      SecondHalf: req.body.SecondHalf,
      Comments: req.body.Comments,
      Date: req.body.Date,
      CreatedOn: req.body.CreatedOn,
      CreatedBy: user.id,
      UpdatedBy: user.id
    };
    try {
      log.logger.info('updateAttendance for User' + JSON.stringify(data));
      dao.employeeAttendance.updateAttendance(data).then(function (data) {
        util.sendSuccessResponse(data, res)
      }, function (err) {
        util.sendErrorResponse(err);
      });
    } catch (e) {
      util.sendErrorResponse(e, res);
    }
  };

})(module.exports);
