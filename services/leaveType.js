(function(leaveType) {

  var dao = require('../data/dao'),
  log = require('./log'),
  util = require('./util');

  leaveType.getLeaveTypes = function(req, resp) {
    try {
      dao.leaveType.getLeaveTypes().then(function(data) {
        util.sendSuccessResponse(data, resp);
      }, function(err) {
        util.sendErrorResponse(err, resp);
      });
    } catch (e) {
      util.sendErrorResponse(e, resp);;
    }
  };

})(module.exports);
