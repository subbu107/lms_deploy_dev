(function(config){
    var log = require('../../services/log');
    var cls = require('continuation-local-storage'),
      namespace = cls.createNamespace('my-very-own-namespace');

    config.Sequelize = require("sequelize");

    config.Sequelize.cls = namespace;

    config.sequelize = new config.Sequelize('LMS_DEV', 'lmsdev', 'welcome@0', {
      host: 'lmsdev.database.windows.net',
      dialect: 'mssql',
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
	   dialectOptions: {
        encrypt: true,
        timeout:100
        },
      logging : function (str) {
        log.logger.info("SEQUELIZE LOG : [" + str +"]");
      }
    });
})(module.exports);