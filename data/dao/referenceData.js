(function(referenceData) {

  var model = require('../model');
  var config = require('../config');
  var log = require('../../services/log');


  referenceData.getleaveTypesWithBalance = function(params) {
    log.logger.info('dao->getleaveTypesWithBalance');

    return config.sequelize.query("exec pr_GetLeaveTypesForUser @p_empId = ?, @p_isAdmin = ?, @p_adminEmployeeId = ?", {
      replacements: [params.EmployeeId, params.IsAdmin, params.AdminEmployeeId ],
      type: config.sequelize.QueryTypes.SELECT
    });
  };

  referenceData.getLeaveTypeByName = function(params) {
    return model.LeaveType.findAll({
      where: {
        Name: params.Name
      }
    });
  };

  referenceData.getLeaveStatusByName = function(params) {
    return model.LeaveStatus.findAll({
      where: {
        Name: params.Name
      }
    });
  };

  referenceData.getCompOffStatusByName = function(params) {
    return model.CompOffStatus.findAll({
      where: {
        Name: params.Name
      }
    });
  };

  referenceData.getWFHStatusByName = function(params) {
    return model.WFHStatus.findAll({
      where: {
        Name: params.Name
      }
    });
  };

})(module.exports);
