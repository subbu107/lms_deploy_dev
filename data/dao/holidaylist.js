(function (holidaylist) {

  var model = require('../model');
  var config = require('../config');
  var log = require('../../services/log');


  holidaylist.getHolidaysForUser = function (params) {
    log.logger.info('dao->getHolidaysForUserWithPromises userId : ' + params.UserId);

    return config.sequelize.transaction(function (t) {
      return model.Employee.findById(params.UserId).then(function (employee) {
        return employee.getLocation();
      }).then(function (location) {
        return location.getHolidays({
          where: ['Year = ' + new Date().getFullYear()]
        });
      });
    });
  };


  holidaylist.getSelectedRestrictedHolidaysForUser = function (params) {
    log.logger.info('dao->getSelectedRestrictedHolidaysForUser userId : ' + params.UserId);

    return model.Employee.findById(params.UserId).then(function (employee) {
      return employee;
    }).then(function (employee) {
      log.logger.info('got employee');
      return model.EmployeeRestrictedHoliday.findOne({
        where: ['EmployeeId = ' + employee.id]
      });
    }).then(function (employeeRestrictedHoliday) {
      
      if (employeeRestrictedHoliday) {
        return model.RestrictedHoliday.findById(employeeRestrictedHoliday.RestrictedHolidayId);
      } else {
        return null;
      }
    });
  };

  holidaylist.setRestrictedHolidayForUser = function (params) {
    log.logger.info('dao->setRestrictedHolidayForUser userId : ' + params.UserId);
    log.logger.info('dao->setRestrictedHolidayForUser holiday : ' + params.holiday);

    return model.EmployeeRestrictedHoliday.create({
      EmployeeId: params.UserId,
      RestrictedHolidayId: params.RestrictedHolidayId,
      CreatedBy : params.UserId
    });

  };

  holidaylist.getAllHolidaysForLocation = function (params) {

    return config.sequelize.transaction(function (t) {
      return model.Location.findAll({
        where: {
          Name: params.LocationName
        }
      }).then(function (locations) {
        return locations[0].getHolidays({
          where: ['Year = ' + params.Year]
        });
      });
    });
  };

  holidaylist.getRecentHolidaysForUser = function (params) {

    var query = [];
    query.push(" select top 3 H.Name, H.Description, H.ActualDate");
    query.push(" from Holiday H ");
    query.push(" join LocationHoliday LH on LH.HolidayId = H.id ");
    query.push(" join Employee E on E.LocationId = LH.LocationId ");
    query.push(" where E.id = ? ");
    query.push(" and H.ActualDate  >=  GETDATE() ");
    query.push(" order by H.ActualDate ");

    return config.sequelize.transaction(function (t) {
      return config.sequelize.query(query.join(" "), {
        replacements: [params.UserId],
        type: config.sequelize.QueryTypes.SELECT
      });
    });
  };

})(module.exports);