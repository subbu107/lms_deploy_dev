(function (employeeLeave) {

    var model = require('../model');
    var dateFormat = require('dateformat');
    var config = require('../config');
    var log = require('../../services/log');
    var js2xmlparser = require("js2xmlparser");

    employeeLeave.getEmployeeWithLeaveHistory = function (params) {
        var leaveHistoryDetails = [];

        return config.sequelize.transaction(function (t) {

            return model.Employee.findById(params.UserId, {
                include: [{
                    model: model.LeaveRequest,
                    as: 'LeaveRequests',
                    include: [{
                        model: model.LeaveStatus,
                        as: 'LeaveStatus'
                    }, {
                        model: model.LeaveType,
                        as: 'LeaveType'
                    }, {
                        model: model.LeaveDuration,
                        as: 'LeaveDurations'
                    }]
                }]
            });
        });

    };


    employeeLeave.CreateLeaveRequest = function (leaveReq) {

        var formattedLeaveDays = [];
        leaveReq.LeaveDays.forEach(function (leaveDay) {
            var formattedLeaveDay = {
                LeaveDay: leaveDay.leaveDay,
                IsHalfDay: leaveDay.isHalfDay
            };

            if (leaveDay.compOffs && leaveDay.compOffs.length >= 1)
                formattedLeaveDay.CompOff1 = leaveDay.compOffs[0].CompOffDurationId;

            if (leaveDay.compOffs && leaveDay.compOffs.length == 2)
                formattedLeaveDay.CompOff2 = leaveDay.compOffs[1].CompOffDurationId;

            formattedLeaveDays.push(formattedLeaveDay);

        });
        leaveReq.LeaveDays = formattedLeaveDays;

        var requestXml = js2xmlparser("LeaveRequest", leaveReq);

        return config.sequelize.transaction(function (t) {
            return config.sequelize.query('exec [dbo].[pr_ApplyLeave] @p_requestXml=?, @p_empId=?, @p_isAdminRequest=?', {
                replacements: [requestXml, leaveReq.EmployeeId, leaveReq.IsAdminRequest],
                type: config.sequelize.QueryTypes.SELECT
            });
        });

    };

    employeeLeave.GetValidDays = function (validDaysReq) {
        return config.sequelize.transaction(function (t) {
            return config.sequelize.query('exec [dbo].[pr_GetValidDaysForUser] @p_empId=?, @p_startDate=?, @p_endDate=?, @p_leaveType=?', {
                replacements: [validDaysReq.EmployeeId, dateFormat(validDaysReq.StartDate, 'mm-dd-yyyy'), dateFormat(validDaysReq.EndDate, 'mm-dd-yyyy'), validDaysReq.LeaveType],
                type: config.sequelize.QueryTypes.SELECT
            });
        });
    };



    employeeLeave.GetValidCompOffDays = function (validCompOffDaysReq) {
        return config.sequelize.transaction(function (t) {
            return config.sequelize.query('exec [dbo].[pr_GetValidCompOffDaysForUser] @p_empId=?, @p_startDate=?, @p_endDate=?', {
                replacements: [validCompOffDaysReq.EmployeeId, dateFormat(validCompOffDaysReq.StartDate, 'mm-dd-yyyy'), dateFormat(validCompOffDaysReq.EndDate, 'mm-dd-yyyy')],
                type: config.sequelize.QueryTypes.SELECT
            });
        });
    };


    employeeLeave.getEmployeeCompOffs = function (getCompOffReq) {
        return config.sequelize.transaction(function (t) {
            return config.sequelize.query('exec [dbo].[pr_GetAvailableCompOffsForUser] @p_empId=?', {
                replacements: [getCompOffReq.EmployeeId],
                type: config.sequelize.QueryTypes.SELECT
            });
        });
    };


    employeeLeave.GetEmployeeLeaveBalance = function (leaveBalReq) {
        return config.sequelize.transaction(function (t) {
            return config.sequelize.query('select AvailableBalance as LeaveBalanceForType from LeaveBalance where EmployeeId = ? and LeaveTypeId=?', {
                replacements: [leaveBalReq.EmployeeId, leaveBalReq.LeaveTypeId],
                type: config.sequelize.QueryTypes.SELECT
            });
        });

    };

    employeeLeave.CreateCompOffRequest = function (compOffReq) {
        var requestXml = js2xmlparser("CompOffRequest", compOffReq);

        return config.sequelize.transaction(function (t) {
            return config.sequelize.query('exec [dbo].[pr_ApplyCompOff] @p_requestXml=?, @p_empId=?, @p_isAdminRequest=?', {
                replacements: [requestXml, compOffReq.EmployeeId, compOffReq.IsAdminRequest],
                type: config.sequelize.QueryTypes.SELECT
            });
        });
    };

    employeeLeave.getRecentLeaveHistory = function (params) {
        var query = [];
        query.push(" select top 5 LD.LeaveRequestId as Id, ");
        query.push(" convert(varchar(10), MIN(LD.LeaveDate), 110) as FromDate , convert(varchar(10), MAX(LD.LeaveDate), 110) as ToDate,  ");
        query.push(" SUM(LD.LeaveHours) as TotalLeaveHours, CONVERT(DECIMAL(3,1), SUM(LD.LeaveHours)/8.0) as LeaveDays, MIN(LR.Comments) as Comments, ");
        query.push(" MIN(LT.Name) as LeaveType, MIN(LS.Name) as Status ");
        query.push(" from LeaveDuration LD ");
        query.push(" join LeaveRequest LR on LD.LeaveRequestId = LR.id ");
        query.push(" join LeaveStatus LS on LR.LeaveStatusId = LS.id ");
        query.push(" join LeaveType LT on LR.LeaveTypeId = LT.id ");
        query.push(" join Employee E on LR.EmployeeId = E.id and LR.EmployeeId = ?");
        query.push(" where LS.Name NOT IN ('Canceled') ");
        query.push(" group by LD.LeaveRequestId ");
        query.push(" order by MIN(LD.LeaveDate) desc ")


        return config.sequelize.query(query.join(" "), {
            replacements: [params.UserId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };


    employeeLeave.GetLeaveDurationsForEmployeeOnADay = function (params) {
        return model.LeaveDuration.findAll({
            where: ['convert(varchar(10),LeaveDate, 110) = ' + "'" + params.DateToCheck.toString() + "'"],
        });
    };

    employeeLeave.GetLeaveRequestByIds = function (params) {
        return model.LeaveRequest.findAll({
            where: {
                id: params,
                LeaveStatusId: [1,2] 
            },
            include: [{
                model: model.LeaveDuration,
                as: 'LeaveDurations'
            }]
        });
    };

})(module.exports);