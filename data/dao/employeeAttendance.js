(function (employeeAttendance) {

  var model = require('../model');
  var config = require('../config');
  var log = require('../../services/log');
  var executionContext = require('./executionContext');

  employeeAttendance.hasUserLoggedInToday = function (params) {

    return model.EmployeeLatestAttendance.findAll({
      where: {
        NTLogin: params.req
      },
    });

  };

  employeeAttendance.markAttendance = function (params) {
    log.logger.info('dao->markAttendance');
    return config.sequelize.transaction(function (t) {
      return model.EmployeeAttendance.create({
        EmployeeId: params.EmpId,
        Date: params.Date,
        FirstHalf: params.FirstHalf,
        SecondHalf: params.SecondHalf,
        CreatedBy: params.EmpId,
        Comments: params.Comments,
        CreatedBy: params.CreatedBy,
        UpdatedBy: params.UpdatedBy,
        CreatedOn: params.CreatedOn
      });
    });
  };

  employeeAttendance.updateAttendance = function (params) {
    log.logger.info('dao->updateAttendance');
    return config.sequelize.transaction(function (t) {
      //findBy EmpId and Date should needed for the attendance of user for the day
      return model.EmployeeAttendance.findOne({
        where: {
          EmployeeId: params.EmpId,
          Date: params.Date 
        }
      }).then(function (employeeAttendance) {
        return employeeAttendance.update({
          EmployeeId: params.EmpId,
          Date: params.Date,
          FirstHalf: params.FirstHalf,
          SecondHalf: params.SecondHalf,
          CreatedBy: params.EmpId,
          Comments: params.Comments,
          CreatedBy: params.CreatedBy,
          UpdatedBy: params.UpdatedBy,
          CreatedOn: params.CreatedOn
        });
      });
    });
  }

  employeeAttendance.getAttendance = function (params) {
    return model.EmployeeAttendance.findOne({
      where: { $and: { EmployeeId: params.id, Date: params.Date } },
    });
  };
})(module.exports);
