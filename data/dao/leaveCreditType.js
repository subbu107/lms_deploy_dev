(function(leaveCreditType) {

  var model = require('../model');
  var config = require('../config');

  leaveCreditType.getLeaveCreditTypes = function(params) {
    return model.LeaveCreditType.findAll();
  };

  leaveCreditType.getleaveCreditType = function(params) {
    return model.LeaveCreditType.findById(params.id);
  };
})(module.exports);
