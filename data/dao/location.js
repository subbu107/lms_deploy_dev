(function(location) {

  var model = require('../model');
  var config = require('../config');

  location.getLocations = function(params) {
    return model.Location.findAll();
  };

  location.getLocation = function(params) {
    return model.Location.findById(params.id);
  };
})(module.exports);
