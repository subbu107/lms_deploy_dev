(function(compOff) {

    var model = require('../model');
    var config = require('../config');
    var log = require('../../services/log');

    compOff.getcompOffsToApprove = function(params) {
        var query = [];
        query.push("select CD.CompOffRequestId as Id, ");
        query.push(" MIN(CR.CreatedOn) as AppliedOn, MIN(CD.CompOffDate) as FromDate , MAX(CD.CompOffDate) as ToDate, ");
        query.push(" MIN(E.Id) as EmployeeId, MIN(E.FirstName) as FirstName, MIN(E.LastName) as LastName, ");
        query.push(" MIN(CR.Comments) as Comments, SUM(CD.CompOffHours) as TotalCompOffHours, ");
        query.push(" MIN(CS.Name) as Status ");
        query.push(" from CompOffDuration CD ");
        query.push(" join CompOffRequest CR on CD.CompOffRequestId = CR.id ");
        query.push(" join CompOffStatus CS on CR.CompOffStatusId = CS.id ");
        query.push(" join Employee E on CR.EmployeeId = E.id ");
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");
        query.push(" group by CD.CompOffRequestId ");
        query.push(" order by CD.CompOffRequestId DESC");
        return config.sequelize.query(query.join(" "), {
            replacements: [params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    compOff.approveCompOffs = function(params) {

        var query = [];
        query.push("update CompOffRequest set CompOffStatusId = ");
        query.push(" (Select id from CompOffStatus where Name = 'Approved') ")
        query.push(" where id in (");
        query.push(params.SelectedIds);
        query.push(")");
        return config.sequelize.transaction(function(t) {
            return config.sequelize.query(query.join(" "), {
                replacements: [params.selectedIds]
            });
        });
    };

    compOff.updateCompOffStatus = function(params) {
        log.logger.info("dao->updateCompOffStatus");
        return config.sequelize.transaction(function(t) {
            return config.sequelize.query('exec pr_UpdateCompOffStatus 	@p_CompOffRequestIds = ?, @p_Status = ?, @p_EmployeeId = ?, @p_Comments =?', {
                replacements: [String(params.SelectedIds), params.Status, params.EmployeeId, params.Comments],
                type: config.sequelize.QueryTypes.UPDATE
            });
        });
    };

    compOff.getEmployeeDetailsOfACompOffRequest = function (params){
      log.logger.info('dao->getEmployeeDetailsOfACompOffRequest');

      var query = [];
      query.push(" select emp.FirstName, emp.LastName, emp.OfficialEmailId, cr.id as compOfftId ");
      query.push(" from Employee emp, CompOffRequest cr ");
      query.push(" where cr.id = ? and emp.id = cr.EmployeeId ");

      return config.sequelize.query(query.join(" "), {
          replacements: [params.compOffRequestId],
          type: config.sequelize.QueryTypes.SELECT
      });
    };

    compOff.getEmployeeCompOffs = function(params) {
        log.logger.info('dao->getEmployeeCompOffs');

        var query = [];
        query.push(" select MIN(CD.CompOffRequestId) as RequestId, MIN(CR.CreatedOn) as AppliedOn, Min(CompOffDate) as CompOffDate, MIN(ExpiresOn) as ExpiresOn, ");
        query.push(" SUM(CD.CompOffHours) as TotalCompOffHours, MIN(CS.Name) as Status, MIN(CR.Comments) as Comments, ");
        query.push(" MIN(LD.LeaveDate) as AvailedOnDateMin, MAX(LD.LeaveDate) as AvailedOnDateMax ");
        query.push(" from CompOffDuration CD ");
        query.push(" join CompOffRequest CR on CD.CompOffRequestId = CR.id and CR.EmployeeId = ? ");
        query.push(" join CompOffStatus CS on CR.CompOffStatusId = CS.id ");
        query.push(" left join LeaveCompOff LC on LC.CompOffDurationId = CD.id ");
        query.push(" left join LeaveDuration LD on LD.id = LC.LeaveDurationId ");
        query.push(" group by CD.CompOffRequestId, CD.CompOffDate ");
        query.push(" order by CD.CompOffRequestId desc ");

        return config.sequelize.query(query.join(" "), {
            replacements: [params.userId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    compOff.GetCompOffSummaryForUser = function(params) {
        return config.sequelize.query('exec pr_GetCompOffSummaryForUser @p_empId=?', {
            replacements: [params.UserId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

})(module.exports);
