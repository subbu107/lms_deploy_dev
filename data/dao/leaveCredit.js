(function(leaveCredit) {

    var model = require('../model');
    var config = require('../config');
    var log = require('../../services/log');
    var executionContext = require('./executionContext');

    leaveCredit.getLeaveCredit = function(params) {
        return model.LeaveCredit.findById(params.id, {
            include: [model.LeaveType, model.LeaveCreditType, model.Employee]
        });
    };

    leaveCredit.getLeaveCredits = function(params) {
        var employeeWhereClause = executionContext.GetLocationWhereClause(params);

        return model.LeaveCredit.findAll({
            include: [
                model.LeaveType,
                model.LeaveCreditType, {
                    model: model.Employee,
                    as: 'Employee',
                    where: employeeWhereClause
                }
            ],
            order: 'CreatedOn DESC'
        });
    };

    leaveCredit.getLeaveCreditsByEmployee = function(params) {
        return model.LeaveCredit.findAll({
            where: {
                EmployeeId: params.EmployeeId
            }
        });
    };

    leaveCredit.performMonthlyLeaveCredit = function(params) {
        log.logger.info("dao->monthlyLeaveCredit");

        return config.sequelize.transaction(function(t) {
            return config.sequelize.query('exec pr_PerformMonthlyLeaveCredit @p_NumberOfLeavesCredited = ?, @p_ForceInsert = ?, @p_DoNotRepeatForEmployee=?, @p_LoggedInUserId = ?', {
                replacements: [params.numberOfLeavesCredited, params.forceInsert, params.doNotRepeatForEmployee, params.loggedInUserId],
                type: config.sequelize.QueryTypes.UPDATE
            });
        });
    };

    leaveCredit.deleteMonthlyLeaveCredit = function(params) {
        log.logger.info("dao->deleteMonthlyLeaveCredit");

        return config.sequelize.transaction(function(t) {
            return config.sequelize.query('exec pr_DeleteMonthlyLeaveCredit @p_DeleteForMonth =?,	@p_LoggedInUserId = ?', {
                replacements: [params.deleteForMonth, params.loggedInUserId],
                type: config.sequelize.QueryTypes.UPDATE
            });
        });
    };

    leaveCredit.create = function(params) {
        log.logger.info("dao->create");

        return config.sequelize.transaction(function(t) {
            return config.sequelize.query('exec pr_AddOrUpdateLeaveCredit	@p_IsAddOperation = ?, @p_Id = ?, @p_EmployeeId = ?, @p_LeaveCreditTypeId= ?, @p_LeaveTypeId = ?, @p_CreditedLeave = ?, @p_Description = ?, @p_LoggedInUserId = ?', {
                replacements: [true, params.id, params.EmployeeId, params.LeaveCreditTypeId, params.LeaveTypeId, params.CreditedLeave, params.Description, params.CreatedBy],
                type: config.sequelize.QueryTypes.UPDATE
            });
        });
};

    leaveCredit.update = function(params) {
        log.logger.info("dao->update");

        return config.sequelize.transaction(function(t) {
            return config.sequelize.query('exec pr_AddOrUpdateLeaveCredit	@p_IsAddOperation = ?, @p_Id = ?, @p_EmployeeId = ?, @p_LeaveCreditTypeId= ?, @p_LeaveTypeId = ?, @p_CreditedLeave = ?, @p_Description = ?, @p_LoggedInUserId = ?', {
                replacements: [false, params.id, params.EmployeeId, params.LeaveCreditTypeId, params.LeaveTypeId, params.CreditedLeave, params.Description, params.UpdatedBy],
                type: config.sequelize.QueryTypes.UPDATE
            });
        });
    };

    leaveCredit.getEmployeeLeaveAccruals = function(params) {
        log.logger.info('dao->getEmployeeLeaveAccruals');

        var query = [];
        query.push(" select LC.id, LC.CreatedOn, LT.Name as 'LeaveType', LCT.Name as 'LeaveCreditType', ");
        query.push(" LC.EmployeeId, LC.CreditedLeave, LC.Description ");
        query.push(" from LeaveCredit LC ");
        query.push(" join LeaveCreditType LCT on LC.LeaveCreditTypeId = LCT.id and LC.EmployeeId = ? ");
        query.push(" join LeaveType LT on LC.LeaveTypeId = LT.id ");
        query.push(" order by LC.CreatedOn desc ");

        return config.sequelize.query(query.join(" "), {
            replacements: [params.userId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };


    leaveCredit.getLeaveUpdatedForEmployee = function(params) {
        log.logger.info('dao->getLeaveUpdatedForEmployee');

        var query = [];

        query.push(" select Balance, Encashable, Expiring, BalanceAfterEncashment");
        query.push(" from LeaveEncashmentReportTable lert inner join Employee e on lert.employeeid = e.id");
        query.push(" where EmployeeId = ? and e.employeestatusid <> 2 and year(e.DateOfJoining) <> 2017");

        return config.sequelize.query(query.join(" "), {
            replacements: [params.userId, params.userId, params.userId, params.userId],
            type: config.sequelize.QueryTypes.SELECT
        });
    }

})(module.exports);