(function(workFromHome) {

  var model = require('../model');
  var config = require('../config');
  var log = require('../../services/log');
  var dateFormat = require('dateformat');
  var js2xmlparser = require("js2xmlparser");


  workFromHome.getWorkFromHomesToApprove = function(params) {
    log.logger.info('dao->getWorkFromHomesToApprove');
    var query = [];
    query.push("select CD.WorkFromHomeRequestId as Id, ");
    query.push(" convert(varchar(10), MIN(CD.WorkFromHomeDate), 110) as FromDate , convert(varchar(10), MAX(CD.WorkFromHomeDate), 110) as ToDate, ");
    query.push(" MIN(E.Id) as EmployeeId, MIN(E.FirstName) as FirstName, MIN(E.LastName) as LastName, ");
    query.push(" MIN(CR.Comments) as Comments, SUM(CD.WorkFromHomeHours) as TotalWorkFromHomeHours, ");
    query.push(" MIN(CS.Name) as Status ");
    query.push(" from WorkFromHomeDuration CD ");
    query.push(" join WorkFromHomeRequest CR on CD.WorkFromHomeRequestId = CR.id ");
    query.push(" join WorkFromHomeStatus CS on CR.WorkFromHomeStatusId = CS.id ");
    query.push(" join Employee E on CR.EmployeeId = E.id ");
    if (params.locationId > 0)
      query.push(" and E.locationId =? ");
    query.push(" where CS.Name not in ('Canceled') ");
    query.push(" group by CD.WorkFromHomeRequestId ");
    query.push(" order by CD.WorkFromHomeRequestId ");
    return config.sequelize.query(query.join(" "), {
      replacements: [params.locationId],
      type: config.sequelize.QueryTypes.SELECT
    });
  };

  workFromHome.updateWorkFromHomeStatus = function(params) {
    log.logger.info("dao->updateWorkFromHomeStatus");
    return config.sequelize.transaction(function(t) {
      return config.sequelize.query('exec pr_UpdateWorkFromHomeStatus 	@p_WorkFromHomeRequestIds = ?, @p_Status = ?, @p_EmployeeId = ?', {
        replacements: [params.SelectedIds, params.Status, params.EmployeeId],
        type: config.sequelize.QueryTypes.UPDATE
      });
    });
  };


  workFromHome.CreateWFHRequest = function(wfhReq) {
    log.logger.info('compoff JSON string : ['+JSON.stringify(wfhReq)+']');

    var requestXml = js2xmlparser("WfhRequest", wfhReq);
    log.logger.info('compoff XML string : ['+requestXml+']');

    return config.sequelize.transaction(function(t) {
      return config.sequelize.query('exec [dbo].[pr_ApplyWfh] @p_requestXml=?, @p_empId=?, @p_isAdminRequest=?', {
        replacements: [requestXml, wfhReq.EmployeeId, wfhReq.IsAdminRequest],
        type: config.sequelize.QueryTypes.SELECT
      });
    });

  };




  // workFromHome.CreateWFHRequest = function(wfhReq) {
  //
  //   log.logger.info('dao->CreateWFHRequest');
  //   log.logger.info('params.employeeName' + wfhReq.EmployeeName);
  //   log.logger.info('req: ' + JSON.stringify(wfhReq));
  //
  //   return config.sequelize.transaction(function(t) {
  //     return model.WFHRequest.create({
  //       EmployeeId: wfhReq.EmployeeId,
  //       WFHStatusId: wfhReq.WFHStatusId,
  //       Comments: wfhReq.Comments,
  //       CreatedOn: dateFormat(new Date(), "mm-dd-yyyy hh:MM:ss"),
  //       CreatedBy: wfhReq.EmployeeId
  //     }).then(function(wfhRequest) {
  //
  //       log.logger.info('inserting into wfhReqDuration with wfhReqRequest linked');
  //       log.logger.info('wfhReq.id : ' + wfhRequest.id);
  //       log.logger.info('WorkFromHomeRequest.EmployeeId :' + wfhRequest.EmployeeId);
  //
  //
  //       wfhReq.Days.forEach(function(wfhDay) {
  //         log.logger.info("wfh day : " + wfhDay.day);
  //         log.logger.info("wfh half day : " + wfhDay.isHalfDay);
  //         var wfhDurationsToCreate = wfhDay.isHalfDay ? 1 : 2;
  //
  //         for (var i = 0; i < wfhDurationsToCreate; i++) {
  //
  //           model.WFHDuration.create({
  //             WFHRequestId: wfhRequest.id,
  //             WFHDate: dateFormat(wfhDay.day, "mm-dd-yyyy"),
  //             WFHHours: 4,
  //             CreatedBy: wfhRequest.CreatedBy,
  //             CreatedOn: dateFormat(wfhRequest.CreatedOn, "mm-dd-yyyy hh:MM:ss"),
  //           });
  //         }
  //       });
  //
  //       model.WFHRequestStatusHistory.create({
  //         WFHRequestId: wfhRequest.id,
  //         WFHStatusId: wfhRequest.WFHStatusId,
  //         CreatedBy: wfhRequest.CreatedBy,
  //         CreatedOn: dateFormat(wfhRequest.CreatedOn, "mm-dd-yyyy hh:MM:ss")
  //       });
  //     });
  //   });
  // };

  workFromHome.getEmployeeWorkFromHomes = function(params) {
    log.logger.info('dao->getEmployeeWorkFromHome');

    var query = [];
    query.push("select WD.WorkFromHomeRequestId as Id, ");
    query.push(" convert(varchar(10), MIN(WD.WorkFromHomeDate), 110) as FromDate , convert(varchar(10), MAX(WD.WorkFromHomeDate), 110) as ToDate, ");
    query.push(" MIN(WR.Comments) as Comments, SUM(WD.WorkFromHomeHours) as TotalWorkFromHomeHours, ");
    query.push(" MIN(CS.Name) as Status  ");
    query.push(" from WorkFromHomeDuration WD ");
    query.push(" join WorkFromHomeRequest WR on WD.WorkFromHomeRequestId = WR.id  ");
    query.push(" join WorkFromHomeStatus CS on WR.WorkFromHomeStatusId = CS.id and WR.EmployeeId = ?");
    query.push(" group by WD.WorkFromHomeRequestId ");
    query.push(" order by WD.WorkFromHomeRequestId desc ");

    return config.sequelize.query(query.join(" "), {
      replacements: [params.userId],
      type: config.sequelize.QueryTypes.SELECT
    });
  };

})(module.exports);
