(function(employee) {

    var model = require('../model');
    var config = require('../config');
    var log = require('../../services/log');
    var executionContext = require('./executionContext');

    employee.getEmployee = function(params) {
        return model.Employee.findById(params.id, {
            include: [model.Location, model.EmployeeStatus]
        });
    };

    employee.getEmployees = function(params) {
        var employeeWhereClause = executionContext.GetLocationWhereClause(params);

        return model.Employee.findAll({
            where: employeeWhereClause,
            include: [model.Location, model.EmployeeStatus]
        });
    };

    employee.getEmployeeStatuses = function(params) {
        return model.EmployeeStatus.findAll();
    };


    employee.getEmployeeByUserName = function(params) {
        log.logger.info('dao->getEmployeeByUserName');
        return model.Employee.findAll({
            where: {
                UserName: params.UserName
            },
            include: [model.EmployeeStatus]
        });
    };

employee.getEmployeeByNTLogin = function(params) {
        log.logger.info('dao->getEmployeeByNTLogin');
         return config.sequelize.query(" SELECT Attendance FROM EmployeeAttendance INNER JOIN Employee ON EmployeeAttendance.EmployeeId=Employee.id where Employee.id=(select id from Employee where @p_NTLogin=? )", {
            replacements: [params.NTlogin],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    employee.getEmployeeMinimalByUserName = function(params) {
        log.logger.info('dao->getEmployeeMinimalByUserName');
        return model.EmployeeMinimal.findAll({
            where: {
                UserName: params.UserName
            },
            include: [model.EmployeeStatusMinimal]
        });
    };

    employee.getActiveEmployeesEmails = function(param) {
        log.logger.info('dao->getActiveEmployeesEmails');
        return model.Employee.findAll({
            where: {$and: { EmployeeStatusId : 1, id: {$ne : param.id } } },
            attributes: ['Id', 'FirstName','LastName','OfficialEmailId'],
            order: 'FirstName ASC'
        });
    };

    employee.searchEmployee = function(params) {
        log.logger.info('dao->searchEmployee');
        log.logger.info('search string : ' + params.SearchString);

        return config.sequelize.query("exec [dbo].[pr_SearchEmployee] @p_searchString=?", {
            replacements: [params.SearchString],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    employee.create = function(params) {

        log.logger.info('dao->createEmployee');
        return config.sequelize.transaction(function(t) {
            return model.Employee.create({
                id: params.id,
                FirstName: params.FirstName,
                MiddleName: params.MiddleName,
                LastName: params.LastName,
                UserName: params.UserName,
                LocationId: params.LocationId,
                EmployeeStatusId: params.EmployeeStatusId,
                IsAdmin: params.IsAdmin,
                IsFemale: params.IsFemale,
                DateOfBirth: params.DateOfBirth,
                DateOfJoining: params.DateOfJoining,
                DateOfExit: params.DateOfExit,
                CreatedBy: params.CreatedBy,
                OfficialEmailId: params.OfficialEmailId,
                NTlogin:params.NTlogin
            });
        });
    };

    employee.update = function(params) {
        log.logger.info('dao->updateEmployee');

        return config.sequelize.transaction(function(t) {
            return model.Employee.findById(params.id).then(function(employee) {
                log.logger.info("retrieved employee " + JSON.stringify(employee));
                return employee.update({
                    id: params.id,
                    FirstName: params.FirstName,
                    MiddleName: params.MiddleName,
                    LastName: params.LastName,
                    UserName: params.UserName,
                    LocationId: params.LocationId,
                    EmployeeStatusId: params.EmployeeStatusId,
                    IsAdmin: params.IsAdmin,
                    IsFemale: params.IsFemale,
                    DateOfBirth: params.DateOfBirth,
                    DateOfJoining: params.DateOfJoining,
                    DateOfExit: params.DateOfExit,
                    UpdatedBy: params.UpdatedBy,
                    OfficialEmailId: params.OfficialEmailId,
                    NTlogin:params.NTlogin
                });
            });
        });
    }


})(module.exports);
