(function(leaveBalance) {

    var model = require('../model');
    var config = require('../config');
    var log = require('../../services/log');
    var executionContext = require('./executionContext');

    leaveBalance.getLeaveBalance = function(params) {
        return model.LeaveBalance.findById(params.id, {
            include: [model.LeaveType, model.Employee]
        });
    };

    leaveBalance.getLeaveBalances = function(params) {
        var employeeWhereClause = executionContext.GetLocationWhereClause(params);

        return model.LeaveBalance.findAll({
            include: [
                model.LeaveType, {
                    model: model.Employee,
                    as: 'Employee',
                    where: employeeWhereClause
                }
            ],
            order: 'CreatedOn DESC'
        });
    };

    leaveBalance.getLeaveBalancesByEmployee = function(params) {
        return model.LeaveBalance.findAll({
            where: {
                EmployeeId: params.EmployeeId
            }
        });
    };

})(module.exports);
