(function(dashboard) {

    var model = require('../model');
    var dateFormat = require('dateformat');
    var config = require('../config');

    dashboard.GetDashboardSummaryForUser = function(params) {
        return config.sequelize.query('exec pr_GetDashboardSummaryForUser @p_empId=?', {
            replacements: [params.UserId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };


})(module.exports);
