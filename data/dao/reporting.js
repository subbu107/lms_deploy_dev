(function(reporting) {

    var model = require('../model');
    var config = require('../config');
    var log = require('../../services/log');
    var dateFormat = require('dateformat');
    var executionContext = require('./executionContext');

    reporting.getLeaveRequestReport = function(params) {
        log.logger.info('dao->getLeaveRequestReport');
        var query = [];
        query.push(" select Coalesce(MIN(E.FirstName),'') + Coalesce(' ' + MIN(E.MiddleName),'') + Coalesce(' ' + MIN(E.LastName),'') as Name, MIN(E.id) as EmployeeId, ");
        query.push(" convert(varchar(10), MIN(LD.LeaveDate), 110) as FromDate, convert(varchar(10), MAX(LD.LeaveDate), 110) as ToDate, ");
        query.push(" SUM(LD.LeaveHours) as TotalLeaveHours, MIN(LR.Comments) as Comments, MIN(LS.Name) as Status ");
        query.push(" from LeaveRequest LR ");
        query.push(" join LeaveStatus LS on LR.LeaveStatusId = LS.id ");
        query.push(" join LeaveDuration LD on LD.LeaveRequestId = LR.id ");
        query.push(" join Employee E on LR.EmployeeId = E.id ");
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");
        query.push(" group by LD.LeaveRequestId ");
        query.push(" order by convert(varchar(10), MIN(LD.LeaveDate), 110) ");
        return config.sequelize.query(query.join(" "), {
            replacements: [params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    reporting.getLeaveSummaryReport = function(params) {
        var query = [];
        query.push(" select LD.LeaveDate as LeaveDate, sum(LD.LeaveHours)/8.0 as LeaveDays, min(LR.EmployeeId) as EmployeeId, ");
        query.push(" min(E.FirstName) + ' ' +min(E.LastName) as Name, min(LT.Name) as LeaveType, min(LS.Name) as LeaveStatus  ");
        query.push(" from LeaveDuration LD join LeaveRequest LR on LD.LeaveRequestId = LR.id ");
        query.push(" join  Employee E on LR.EmployeeId = E.id ");
        query.push(" join LeaveType LT on LR.LeaveTypeId = LT.id ");
        query.push(" join LeaveStatus LS on LR.LeaveStatusId = LS.id  ");
        query.push(" where LR.LeaveStatusId not in (select id from LeaveStatus where Name in ('Rejected', 'Canceled')) ");
        query.push(" and LD.Id not in (select LC.LeaveDurationId from LeaveCompOff LC) ");
        query.push(" and LD.LeaveDate BETWEEN ? AND ? ");
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");
        query.push(" group by LR.EmployeeId, LD.LeaveDate, LR.LeaveTypeId, LR.LeaveStatusId   ");
        query.push(" order by LD.LeaveDate ");
        return config.sequelize.query(query.join(" "), {
            replacements: [params.fromDate, params.toDate, params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    reporting.getCompOffSummaryReport = function(params) {
        var query = [];
        query.push(" select LD.LeaveDate as LeaveDate, sum(LD.LeaveHours)/8.0 as LeaveDays, min(LR.EmployeeId) as EmployeeId, ");
        query.push(" min(E.FirstName) + ' ' +min(E.LastName) as Name, min(LT.Name) as LeaveType, min(LS.Name) as LeaveStatus  ");
        query.push(" from LeaveDuration LD join LeaveRequest LR on LD.LeaveRequestId = LR.id ");
        query.push(" join  Employee E on LR.EmployeeId = E.id ");
        query.push(" join LeaveType LT on LR.LeaveTypeId = LT.id ");
        query.push(" join LeaveStatus LS on LR.LeaveStatusId = LS.id  ");
        query.push(" where LR.LeaveStatusId not in (select id from LeaveStatus where Name in ('Rejected', 'Canceled')) ");
        query.push(" and LD.Id in (select LC.LeaveDurationId from LeaveCompOff LC) ");
        query.push(" and LD.LeaveDate BETWEEN ? AND ? ");
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");
        query.push(" group by LR.EmployeeId, LD.LeaveDate, LR.LeaveTypeId, LR.LeaveStatusId   ");
        query.push(" order by LD.LeaveDate ");
        return config.sequelize.query(query.join(" "), {
            replacements: [params.fromDate, params.toDate, params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    reporting.getLeaveBalanceReport = function(params) {
        log.logger.info('dao->getLeaveBalanceReport');
        var query = [];
        query.push(" select LB.EmployeeId, E.FirstName + ' '+ E.LastName as 'FullName', LB.AvailableBalance, LT.Name as LeaveType ");
        query.push(" from LeaveBalance LB ");
        query.push(" join LeaveType LT on LB.LeaveTypeId = LT.id ");
        query.push(" join Employee E on LB.EmployeeId = E.id ");
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");
        query.push(" order by LB.EmployeeId ");
        return config.sequelize.query(query.join(" "), {
            replacements: [params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    reporting.getNewJoineeReport = function(params) {
        log.logger.info('dao->getNewJoineeReport');
        var query = [];
        query.push(" SELECT E.*, EU.FirstName + ' ' + EU.LastName as 'AddedBy', L.Name as 'Location' ");
        query.push(" FROM Employee E ");
        query.push(" join Employee EU on E.CreatedBy = EU.id ");
        query.push(" join Location L on E.LocationId = L.id ");
        query.push(" WHERE E.DateOfJoining BETWEEN ? AND ? ");
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");
        query.push(" order by E.DateOfJoining ");
        return config.sequelize.query(query.join(" "), {
            replacements: [params.fromDate, params.toDate, params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

/*
    reporting.getEmployeeExitReport = function(params) {
        log.logger.info('dao->getEmployeeExitReport');
        var query = [];
        query.push(" select E.id into #FilteredEmployee FROM Employee E join Location L on E.LocationId = L.id WHERE E.DateOfExit BETWEEN ? AND ? ");
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");

        query.push(" select E.id as EmployeeId, sum(LC.CreditedLeave) as 'CreditedLeave' into #EmployeeLeaveCredit ");
        query.push(" FROM #FilteredEmployee E ");
        query.push(" join LeaveCredit LC on LC.EmployeeId = E.id and LC.LeaveTypeId = (select id from LeaveType where Name = 'Annual') ");
        query.push(" group by E.id ");

        query.push(" select E.id as EmployeeId, sum(ld.LeaveHours) as 'UtilizedLeave' into #EmployeeLeaveUtilized ");
        query.push(" FROM #FilteredEmployee E   ");
        query.push(" join LeaveRequest LR on LR.EmployeeId = E.id and LR.LeaveTypeId in (select id from LeaveType where Name = 'Annual') and LR.LeaveStatusId in (select id from leavestatus where (name = 'Approved' or name = 'Applied')) ");
        query.push(" join LeaveDuration LD on LD.LeaveRequestId = LR.id and LD.id not in (select lc.LeaveDurationId from LeaveCompOff LC) ");
        query.push(" group by E.id ");

        query.push(" select E.id as EmployeeId, sum(LB.AvailableBalance) as 'ClosingBalance' into #EmployeeClosingBalance ");
        query.push(" FROM #FilteredEmployee E   ");
        query.push(" join LeaveBalance LB on LB.EmployeeId = E.id and LB.LeaveTypeId = (select id from LeaveType where Name = 'Annual') ");
        query.push(" group by E.id ");

        query.push(" SELECT E.id, E.FirstName, E.LastName, E.DateOfExit, ECB.ClosingBalance as 'ClosingBalance', ELC.CreditedLeave as 'AccruedDuringYear', ELU.UtilizedLeave as 'UtilizedLeave', L.Name as 'Location'   ");
        query.push(" FROM Employee E   ");
        query.push(" join #FilteredEmployee FE on FE.id = E.id ");
        query.push(" join Location L on E.LocationId = L.id ");
        query.push(" left join #EmployeeClosingBalance ECB on ECB.EmployeeId = E.id  ");
        query.push(" left join #EmployeeLeaveCredit ELC on ELC.EmployeeId = E.id  ");
        query.push(" left join #EmployeeLeaveUtilized ELU on ELU.EmployeeId = E.id  ");
        query.push(" order by E.DateOfExit ");

        query.push(" drop table #EmployeeClosingBalance ");
        query.push(" drop table #EmployeeLeaveCredit ");
        query.push(" drop table #EmployeeLeaveUtilized ");
        query.push(" drop table #FilteredEmployee ");

        return config.sequelize.query(query.join(" "), {
            replacements: [params.fromDate, params.toDate, params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };
*/
    reporting.getEmployeeExitReport = function(params) {
        log.logger.info('dao->getEmployeeExitReport');

        return config.sequelize.query('exec pr_EmployeeExitReport @p_fromDate=?, @p_toDate=?, @p_locationId=?', {
            replacements: [params.fromDate, params.toDate, params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    reporting.getLossOfPayReport = function(params) {
        log.logger.info('dao->getLossOfPayReport');
        var query = [];
        var replacementsArray = []

        query.push("If(OBJECT_ID('tempdb..#LossOfPayReport') Is Not Null) Begin Drop Table #LossOfPayReport End ");

        query.push(" CREATE TABLE #LossOfPayReport(EmployeeId int, FirstName varchar(200), LastName varchar(200), FromDate datetime,  ");
        query.push(" ToDate datetime, TotalLeaveHours int, ApproverName varchar(400), Location varchar(200), LeaveRequestId int, ApproverId int); ");
        query.push(" insert into #LossOfPayReport (EmployeeId, FromDate, ToDate, TotalLeaveHours, LeaveRequestId) ");
        query.push(" select MIN(LR.EmployeeId) as EmployeeId, MIN(LD.LeaveDate) as FromDate, MAX(LD.LeaveDate) as ToDate, SUM(LD.LeaveHours) as TotalLeaveHours, LD.LeaveRequestId ");
        query.push(" from LeaveDuration LD join LeaveRequest LR on LD.LeaveRequestId = LR.id and LR.LeaveTypeId in (select id from LeaveType where Name = 'LossOfPay') and LR.LeaveStatusId in (select id from leavestatus where name = 'Approved')  ");
        query.push(" join Employee E on E.id = LR.EmployeeID  ");
        if (params.locationId > 0) {
            query.push(" and E.locationId =? ");
            replacementsArray.push(params.locationId);
        }
        query.push(" where LD.LeaveDate BETWEEN ? AND ?  ");
        replacementsArray.push(params.fromDate);
        replacementsArray.push(params.toDate);
        query.push(" group by LD.LeaveRequestId ");
        query.push(" update LOPR set LOPR.FirstName = E.FirstName, LOPR.LastName = E.LastName, LOPR.Location = L.Name ");
        query.push(" from #LossOfPayReport LOPR join Employee E on E.id = LOPR.EmployeeID join Location L on E.LocationId = L.id ");
        query.push(" update LOPR set LOPR.ApproverId = LA.CreatedBy from #LossOfPayReport LOPR join (");
        query.push(" SELECT LRSH.LeaveRequestId,   LRSH.CreatedBy FROM LeaveRequestStatusHistory LRSH INNER JOIN( ");
        query.push(" SELECT MAX(id) as id FROM LeaveRequestStatusHistory ");
        query.push(" where LeaveRequestId in (select LeaveRequestId from #LossOfPayReport) and LeaveStatusId = (select id from leavestatus where name = 'Approved')  ");
        query.push(" GROUP BY LeaveRequestId) LatestApprovalRecord ON LRSH.ID = LatestApprovalRecord.ID ) as LA on LA.LeaveRequestId = LOPR.LeaveRequestId ");
        query.push(" update LOPR set LOPR.ApproverName = Coalesce(A.FirstName,'') + Coalesce(' ' + A.LastName,'') ");
        query.push(" from #LossOfPayReport LOPR join Employee A on A.id = LOPR.ApproverId ");
        query.push(" select * from #LossOfPayReport order by FromDate ");
        query.push("If(OBJECT_ID('tempdb..#LossOfPayReport') Is Not Null) Begin Drop Table #LossOfPayReport End ");

        return config.sequelize.query(query.join(" "), {
            replacements: replacementsArray,
            type: config.sequelize.QueryTypes.SELECT
        });
    };

     reporting.getLeaveEncashmentReport = function(params) {
        log.logger.info('dao->getLeaveEncashmentReport');
        var query = [];
        query.push(" select LE.EmployeeId, LE.FirstName + ' '+ LE.LastName as 'FullName', LE.DateOfJoining, LE.LocationName, LE.OpeningBalance, LE.Accrued, LE.TotalLeaves ");
        query.push(", LE.Utilized, LE.Balance, LE.Expiring, LE.Encashable, LE.BalanceAfterEncashment");        
        query.push(" from LeaveEncashmentReportTable LE "); 
        query.push(" join Location L on LE.LocationName = L.Name ");       
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");
        query.push(" order by LE.EmployeeId ");
        return config.sequelize.query(query.join(" "), {
            replacements: [params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    reporting.getCompOffExpiryReport = function(params) {
        log.logger.info('dao->getCompOffExpiryReport');
      return config.sequelize.query('exec pr_compOffExpiryReport', {            
            type: config.sequelize.QueryTypes.SELECT
        });
    };

})(module.exports);
