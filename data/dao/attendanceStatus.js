(function (attendanceStatus) {

    var model = require('../model');
    var log = require('../../services/log');

    attendanceStatus.GetAttendanceStatusName = function (params) {
        log.logger.info('dao->attendanceStatusDao');
        return model.AttendanceStatus.findAll();
    };

})(module.exports);
