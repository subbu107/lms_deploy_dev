(function(leave) {

    var model = require('../model');
    var config = require('../config');
    var log = require('../../services/log');

    leave.getLeavesToApprove = function(params) {
        log.logger.info('dao->getLeavesToApprove');

        var query = [];
        query.push("select LD.LeaveRequestId as Id, ");
        query.push(" MIN(LD.LeaveDate) as FromDate , MAX(LD.LeaveDate) as ToDate, MIN(LR.CreatedOn) as AppliedOn, ");
        query.push(" MIN(E.Id) as EmployeeId, MIN(E.FirstName) as FirstName, MIN(E.LastName) as LastName, ");
        query.push(" MIN(LR.Comments) as Comments, SUM(LD.LeaveHours) as TotalLeaveHours, ");
        query.push(" MIN(LS.Name) as Status, MIN(LT.Description) as LeaveType, SUM(CompOffHours) as TotalCompOffHours ");
        query.push(" from LeaveDuration LD ");
        query.push(" join LeaveRequest LR on LD.LeaveRequestId = LR.id ");
        query.push(" join LeaveStatus LS on LR.LeaveStatusId = LS.id ");
        query.push(" join LeaveType LT on LR.LeaveTypeId = LT.id ");
        query.push(" join Employee E on LR.EmployeeId = E.id ");
        if (params.locationId > 0)
            query.push(" and E.locationId =? ");
        query.push(" left join (  ");
        query.push(" select sum(CD.CompOffHours) as CompOffHours, LC.LeaveDurationId  ");
        query.push(" from LeaveCompOff LC  ");
        query.push(" join CompOffDuration CD on CD.id = LC.CompOffDurationId ");
        query.push(" group by LC.LeaveDurationId ");
        query.push(" ) as CompOffData on CompOffData.LeaveDurationId = LD.id  ");
        query.push(" group by LD.LeaveRequestId ");
        query.push(" order by LD.LeaveRequestId DESC ");
        return config.sequelize.query(query.join(" "), {
            replacements: [params.locationId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };


    leave.updateLeaveStatus = function(params) {
        log.logger.info("dao->updateLeaveStatus");
        return config.sequelize.transaction(function(t) {
            return config.sequelize.query('exec pr_UpdateLeaveStatus 	@p_LeaveRequestIds = ?, @p_Status = ?, @p_EmployeeId = ?, @p_Comments =?', {
                replacements: [String(params.SelectedIds), params.Status, params.EmployeeId, params.Comments],
                type: config.sequelize.QueryTypes.UPDATE
            });
        });
    };

    leave.getEmployeeLeaves = function(params) {
        log.logger.info('dao->getEmployeeLeaves');

        var query = [];
        query.push("select LD.LeaveRequestId as Id, ");
        query.push(" MIN(LD.LeaveDate) as FromDate , MAX(LD.LeaveDate) as ToDate, ");
        query.push(" MIN(LR.CreatedOn) as AppliedOn, ");
        query.push(" MIN(LR.Comments) as Comments, SUM(LD.LeaveHours) as TotalLeaveHours, ");
        query.push(" MIN(LS.Name) as Status, MIN(LT.Description) as LeaveType,");
        query.push(" SUM(CompOffHours) as CompOffHours ");
        query.push(" from LeaveDuration LD ");
        query.push(" join LeaveRequest LR on LD.LeaveRequestId = LR.id and LR.EmployeeId = ? ");
        query.push(" join LeaveStatus LS on LR.LeaveStatusId = LS.id ");
        query.push(" join LeaveType LT on LR.LeaveTypeId = LT.id ");
        query.push(" left join (  ");
        query.push(" select sum(CD.CompOffHours) as CompOffHours, LC.LeaveDurationId  ");
        query.push(" from LeaveCompOff LC  ");
        query.push(" join CompOffDuration CD on CD.id = LC.CompOffDurationId ");
        query.push(" group by LC.LeaveDurationId ");
        query.push(" ) as CompOffData on CompOffData.LeaveDurationId = LD.id  ");
        query.push(" group by LD.LeaveRequestId ");
        query.push(" order by LD.LeaveRequestId desc ");

        return config.sequelize.query(query.join(" "), {
            replacements: [params.userId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    leave.getLeaveDetails = function(params) {
        log.logger.info('dao->getLeaveDetails');

        var query = [];
        query.push(" select LD.LeaveDate, LD.LeaveHours, LR.id as LeaveRequestId, LR.EmployeeId, LR.CreatedOn, LS.Name as LeaveStatus, LT.Description as LeaveType ");
        query.push(" , CompOffData.CompOffHours, CompOffData.MinCompOffDate as MinCompOffDate, CompOffData.MaxCompOffDate as MaxCompOffDate ");
        query.push(" from LeaveDuration LD ");
        query.push(" join LeaveRequest LR on LD.LeaveRequestId = LR.id ");
        query.push(" join LeaveStatus LS on LR.LeaveStatusId = LS.id ");
        query.push(" join LeaveType LT on LR.LeaveTypeId = LT.id ");
        query.push(" left join ( select sum(CD.CompOffHours) as CompOffHours, min(CD.CompOffDate) as MinCompOffDate, max(CD.CompOffDate) as MaxCompOffDate, LC.LeaveDurationId ");
        query.push(" from LeaveCompOff LC ");
        query.push(" join CompOffDuration CD on CD.id = LC.CompOffDurationId ");
        query.push(" group by LC.LeaveDurationId  ) as CompOffData on CompOffData.LeaveDurationId = LD.id ");
        query.push(" where LR.Id = ? ");
        query.push(" order by LD.LeaveDate ");

        return config.sequelize.query(query.join(" "), {
            replacements: [params.leaveRequestId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

    leave.getEmployeeDetailsOfALeaveRequest = function (params){
      log.logger.info('dao->getEmployeeDetailsOfALeaveRequest');

      var query = [];
      query.push(" select emp.FirstName, emp.LastName, emp.OfficialEmailId, lr.id as leaveRequestId ");
      query.push(" from Employee emp, LeaveRequest lr ");
      query.push(" where lr.id = ? and emp.id = lr.EmployeeId ");

      return config.sequelize.query(query.join(" "), {
          replacements: [params.leaveRequestId],
          type: config.sequelize.QueryTypes.SELECT
      });
    };

    leave.getLeaveStatusHistory = function(params) {
        log.logger.info('dao->getLeaveStatusHistory');

        var query = [];
        query.push(" select LRSH.LeaveRequestId, LR.EmployeeId, LS.Name as LeaveStatus, E.FirstName + ' '+ E.LastName as ChangedBy, LRSH.CreatedOn as StatusDate, LRSH.Comments ");
        query.push(" from LeaveRequestStatusHistory LRSH ");
        query.push(" join LeaveRequest LR on LRSH.LeaveRequestId = LR.id ");
        query.push(" join LeaveStatus LS on LRSH.LeaveStatusId = LS.id ");
        query.push(" join Employee E on LRSH.CreatedBy = E.id  ");
        query.push(" where LR.Id = ? ");
        query.push(" order by StatusDate desc ");

        return config.sequelize.query(query.join(" "), {
            replacements: [params.leaveRequestId],
            type: config.sequelize.QueryTypes.SELECT
        });
    };

})(module.exports);
