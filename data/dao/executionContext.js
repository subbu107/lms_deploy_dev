(function(executionContext) {

    executionContext.GetLocationWhereClause = function(params) {
        var locationWhereClause = null;
        //if a valid location value is passed in, serach based on that
        if (params.locationId > -1) {
            locationWhereClause = {
                LocationId: params.locationId
            };
        } else { //if location is not selected, have a condition which is always true
            locationWhereClause = {
                LocationId: {
                    $ne: null
                }
            };
        }
        return locationWhereClause;
    };
})(module.exports);
