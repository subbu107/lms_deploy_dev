(function(leaveType) {

  var model = require('../model');
  var config = require('../config');

  leaveType.getLeaveTypes = function(params) {
    return model.LeaveType.findAll();
  };

  leaveType.getleaveType = function(params) {
    return model.LeaveType.findById(params.id);
  };
})(module.exports);
