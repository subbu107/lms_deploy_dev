(function(dao) {
    dao.holidayList = require('./holidaylist');
    dao.employeeAttendance = require('./employeeAttendance');
    dao.employeeLeave = require('./employeeLeave');
    dao.referenceData = require('./referenceData');
    dao.dashboard = require('./dashboard');
    dao.compOff = require('./compOff');
    dao.location = require('./location');
    dao.leave = require('./leave');
    dao.reporting = require('./reporting');
    dao.workFromHome = require('./workFromHome');
    dao.employee = require('./employee');
    dao.leaveCredit = require('./leaveCredit');
    dao.leaveCreditType = require('./leaveCreditType');
    dao.leaveType = require('./leaveType');
    dao.leaveBalance = require('./leaveBalance');
    dao.executionContext = require('./executionContext');
    dao.attendanceStatus = require('./attendanceStatus');
})(module.exports);
