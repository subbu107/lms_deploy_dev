
(function(data){


    var holidayList = require("./holidayList");
    var dao = require("./dao");


    data.getHolidayList = function(req, resp){

        holidayList.get(req.param('locationName'), function(err, recordset){
            if(err){
                console.log(err);
            }
            console.dir(recordset);
            resp.set('Content-type','application/json');
            resp.send(recordset);
        });
    };


    data.GetAllHolidaysForLocation = dao.GetAllHolidaysForLocation;


})(module.exports);
