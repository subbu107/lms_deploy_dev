(function(model){

    var sequelize = require('../config');

    model.Location = require('./location').Location;
    model.Holiday = require('./holiday').Holiday;
    model.LocationHoliday = require('./locationholiday').LocationHoliday;
    model.Employee = require('./employee').Employee;
    model.EmployeeStatus = require('./employeeStatus').EmployeeStatus;
    model.LeaveType = require('./leaveType').LeaveType;
    model.LeaveStatus = require('./leaveStatus').LeaveStatus;
    model.LeaveRequest = require('./leaveRequest').LeaveRequest;
    model.LeaveDuration = require('./leaveDuration').LeaveDuration;
    model.CompOffStatus = require('./compOffStatus').CompOffStatus;
    model.CompOffRequest = require('./compOffRequest').CompOffRequest;
    model.CompOffDuration = require('./compOffDuration').CompOffDuration;
    model.LeaveCompOff = require('./LeaveCompOff').LeaveCompOff;
    model.LeaveRequestStatusHistory = require('./leaveRequestStatusHistory').LeaveRequestStatusHistory;
    model.CompOffRequestStatusHistory = require('./compOffRequestStatusHistory').CompOffRequestStatusHistory;
    model.LeaveBalance = require('./leaveBalance').LeaveBalance;
    model.WFHRequest = require('./wfhRequest').WFHRequest;
    model.WFHDuration = require('./wfhDuration').WFHDuration;
    model.WFHStatus = require('./wfhStatus').WFHStatus;
    model.WFHRequestStatusHistory = require('./WFHRequestStatusHistory').WFHRequestStatusHistory;
    model.LeaveCreditType = require('./leaveCreditType').LeaveCreditType;
    model.LeaveCredit = require('./leaveCredit').LeaveCredit;
    model.EmployeeMinimal = require('./employeeMinimal').EmployeeMinimal;
    model.EmployeeStatusMinimal = require('./employeeStatusMinimal').EmployeeStatusMinimal;
    model.EmployeeAttendance = require('./employeeAttendance').EmployeeAttendance;
    model.EmployeeLatestAttendance= require('./employeeLatestAttendance').EmployeeLatestAttendance;
    model.RestrictedHoliday = require('./restrictedHoliday').RestrictedHoliday;
    model.EmployeeRestrictedHoliday= require('./employeeRestrictedHoliday').EmployeeRestrictedHoliday;
    model.AttendanceStatus= require('./attendanceStatus').AttendanceStatus;

    model.Holiday.belongsToMany(model.Location, { through: {model: model.LocationHoliday, unique:false}  });
    model.Location.belongsToMany(model.Holiday, { through: {model: model.LocationHoliday, unique:false}  });
    model.Employee.belongsTo(model.Location, { foreignKey : 'LocationId'});
    model.Location.hasMany(model.Employee, {foreignKey : 'LocationId'});
    model.Employee.hasMany(model.LeaveRequest, {foreignKey : 'EmployeeId'});
    model.Employee.hasMany(model.CompOffRequest, {foreignKey : 'EmployeeId'});
    model.LeaveRequest.belongsTo(model.LeaveStatus, {as: 'LeaveStatus', foreignKey:'LeaveStatusId'});
    model.LeaveRequest.belongsTo(model.LeaveType, {as: 'LeaveType', foreignKey:'LeaveTypeId'});
    model.LeaveRequest.hasMany(model.LeaveDuration, { foreignKey:'LeaveRequestId'});
    model.LeaveDuration.belongsTo(model.LeaveRequest, {foreignKey:'LeaveRequestId'});
    
    model.CompOffRequest.belongsTo(model.CompOffStatus, {as: 'CompOffStatus', foreignKey:'CompOffStatusId'});
    model.CompOffRequest.hasMany(model.CompOffDuration, { foreignKey:'CompOffRequestId'});
    model.LeaveDuration.hasMany(model.LeaveCompOff, {foreignKey : 'LeaveDurationId'});
    model.CompOffDuration.hasMany(model.LeaveCompOff, {foreignKey : 'CompOffDurationId'});
    model.LeaveRequest.hasMany(model.LeaveRequestStatusHistory, {foreignKey : 'LeaveRequestId'});
    model.LeaveBalance.belongsTo(model.LeaveType, {foreignKey:'LeaveTypeId'});
    model.LeaveBalance.belongsTo(model.Employee, {foreignKey:'EmployeeId'});

    model.WFHRequest.belongsTo(model.WFHStatus, {as: 'WFHStatus', foreignKey:'WFHStatusId'});
    model.WFHRequest.hasMany(model.WFHDuration, { foreignKey:'WFHRequestId'});

    model.LeaveCredit.belongsTo(model.Employee, {foreignKey:'EmployeeId'});
    model.LeaveCredit.belongsTo(model.LeaveCreditType, {foreignKey:'LeaveCreditTypeId'});
    model.LeaveCredit.belongsTo(model.LeaveType, {foreignKey:'LeaveTypeId'});

    model.Employee.belongsTo(model.EmployeeStatus, {foreignKey:'EmployeeStatusId'});
    model.EmployeeMinimal.belongsTo(model.EmployeeStatusMinimal, {foreignKey:'EmployeeStatusId'});

    model.EmployeeRestrictedHoliday.belongsTo(model.RestrictedHoliday, {foreignKey: 'RestrictedHolidayId'});
    
    model.EmployeeAttendance.belongsTo(model.Employee, {as: 'Employee', foreignKey : 'EmployeeId'});
    
})(module.exports);
