(function(model) {

  var config = require('../config');

  model.LeaveCompOff = config.sequelize.define('LeaveCompOff', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    LeaveRequestId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveRequestId'
    },
    CompOffRequestId: {
      type: config.Sequelize.BIGINT,
      field: 'CompOffRequestId'
    },
    LeaveDurationId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveDurationId'
    },
    CompOffDurationId: {
      type: config.Sequelize.BIGINT,
      field: 'CompOffDurationId'
    },
    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: false,
    tableName: 'LeaveCompOff'
  });

})(module.exports);
