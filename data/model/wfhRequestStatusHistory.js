(function(model) {

  var config = require('../config');

  model.WFHRequestStatusHistory = config.sequelize.define('WorkFromHomeRequestStatusHistory', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement:true
    },
    WFHRequestId: {
      type: config.Sequelize.BIGINT,
      field: 'WorkFromHomeRequestId'
    },
    WFHStatusId: {
      type: config.Sequelize.BIGINT,
      field: 'WorkFromHomeStatusId'
    },

    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: false,
    tableName: 'WorkFromHomeRequestStatusHistory'
  });

})(module.exports);
