(function(model) {

  var config = require('../config');

  model.CompOffRequest = config.sequelize.define('CompOffRequest', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement:true
    },
    EmployeeId: {
      type: config.Sequelize.BIGINT,
      field: 'EmployeeId'
    },
    CompOffStatusId: {
      type: config.Sequelize.BIGINT,
      field: 'CompOffStatusId'
    },
    Comments: {
      type: config.Sequelize.STRING,
      field: 'Comments'
    },
    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: false,
    tableName: 'CompOffRequest'
  });

})(module.exports);
