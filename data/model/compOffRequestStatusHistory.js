(function(model) {

  var config = require('../config');

  model.CompOffRequestStatusHistory = config.sequelize.define('CompOffRequestStatusHistory', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement:true
    },
    CompOffRequestId: {
      type: config.Sequelize.BIGINT,
      field: 'CompOffRequestId'
    },
    CompOffStatusId: {
      type: config.Sequelize.BIGINT,
      field: 'CompOffStatusId'
    },

    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: false,
    tableName: 'CompOffRequestStatusHistory'
  });

})(module.exports);
