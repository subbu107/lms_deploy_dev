(function(model) {

  var config = require('../config');

  model.LeaveDuration = config.sequelize.define('LeaveDuration', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    LeaveRequestId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveRequestId'
    },
    LeaveDate: {
      type: config.Sequelize.STRING,
      field: 'LeaveDate',
      get: function() {
        return this.getDataValue('LeaveDate').toString().substring(0, 15);
      }
    },


    LeaveHours: {
      type: config.Sequelize.INTEGER,
      field: 'LeaveHours',
    },

    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }

  }, {
    timestamps: false,
    tableName: 'LeaveDuration'
  });

})(module.exports);
