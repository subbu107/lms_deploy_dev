(function(model) {

  var config = require('../config');

  model.EmployeeStatus = config.sequelize.define('EmployeeStatus', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: config.Sequelize.STRING,
      field: 'Name'
    },
    Description: {
      type: config.Sequelize.STRING,
      field: 'Description'
    },
    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: true,
    createdAt: 'createdOn',
    updatedAt: 'updatedOn',
    tableName: 'EmployeeStatus'
  });

})(module.exports);
