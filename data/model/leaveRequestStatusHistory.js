(function(model) {

  var config = require('../config');

  model.LeaveRequestStatusHistory = config.sequelize.define('LeaveRequestStatusHistory', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement:true
    },
    LeaveRequestId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveRequestId'
    },
    LeaveStatusId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveStatusId'
    },

    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: false,
    tableName: 'LeaveRequestStatusHistory'
  });

})(module.exports);
