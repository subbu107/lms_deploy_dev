(function(model) {

  var config = require('../config');

  model.LeaveCredit = config.sequelize.define('LeaveCredit', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement:true
    },
    EmployeeId: {
      type: config.Sequelize.BIGINT,
      field: 'EmployeeId'
    },
    LeaveCreditTypeId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveCreditTypeId'
    },
    LeaveTypeId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveTypeId'
    },
    CreditedLeave: {
      type: config.Sequelize.DECIMAL,
      field: 'CreditedLeave'
    },
    Description: {
      type: config.Sequelize.STRING,
      field: 'Description'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  },
  {
    timestamps: true,
    createdAt: 'createdOn',
    updatedAt: 'updatedOn',
    tableName: 'LeaveCredit'
  });

})(module.exports);
