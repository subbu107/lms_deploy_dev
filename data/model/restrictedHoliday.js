(function(model) {

  var config = require('../config');

  model.RestrictedHoliday = config.sequelize.define('RestrictedHoliday', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    LocationId: {
      type: config.Sequelize.BIGINT,
      field : 'LocationId'     
    },
    Name: {
      type: config.Sequelize.STRING,
      field: 'Name'
    },
    Year: {
      type: config.Sequelize.STRING,
      field: 'Year'
    },    
    ActualDate: {
      type: config.Sequelize.DATE,
      field: 'ActualDate'
    }
  }, {
    timestamps: true,
    createdAt: 'createdOn',
    updatedAt: 'updatedOn',
    freezeTableName: true // Model tableName will be the same as the model name
  });
})(module.exports);
