(function(model){

  var config = require('../config');

  model.WFHRequest = config.sequelize.define('WorkFromHomeRequest', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement:true
    },
    EmployeeId: {
      type: config.Sequelize.BIGINT,
      field: 'EmployeeId'
    },
    WFHStatusId: {
      type: config.Sequelize.BIGINT,
      field: 'WorkFromHomeStatusId'
    },
    Comments: {
      type: config.Sequelize.STRING,
      field: 'Comments'
    },
    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: false,
    tableName: 'WorkFromHomeRequest'
  });

})(module.exports);
