(function(model) {

  var config = require('../config');

  model.Employee = config.sequelize.define('Employee', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
    },
    FirstName: {
      type: config.Sequelize.STRING,
      field: 'FirstName'
    },
    MiddleName: {
      type: config.Sequelize.STRING,
      field: 'MiddleName'
    },
    LastName: {
      type: config.Sequelize.STRING,
      field: 'LastName'
    },
    LocationId: {
      type: config.Sequelize.BIGINT,
      field: 'LocationId'
    },
    IsAdmin: {
      type: config.Sequelize.BOOLEAN,
      field: 'IsAdmin',
      defaultValue: false,
      allowNull: false
    },
    UserName: {
      type: config.Sequelize.STRING,
      field: 'UserName'
    },
    CreatedOn: {
      type: config.Sequelize.DATE,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.DATE,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    },
    EmployeeStatusId: {
      type: config.Sequelize.BIGINT,
      field: 'EmployeeStatusId'
    },
    IsFemale: {
      type: config.Sequelize.BOOLEAN,
      field: 'IsFemale',
      defaultValue: false,
      allowNull: false
    },
    DateOfBirth: {
      type: config.Sequelize.DATE,
      field: 'DateOfBirth',
    },
    DateOfJoining: {
      type: config.Sequelize.DATE,
      field: 'DateOfJoining',
    },
    DateOfExit: {
      type: config.Sequelize.DATE,
      field: 'DateOfExit',
    },
    OfficialEmailId: {
      type: config.Sequelize.STRING(50),
      field: 'OfficialEmailId',
      validate: {
        isEmail: true
      }
    },
    NTLogin: {
      type: config.Sequelize.STRING(50),
      field: 'NTLogin',
    }
  }, {
    getterMethods   : {
    FullName       : function()  { return this.FirstName + ' ' + this.LastName }
    },
    timestamps: true,
    createdAt: 'createdOn',
    updatedAt: 'updatedOn',
    tableName: 'Employee'
  });
})(module.exports);
