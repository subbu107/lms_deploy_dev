(function(model) {

  var config = require('../config');

  model.EmployeeStatusMinimal = config.sequelize.define('EmployeeStatusMinimal', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: config.Sequelize.STRING,
      field: 'Name'
    }
  }, {
    timestamps: false,
    tableName: 'EmployeeStatus'
  });

})(module.exports);
