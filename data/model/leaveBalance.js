(function(model) {

  var config = require('../config');

  model.LeaveBalance = config.sequelize.define('LeaveBalance', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    EmployeeId: {
      type: config.Sequelize.BIGINT,
      field: 'EmployeeId'
    },
    LeaveTypeId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveTypeId'
    },
    AvailableBalance : {
      type: config.Sequelize.BIGINT,
      field: 'AvailableBalance'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: true,
    createdAt: 'createdOn',
    updatedAt: 'updatedOn',
    tableName: 'LeaveBalance'
  });

})(module.exports);
