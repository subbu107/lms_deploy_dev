(function(model) {

  var config = require('../config');

  model.Holiday = config.sequelize.define('Holiday', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: config.Sequelize.STRING,
      field: 'Name'
    },
    Description: {
      type: config.Sequelize.STRING,
      field: 'Description'
    },
    ActualDate: {
      type: config.Sequelize.DATE,
      field: 'ActualDate'
    }
  }, {
    timestamps: true,
    createdAt: 'createdOn',
    updatedAt: 'updatedOn',
    freezeTableName: true // Model tableName will be the same as the model name
  });
})(module.exports);
