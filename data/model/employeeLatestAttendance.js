(function(model) {

  var config = require('../config');

  model.EmployeeLatestAttendance = config.sequelize.define('EmployeeLatestAttendance', {
        id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    NTLogin: {
      type: config.Sequelize.STRING,
      field: 'NTLogin'
    },
    LatestAttendance: {
      type: config.Sequelize.STRING,
      field: 'LatestAttendance'
    }
  },
  { timestamps:false,
    tableName: 'EmployeeLatestAttendance'
  });

})(module.exports);
