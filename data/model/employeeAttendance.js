(function (model) {

  var config = require('../config');

  model.EmployeeAttendance = config.sequelize.define('EmployeeAttendance', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    EmployeeId: {
      type: config.Sequelize.BIGINT,
      field: 'EmployeeId'
    },
    Date: {
      type: config.Sequelize.STRING,
      field: 'Date',
      get: function () {
        return this.getDataValue('Date').toString().substring(0, 15);
      }
    },
    FirstHalf: {
      type: config.Sequelize.BIGINT,
      field: 'FirstHalf'
    },
    SecondHalf: {
      type: config.Sequelize.BIGINT,
      field: 'SecondHalf'
    },
    Comments: {
      type: config.Sequelize.STRING,
      field: 'Comments'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
     CreatedOn: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedOn',
      get: function () {
        return this.getDataValue('CreatedOn').toString().substring(0, 15);
      }
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }

  },
    {
      timestamps: true,
      createdAt: false,
      updatedAt: 'UpdatedOn',
      tableName: 'EmployeeAttendance'
    });

})(module.exports);
