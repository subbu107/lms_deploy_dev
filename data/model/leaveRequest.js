(function(model) {

  var config = require('../config');

  model.LeaveRequest = config.sequelize.define('LeaveRequest', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement:true
    },
    EmployeeId: {
      type: config.Sequelize.BIGINT,
      field: 'EmployeeId'
    },
    LeaveTypeId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveTypeId'
    },
    LeaveStatusId: {
      type: config.Sequelize.BIGINT,
      field: 'LeaveStatusId'
    },
    Comments: {
      type: config.Sequelize.STRING,
      field: 'Comments'
    },
    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: false,
    tableName: 'LeaveRequest'
  });

})(module.exports);
