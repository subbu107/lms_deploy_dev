(function(model){

        var config = require('../config');
        model.LocationHoliday = config.sequelize.define('LocationHoliday', {

          id: {
            type: config.Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement:true
          },
          Year: {
            type: config.Sequelize.DATE,
            field: 'Year'
          },
          LocationId: {
            type: config.Sequelize.BIGINT,
          },
          HolidayId: {
            type: config.Sequelize.BIGINT,
          },


        }, {
          timestamps: true,
          createdAt : 'createdOn',
          updatedAt: 'updatedOn',
          freezeTableName: true // Model tableName will be the same as the model name
        });

    })(module.exports);
