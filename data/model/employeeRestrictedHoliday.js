(function(model) {

  var config = require('../config');

  model.EmployeeRestrictedHoliday = config.sequelize.define('EmployeeRestrictedHoliday', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    EmployeeId: {
      type: config.Sequelize.BIGINT,
      field : 'EmployeeId'     
    },
    RestrictedHolidayId: {
      type: config.Sequelize.BIGINT,
      field: 'RestrictedHolidayId'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: true,
    createdAt: 'createdOn',
    updatedAt: 'updatedOn',
    freezeTableName: true // Model tableName will be the same as the model name
  });
})(module.exports);
