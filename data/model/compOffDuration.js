(function(model) {

  var config = require('../config');

  model.CompOffDuration = config.sequelize.define('CompOffDuration', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    CompOffRequestId: {
      type: config.Sequelize.BIGINT,
      field: 'CompOffRequestId'
    },
    CompOffDate: {
      type: config.Sequelize.STRING,
      field: 'CompOffDate',
      get: function() {
        return this.getDataValue('CompOffDate').toString().substring(0, 15);
      }
    },
    CompOffHours: {
      type: config.Sequelize.INTEGER,
      field: 'CompOffHours',
    },

    CreatedOn: {
      type: config.Sequelize.STRING,
      field: 'CreatedOn'
    },
    UpdatedOn: {
      type: config.Sequelize.STRING,
      field: 'UpdatedOn'
    },
    CreatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'CreatedBy'
    },
    UpdatedBy: {
      type: config.Sequelize.BIGINT,
      field: 'UpdatedBy'
    }
  }, {
    timestamps: false,
    tableName: 'CompOffDuration'
  });

})(module.exports);
