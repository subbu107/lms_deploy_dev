(function(model){

    var config = require('../config');

    model.WFHDuration = config.sequelize.define('WorkFromHomeDuration', {

      id: {
        type: config.Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      WFHRequestId: {
        type: config.Sequelize.BIGINT,
        field: 'WorkFromHomeRequestId'
      },
      WFHDate: {
        type: config.Sequelize.STRING,
        field: 'WorkFromHomeDate',
        get: function() {
          return this.getDataValue('WorkFromHomeDate').toString().substring(0, 15);
        }
      },
      WFHHours: {
        type: config.Sequelize.INTEGER,
        field: 'WorkFromHomeHours',
      },

      CreatedOn: {
        type: config.Sequelize.STRING,
        field: 'CreatedOn'
      },
      UpdatedOn: {
        type: config.Sequelize.STRING,
        field: 'UpdatedOn'
      },
      CreatedBy: {
        type: config.Sequelize.BIGINT,
        field: 'CreatedBy'
      },
      UpdatedBy: {
        type: config.Sequelize.BIGINT,
        field: 'UpdatedBy'
      }
    }, {
      timestamps: false,
      tableName: 'WorkFromHomeDuration'
    });

})(module.exports);
