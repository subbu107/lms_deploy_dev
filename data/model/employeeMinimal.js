(function(model) {

  var config = require('../config');

  model.EmployeeMinimal = config.sequelize.define('EmployeeMinimal', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
    },
    FirstName: {
      type: config.Sequelize.STRING,
      field: 'FirstName'
    },
    IsAdmin: {
      type: config.Sequelize.BOOLEAN,
      field: 'IsAdmin',
      defaultValue: false,
      allowNull: false
    },
    UserName: {
      type: config.Sequelize.STRING,
      field: 'UserName'
    },
    EmployeeStatusId: {
      type: config.Sequelize.BIGINT,
      field: 'EmployeeStatusId'
    },
    LocationId: {
      type: config.Sequelize.BIGINT,
      field: 'LocationId'
    }
  }, {
    timestamps: false,
    tableName: 'Employee'
  });
})(module.exports);
