(function (model) {

    var config = require('../config');

    model.AttendanceStatus = config.sequelize.define('AttendanceStatus', {

        ID: {
            type: config.Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            field:'id'
        },
        Name: {
            type: config.Sequelize.STRING,
            field: 'name'
        }
    },
    {
      timestamps: false,
      tableName: 'AttendanceStatus'
    });

})(module.exports);
