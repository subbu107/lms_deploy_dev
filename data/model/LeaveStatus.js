(function(model) {

  var config = require('../config');

  model.LeaveStatus = config.sequelize.define('LeaveStatus', {

    id: {
      type: config.Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: config.Sequelize.STRING,
      field: 'Name'
    },
    Description: {
      type: config.Sequelize.STRING,
      field: 'Description'
    },

  }, {
    timestamps: true,
    createdAt: 'createdOn',
    updatedAt: 'updatedOn',
    tableName: 'LeaveStatus'
  });

})(module.exports);
