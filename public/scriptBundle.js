(function() {
    'use strict';

    // Global error handler
    window.onerror = function(message) {
        if (window.alerts)
            window.alerts.error("There was a problem with your last action. Please reload this page!" + message);
        else {
            alert("Serious issue!! Please close application and open again!" + message);
        }
    };

    angular.module('lms', [
        // Angular modules
        'ngRoute',
        'ngSanitize',
        'nzToggle',
        'trNgGrid',
        'ui.bootstrap',
        'angular-jwt',
        'angular-storage',
        'angularMoment',
        'ngMaterial',
        'ui.select'
    ]);

    // Handle routing errors and success events
    angular.module('lms').run([
        '$route',
        function($route) {
            TrNgGrid.rowSelectedCssClass = "LMSRowSelect";
            //TrNgGrid.headerCellCssClass = "tr-ng-column-header "+ "LMSTableHeader " + TrNgGrid.cellCssClass;
            // Include $route to kick start the router.
        }
    ]);

    angular.module('lms').config(function($httpProvider) {
        //IE has 'If-Modified-Since' header based ajax request caching
        //which yields undesirable results during site navigation.
        $httpProvider.defaults.cache = false;
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        // disable IE ajax request caching
        $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';

        $httpProvider.interceptors.push([
            '$q', '$location', '$log', 'store', 'jwtHelper',
            function($q, $location, $log, store, jwtHelper) {
                return {
                    'request': function(config) {
                        var params = config.params;
                        var authHeader = config.headers.Authorization;
                        var tokenFromAuthHeader = null;
                        if (authHeader) {
                            var authArr = authHeader.split(' ');
                            tokenFromAuthHeader = authArr[1];
                        } else {
                            var url = window.location.href;
                            var urlSplit = url.split('?');
                            if (urlSplit[1]) {
                                var paramSplit = urlSplit[1].split('=');
                                if (paramSplit[0] === 'token') {
                                    tokenFromAuthHeader = paramSplit[1];
                                } else if (paramSplit[1] === 'errorMessage') {
                                    $scope.error = true;
                                    $scope.errorMessage = 'Invalid user. Please contact admin for registering';
                                }
                            }
                        }

                        config.headers = config.headers || {};

                        var token = tokenFromAuthHeader ? tokenFromAuthHeader : store.get('authToken');

                        if (token && !jwtHelper.isTokenExpired(token) && jwtHelper.getTokenExpirationDate(token)) {
                            if (tokenFromAuthHeader) {
                                store.set('authToken', tokenFromAuthHeader);
                                var userInfo = jwtHelper.decodeToken(tokenFromAuthHeader);
                                store.set('userInfo', userInfo);
                            }

                            config.headers.Authorization = 'Bearer ' + token;
                            var directUrl = store.get('directUrl');
                            if (directUrl) {
                                store.set('directUrl', null);
                                $location.path(directUrl);
                            }

                        } else {
                            if ($location.path() != '/signin' && $location.path() != '/hackin') {
                                store.set('directUrl', $location.path());
                                $location.path('/signin');
                            }

                        }


                        return config;
                    },
                    'responseError': function(response) {
                        if (response.status === 401 || response.status === 403) {
                            $location.path('/signin');
                        }
                        return $q.reject(response);
                    }
                };
            }
        ]);
    });
})();
(function() {
    'use strict';

    angular.module('lms').config([
        '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
            $routeProvider.
                when('/',
                {
                    templateUrl: '/app/dashboard/dashboard.html',
                    controller: 'dashboardCtrl'
                }).
                when('/applyForLeave',
                {
                    templateUrl: '/app/applyLeave/applyLeave.html',
                    controller: 'applyLeaveCtrl',
                    controllerAs : 'apply'
                }).
                when('/applyForCompOff',
                {
                    templateUrl: '/app/applyLeave/applyCompOff.html',
                    controller: 'applyCompOffCtrl'
                }).
                when('/holidayList',
                {
                    templateUrl: '/app/holidayList/holidayList.html',
                    controller: 'holidayListCtrl'
                }).
                when('/dashboard',
                {
                    templateUrl: '/app/dashboard/dashboard.html',
                    controller: 'dashboardCtrl'
                }).
                when('/instructions',
                {
                    templateUrl: '/app/instructions/instructions.html',
                }).
                when('/contributors',
                {
                    templateUrl: '/app/instructions/contributors.html',
                }).
                when('/userEntry/:employeeId',
                {
                    templateUrl: '/app/admin/users/userEntry.html',
                    controller: 'userEntryCtrl'
                }).
                when('/leaveHistory',
                {
                    templateUrl: '/app/history/leaveHistory.html',
                    controller: 'leaveHistoryCtrl',
                }).
                when('/leaveDetails/:leaveRequestId',
                {
                    templateUrl: '/app/history/leaveDetails.html',
                    controller: 'leaveDetailsCtrl',
                }).
                when('/compOffHistory',
                {
                    templateUrl: '/app/history/compOffHistory.html',
                    controller: 'compOffHistoryCtrl',
                }).                
                when('/admin',
                {
                    templateUrl: '/app/admin/admin.html',
                    controller: 'adminCtrl'
                }).
                 when('/adminAttendance',
                {
                    templateUrl: '/app/admin/attendance/attendance.html',
                    controller: 'adminAttendanceCtrl',
                    landingPage: 'adminAttendance'
                }).
                when('/adminApprovals',
                {
                    templateUrl: '/app/admin/approvals/approvals.html',
                    controller: 'adminApprovalsCtrl',
                    landingPage: 'leave'
                }).
                when('/adminCompOffApprovals',
                {
                    templateUrl: '/app/admin/approvals/approvals.html',
                    controller: 'adminApprovalsCtrl',
                    landingPage: 'compOff'
                }).
                when('/adminLeaveCredit',
                {
                    templateUrl: '/app/admin/leaveCredit/leaveCredit.html',
                    controller: 'adminLeaveCreditCtrl'
                }).
                when('/leaveCreditEntry/:leaveCreditId',
                {
                    templateUrl: '/app/admin/leaveCredit/leaveCreditEntry.html',
                    controller: 'adminLeaveCreditEntryCtrl'
                }).
                when('/adminReports',
                {
                    templateUrl: '/app/admin/reports/reporting.html'
                }).
                when('/leaveSummaryReport',
                {
                    templateUrl: '/app/admin/reports/leaveSummaryReport.html'
                }).
                when('/leaveBalanceReport',
                {
                    templateUrl: '/app/admin/reports/leaveBalanceReport.html'
                }).
                when('/newJoineeReport',
                {
                    templateUrl: '/app/admin/reports/newJoineeReport.html'
                }).
                when('/employeeExitReport',
                {
                    templateUrl: '/app/admin/reports/employeeExitReport.html'
                }).
                when('/lossOfPayReport',
                {
                    templateUrl: '/app/admin/reports/lossOfPayReport.html'
                }).
                when('/compOffReport',
                {
                    templateUrl: '/app/admin/reports/compOffReport.html'
                }).
                when('/annualLeaveReport',
                {
                    templateUrl: '/app/admin/reports/annualLeaveReport.html'
                }).            
                when('/leaveEncashmentReport',
                {
                    templateUrl: '/app/admin/reports/leaveEncashmentReport.html'
                }).
                when('/compOffExpiryReport',
                {
                    templateUrl: '/app/admin/reports/compOffExpiryReport.html'
                }).
                when('/adminUsers',
                {
                    templateUrl: '/app/admin/users/users.html'
                }).
                when('/impersonateUser',
                {
                    templateUrl: '/app/admin/users/impersonateUser.html'
                }).
                when('/signin',
                {
                    templateUrl: '/app/auth/signin.html',
                    controller: 'signinCtrl'
                }).
                when('/roundTrip',
                {
                    templateUrl: '/app/auth/roundTrip.html',
                    controller: 'roundTripCtrl'
                }).
                when('/hackin',
                {
                    templateUrl: '/app/auth/hackin.html',
                    controller: 'hackInCtrl'
                }).
                when('/signout',
                {
                    templateUrl: '/app/auth/signout.html',
                    controller: 'signinCtrl'
                }).
                when('/attendance',
                {
                    templateUrl: '/app/attendance/attendance.html',
                    controller: 'attendanceCtrl'
                }).
                otherwise({ redirectTo: '/signin' });

            $locationProvider.html5Mode({
                enabled: false,
                requireBase: false
            });
        }
    ]);
})();


(function() {
  'use strict';

  // Controller name is handy for logging
  var controllerId = 'adminCtrl';

  // Define the controller on the module.
  // Inject the dependencies.
  // Point to the controller definition function.
  angular.module('lms').controller(controllerId, ['$scope', '$log', showAdmin]);

  function showAdmin($scope, $log) {
    function activate() {
    }

    function handleError(err) {
      $scope.error = true;
      $log.debug(err);
      $scope.errorMessage = "An Internal error occurred";
    }
    activate();
  }
})();

(function () {
    'use strict';

    var controllerId = 'attendanceCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$log', 'attendanceService', 'dateService', 'employeeService', getAttendance]);

    function getAttendance($scope, $log, attendanceService, dateService, employeeService) {

        $scope.itemArray = [];
        $scope.selected = [];


        $scope.tagTransform = function (newTag) {
            if (_.some($scope.selected.value, function (employee) {
                if (employee.OfficialEmailId == this.searchString) {
                    return true;
                }
                return false;
            }, { searchString: newTag.toLowerCase() })) {
                return null;
            }
            var item = {
                FullName: newTag,
                OfficialEmailId: newTag.toLowerCase(),
                Id: $scope.id++
            };
            return item;
        };

        var getEmployeeEmails = function () {
            employeeService.getActiveEmployeesEmails().then(function (data) {
                $scope.itemArray = data;
                _.each($scope.itemArray, function (employee) {
                    if (angular.isDefined(employee.OfficialEmailId))
                        employee.OfficialEmailId = employee.OfficialEmailId.toLowerCase();
                });
            }, function (err) {
                $scope.disableApply = false;
                $scope.error = true;
                $scope.errorMessage = error;
                dialogService.showAlert(error);
            });
        }

        $scope.markAttendance = function () {
            console.log($scope.firstHalf, $scope.secondHalf, $scope.comments);
            var date = dateService.formatDateWithCustomFormat(new Date(), 'MM-dd-yyyy');
            var data = {
                FirstHalf: $scope.firstHalf,
                SecondHalf: $scope.secondHalf,
                Comments: $scope.comments,
                Date: date,
                CreatedOn: date
            };

            if ($scope.edit == 0) {
                attendanceService.markAttendance(data)
                    .then(function (response) {
                        $scope.error = false;
                        $scope.attendance = response;
                        $scope.edit++;
                        $scope.getUserAttendance();
                    }, function (err) {
                        $scope.error = true;
                        $scope.errorMessage = err;
                    });
            }
            else {
                $scope.updateAttendance();
                console.log('updateAttendance hitting')
            }


        }

        $scope.updateAttendance = function () {
            console.log($scope.firstHalf, $scope.secondHalf, $scope.comments);
            var date = dateService.formatDateWithCustomFormat(new Date(), 'MM-dd-yyyy');
            var data = {
                FirstHalf: $scope.firstHalf,
                SecondHalf: $scope.secondHalf,
                Comments: $scope.comments,
                Date: date,
                CreatedOn: date
            };
            attendanceService.updateAttendance(data)
                .then(function (response) {
                    $scope.error = false;
                    $scope.attendance = response;
                    $scope.edit++;
                    $scope.getUserAttendance();
                }, function (err) {
                    $scope.error = true;
                    $scope.errorMessage = err;
                });
        }

        $scope.editAttendance = function () {
            $scope.edit++;
        }

        $scope.getUserAttendance = function () {
            var date = dateService.formatDateWithCustomFormat(new Date(), 'MM-dd-yyyy');
            attendanceService.getUserAttendance(date).then(function (data) {
                $scope.error = false;
                $scope.attendanceForUser = data;
                console.log($scope.attendanceForUser);
            }, function (err) {
                $scope.error = true;
                $scope.errorMessage = err;
            });
        };
        function activate() {
            $scope.firstHalf = "1";
            $scope.secondHalf = "1";
            $scope.commentsRequired = true;
            $scope.edit = "0";
            getEmployeeEmails();
            $scope.getUserAttendance();
        }
        activate();
    };

})();

(function() {
    'use strict';

    var controllerId = 'applyCompOffCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$location', '$log', 'leaveLookupService', 'referenceDataService', 'applyService', 'dateService', 'dialogService', 'employeeService', applyCompOff]);

    function applyCompOff($scope, $location, $log, leaveLookupService, referenceDataService, applyService, dateService, dialogService, employeeService) {

      $scope.itemArray = [];

       $scope.selected = [];
       $scope.id = -900000;

       $scope.tagTransform = function (newTag) {
         if(_.some($scope.selected.value, function(employee){
           if(employee.OfficialEmailId == this.searchString){
             return true;
           }
           return false;
         }, {searchString: newTag.toLowerCase()})){
           return null;
         }
         var item = {
             FullName: newTag,
             OfficialEmailId: newTag.toLowerCase(),
             Id: $scope.id++
         };
         return item;
       }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "Apply for leave failed due to internal error :" + err;
        }

        var initializeDateOptions = function() {
            var today = new Date();
            $scope.from = {
                minDate: new Date(),
                maxDate: new Date(),
                opened: false
            };
            $scope.to = {
                minDate: new Date(),
                maxDate: new Date(),
                opened: false
            };

            $scope.from.disabled = function(date, mode) {
                return (date < $scope.from.minDate || date > $scope.from.maxDate);
            };

            $scope.to.disabled = function(date, mode) {
                return (date < $scope.to.minDate || date > $scope.to.maxDate);
            };

            $scope.dateOptions = {
                formatYear: 'yyyy',
                startingDay: 1
            };

            $scope.disabledFrom = function(date, mode) {
                return (date < $scope.from.minDate || date > $scope.from.maxDate);
            };

            $scope.disabledTo = function(date, mode) {
                return (date < $scope.to.minDate || date > $scope.to.maxDate);
            };

            $scope.openFrom = function($event) {
                $scope.from.opened = true;
            };

            $scope.openTo = function($event) {
                $scope.to.opened = true;
            };
        }

        var setupDateRanges = function() {
            var today = new Date();

            var fromMinDateRange = -30;
            var fromMaxDateRange = 0;
            var toMinDateRange = -30;
            var toMaxDateRange = 0;

            $scope.from.minDate.setDate(today.getDate() + fromMinDateRange);
            $scope.from.maxDate.setDate(today.getDate() + fromMaxDateRange);
            $scope.to.minDate.setDate(today.getDate() + toMinDateRange);
            $scope.to.maxDate.setDate(today.getDate() + toMaxDateRange);
        }

        var adjustToDate = function() {
            if ($scope.details.fromDate > $scope.details.toDate) {
                $scope.details.toDate = $scope.details.fromDate;
            }
        }

        var adjustFromDate = function() {
            if ($scope.details.fromDate > $scope.details.toDate) {
                $scope.details.fromDate = $scope.details.toDate;
            }
        }

        $scope.onFromDateChange = function() {
            $scope.error = false;
            adjustToDate();
            refreshDays();
        };

        $scope.onToDateChange = function() {
            $scope.error = false;
            adjustFromDate();
            refreshDays();
        };

        var getValidCompOffDays = function() {
            leaveLookupService.lookupCompOffs().then(function(data) {
                $scope.compOffs = data;
                refreshDays();
            }, handleError);
        };

        var lookupCompOffs = function() {
            leaveLookupService.lookupCompOffs().then(function(data) {
                $scope.error = false;
                $scope.compOffs = data;
            }, function(error) {
                handleError(error);
            });
        }

        var refreshDays = function() {
            leaveLookupService.getValidDays($scope.details.fromDate, $scope.details.toDate, null).then(function(response) {
                $scope.validDays = leaveLookupService.adjustDateRows(response, $scope.compOffs, null, $scope.details.fromDate, $scope.details.toDate);
            }, handleError);
        }

        var applyForCompOff = function() {
            applyService.applyForCompOff($scope.validDays, $scope.details.comments, $scope.selected.value).then(function(data) {
                $scope.error = false;
                $location.path('/dashboard');
            }, function(error) {
                $scope.disableApply = false;
                $scope.error = true;
                $scope.errorMessage = error;
                dialogService.showAlert(error);
            });
        };

        $scope.doApply = function() {
            $scope.disableApply = true;
            applyForCompOff();
        };

        var getEmployeeEmails = function(){
          employeeService.getActiveEmployeesEmails().then(function(data){
            $scope.itemArray = data;
            _.each($scope.itemArray, function(employee){
              if(angular.isDefined(employee.OfficialEmailId))
                employee.OfficialEmailId = employee.OfficialEmailId.toLowerCase();
            });
          }, function(err){
            $scope.disableApply = false;
            $scope.error = true;
            $scope.errorMessage = error;
            dialogService.showAlert(error);
          });
        }

        $scope.dateFormat = dateService.getUIDateFormat();

        var activate = function() {
            $scope.disableApply = false;

            $scope.details = {
                fromDate: new Date(),
                toDate: new Date(),
                comments: ''
            };

            $scope.details.commentsRequired = true;
            $scope.applyType = 1;
            initializeDateOptions();
            setupDateRanges();
            getValidCompOffDays();
            getEmployeeEmails();
        }

        activate();

    }
})();

(function() {
    'use strict';

    var controllerId = 'applyLeaveCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$location', '$log', 'leaveLookupService', 'referenceDataService', 'applyService', 'dateService', 'dialogService','employeeService', applyLeave]);

    function applyLeave($scope, $location, $log, leaveLookupService, referenceDataService, applyService, dateService, dialogService, employeeService) {

      $scope.itemArray = [];

       $scope.selected = [];
       $scope.id = -900000;

       $scope.tagTransform = function (newTag) {
         if(_.some($scope.selected.value, function(employee){
           if(employee.OfficialEmailId == this.searchString){
             return true;
           }
           return false;
         }, {searchString: newTag.toLowerCase()})){
           return null;
         }
         var item = {
             FullName: newTag,
             OfficialEmailId: newTag.toLowerCase(),
             Id: $scope.id++
         };
         return item;
       };

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "Apply for leave failed due to internal error : " + err;
        }

        var initializeDateOptions = function() {
            var today = new Date();
            $scope.from = {
                minDate: new Date(),
                maxDate: new Date(),
                opened: false
            };

            $scope.createOption = function(term) {
              $scope.$apply(function() {
                $scope.recieversList.push(term);
                $scope.recievers.push(term);
              });
            }

            $scope.to = {
                minDate: new Date(),
                maxDate: new Date(),
                opened: false
            };

            $scope.from.disabled = function(date, mode) {
                return (date < $scope.from.minDate || date > $scope.from.maxDate);
            };

            $scope.to.disabled = function(date, mode) {
                return (date < $scope.to.minDate || date > $scope.to.maxDate);
            };

            $scope.dateOptions = {
                formatYear: 'yyyy',
                startingDay: 1
            };

            $scope.disabledFrom = function(date, mode) {
                return (date < $scope.from.minDate || date > $scope.from.maxDate);
            };

            $scope.disabledTo = function(date, mode) {
                return (date < $scope.to.minDate || date > $scope.to.maxDate);
            };

            $scope.openFrom = function($event) {
                $scope.from.opened = true;
            };

            $scope.openTo = function($event) {
                $scope.to.opened = true;
            };
        }

        var setupDateRanges = function() {
            var today = new Date();

            var fromMinDateRange = -60;
            var fromMaxDateRange = 60;
            var toMinDateRange = -60;
            var toMaxDateRange = 60;

            $scope.from.minDate.setDate(today.getDate() + fromMinDateRange);
            $scope.from.maxDate.setDate(today.getDate() + fromMaxDateRange);
            $scope.to.minDate.setDate(today.getDate() + toMinDateRange);
            $scope.to.maxDate.setDate(today.getDate() + toMaxDateRange);
        }

        var adjustToDate = function() {
            if ($scope.details.fromDate > $scope.details.toDate) {
                $scope.details.toDate = $scope.details.fromDate;
            }
        }

        var adjustFromDate = function() {
            if ($scope.details.fromDate > $scope.details.toDate) {
                $scope.details.fromDate = $scope.details.toDate;
            }
        }

        $scope.onFromDateChange = function() {
            $scope.error = false;
            adjustToDate();
            refreshLeaveDays();
        };

        $scope.onToDateChange = function() {
            $scope.error = false;
            adjustFromDate();
            refreshLeaveDays();
        };

        var getValidLeaveDays = function() {
            $scope.error = false;
            referenceDataService.getleaveTypesWithBalance().then(function(response) {
                $scope.leaveTypes = response;
                $scope.SelectedLeaveType = $scope.leaveTypes[0];
                leaveLookupService.lookupCompOffs().then(function(data) {
                    $scope.compOffs = data;
                    refreshLeaveDays();
                }, handleError);
            }, handleError);
        };

        $scope.adjustDateRows = function() {
            $scope.validLeaveDays = leaveLookupService.adjustDateRows($scope.validLeaveDays, $scope.compOffs, $scope.SelectedLeaveType.LeaveTypeName, $scope.details.fromDate, $scope.details.toDate);
        };

        $scope.refreshLeaveDays = function() {
            $scope.error = false;
            refreshLeaveDays();
        }

        var refreshLeaveDays = function() {
            leaveLookupService.getValidDays($scope.details.fromDate, $scope.details.toDate, $scope.SelectedLeaveType.LeaveTypeName).then(function(response) {
                $scope.validLeaveDays = leaveLookupService.adjustDateRows(response, $scope.compOffs, $scope.SelectedLeaveType.LeaveTypeName, $scope.details.fromDate, $scope.details.toDate);
            }, handleError);
        }

        var lookupLeaveTypes = function() {

            referenceDataService.getleaveTypesWithBalance().then(function(response) {
                $scope.leaveTypes = response;
                $scope.SelectedLeaveType = $scope.leaveTypes[0];
            }, handleError);

        };

        var lookupCompOffs = function() {
            leaveLookupService.lookupCompOffs().then(function(data) {
                $scope.error = false;
                $scope.compOffs = data;
            }, function(error) {
                handleError(error);
            });
        }

        var applyForLeave = function() {
            applyService.applyForLeave($scope.SelectedLeaveType, $scope.validLeaveDays, $scope.details.comments, $scope.selected.value).then(function(data) {
                $scope.error = false;
                $location.path('/dashboard');
            }, function(error) {
                $scope.disableApply = false;
                $scope.error = true;
                $scope.errorMessage = error;
                dialogService.showAlert(error);
            });
        };

        var getEmployeeEmails = function(){
          employeeService.getActiveEmployeesEmails().then(function(data){
            $scope.itemArray = data;
            _.each($scope.itemArray, function(employee){
              if(angular.isDefined(employee.OfficialEmailId))
                employee.OfficialEmailId = employee.OfficialEmailId.toLowerCase();
            });
          }, function(err){
            $scope.disableApply = false;
            $scope.error = true;
            $scope.errorMessage = error;
            dialogService.showAlert(error);
          });
        }

        $scope.doApply = function() {
            $scope.disableApply = true;
            applyForLeave();
        };

        $scope.dateFormat = dateService.getUIDateFormat();

        var activate = function() {
            $scope.disableApply = false;

            $scope.details = {
                fromDate: new Date(),
                toDate: new Date(),
                comments: ''
            };

            $scope.details.commentsRequired = true;
            $scope.applyType = 0;
            initializeDateOptions();
            setupDateRanges();
            getValidLeaveDays();
            getEmployeeEmails();
        }

        activate();
    }
})();


(function() {
    'use strict';

    var controllerId = 'hackInCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$location', '$log', 'store', 'jwtHelper', 'loginService', hackInCtrl]);

    function hackInCtrl($scope, $rootScope, $location, $log, store, jwtHelper, loginService) {

        $scope.signIn = function(username, password) {
            if ($location.host() == 'localhost') {
                loginService.performFormBasedLogin(username, password).then(function(response) {
                    var token = response.token;
                    var userInfo = jwtHelper.decodeToken(token);
                    store.set('authToken', token);
                    store.set('userInfo', userInfo);
                    $scope.userInfo = userInfo;
                    $rootScope.$broadcast('userInfoUpdate', userInfo);
                    $location.path('/');
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = err;
                });
            } else {
                $scope.error = true;
                $scope.errorMessage = 'Allowed only from localhost';
            }
        };
    }
})();

(function() {
    'use strict';

    var controllerId = 'roundTripCtrl';
    angular.module('lms').controller(controllerId, ['$scope', 'utilService', roundTripCtrl]);

    function roundTripCtrl($scope, utilService) {
        var activate = function() {
            var url = window.location.href;
            var urlSplit = url.split('?');
            if (urlSplit[1]) {
                url = url.replace('?' + urlSplit[1], '');
                window.location.href = url;
            }
            utilService.refreshPage('/dashboard');
        }

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'signinCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$window', '$location', '$log', 'store', 'appDataStoreService', signInCtrl]);

    function signInCtrl($scope, $rootScope, $window, $location, $log, store, appDataStoreService) {
        var activate = function() {
            $scope.showLiveLogout = false;
            checkForErrorMessage();
            //checkForUnSupportedBrowser();
            checkForSupportedBrowser();
        }

        var checkForSupportedBrowser = function() {
            $scope.supportedBrowser = false;

            var supportedBrowserAgentNames = [
                'Chrome/',
                'SpiderLogic-Attendance-TrayApp'
            ];

            var unSupportedBrowserAgentNames = [
                'Edge/',
                'internet explorer',
                '.NET4.0C',
                '.NET4.0E',
                '.NET CLR 2.0.50727',
                '.NET CLR 3.0.30729',
                '.NET CLR 3.5.30729'
            ];

            var userAgent = $window.navigator.userAgent;
            for (var i = 0; i < supportedBrowserAgentNames.length; ++i) {
                $scope.isUnSupportedBrowser = false;
                if (userAgent.indexOf(supportedBrowserAgentNames[i]) > -1) {
                  for (var i = 0; i < unSupportedBrowserAgentNames.length; ++i) {

                      if (userAgent.indexOf(unSupportedBrowserAgentNames[i]) > -1) {
                          $scope.isUnSupportedBrowser = true;
                          break;
                      }
                  }
                    $scope.supportedBrowser = ! $scope.isUnSupportedBrowser;
                    break;
                }
            }

            $scope.isUnSupportedBrowser = ! $scope.supportedBrowser;
        }

        var checkForUnSupportedBrowser = function() {
            $scope.isUnSupportedBrowser = false;

            var unSupportedBrowserAgentNames = [
                'Edge/',
                'internet explorer',
                '.NET4.0C',
                '.NET4.0E',
                '.NET CLR 2.0.50727',
                '.NET CLR 3.0.30729',
                '.NET CLR 3.5.30729'
            ];

            var userAgent = $window.navigator.userAgent;
            for (var i = 0; i < unSupportedBrowserAgentNames.length; ++i) {

                if (userAgent.indexOf(unSupportedBrowserAgentNames[i]) > -1) {
                    $scope.isUnSupportedBrowser = true;
                    break;
                }
            }
        }

        var handleError = function(err) {
            $scope.error = true;
            if (err)
                $scope.errorMessage = err;
            else
                $scope.errorMessage = "An internal error occurred";
        }

        var checkForErrorMessage = function() {
            $scope.error = false;
            $scope.errorMessage = null;
            $scope.showLiveLogout = false;

            var errorMessage = $location.search().errorMessage;
            if (errorMessage && typeof(errorMessage) !== "boolean") {
                showLoginError(errorMessage);
            }
        }

        var showLoginError = function(errorMessage) {
            var expandedErrorMessage = "An error occurred";
            switch (errorMessage) {
                case "USER_EMAIL_NON_SPIDER":
                    expandedErrorMessage = "Looks like you have used a Windows Live Id which is not the registered id. Please log in to Windows Live with the registered SpiderLogic user name.";
                    $scope.showLiveLogout = true;
                    break;
                case "FAILURE_AT_WINDOWS_LIVE":
                    expandedErrorMessage = "Failure at Windows Live service side. Please try again later, or contact LMS admin.";
                    break;
                case "EMP_INVALID":
                    expandedErrorMessage = "Invalid user, user data is not found in the system.";
                    break;
                case "EMP_MULTIPLE":
                    expandedErrorMessage = "Invalid user, user data is not consistent in the system.";
                    break;
                case "INACTIVE_USER":
                    expandedErrorMessage = "Invalid user, user is not active in the system.";
                    break;
            }
            handleError(expandedErrorMessage);
        }

        $scope.signOut = function() {
            appDataStoreService.setImpersonatedUserId(null);
            appDataStoreService.setImpersonatedUserName(null);
            appDataStoreService.setUsersSelectedLocation(null);
            store.set('authToken', null);
            store.set('userInfo', null);
            $scope.userInfo = null;
            $rootScope.$broadcast('userInfoUpdate', null);
            $location.path('/signIn');
        };

        activate();
    }
})();

(function() {
  'use strict';

  // Controller name is handy for logging
  var controllerId = 'dashboardCtrl';

  angular.module('lms').controller(controllerId, ['$scope', '$log', 'dashboardService', 'authService', getDashboard]);

  function getDashboard($scope, $log, dashboardService, authService) {

    var getDashboardSummary = function() {
      dashboardService.getDashboardSummary()
        .then(function(response) {
          $scope.summaryError = false;
          $scope.dashboardSummary = response;
        }, function(err) {
          $scope.summaryError = true;
          $scope.summaryErrorMessage = err;
        });
    }

    var getRecentLeaveHistory = function() {
      dashboardService.getRecentLeaveHistory()
        .then(function(response) {
          if (response.length == 0) {
            $scope.leaveHistoryError = true;
            $scope.leaveHistoryErrorMessage = "Leave History is not found";
          } else {
            $scope.leaveHistoryError = false;
            $scope.leaveHistory = response;
          }
        }, function(err) {
          $scope.leaveHistoryError = true;
          $scope.leaveHistoryErrorMessage = err;
        });
    }

    var getRecentHolidaylist = function() {
      dashboardService.getRecentHolidaylist()
        .then(function(response) {
          $scope.holidaysError = false;
          $scope.holidayList = response;
        }, function(err) {
          $scope.holidaysError = true;
          $scope.holidaysErrorMessage = err;
        });
    }

    var checkToShowLogin = function() {
      $scope.shouldShowLogin = !authService.isUserLoggedIn();
    }

    function activate() {
      getDashboardSummary();
      getRecentLeaveHistory();
      getRecentHolidaylist();
      checkToShowLogin();
    }

    activate();
  }
})();

(function() {
    'use strict';
    var controllerId = 'compOffHistoryCtrl';
    
    angular.module('lms').controller(controllerId, ['$scope', '$log', 'utilService', 'compOffService', 'dateService', getCompOffHistory]);

    function getCompOffHistory($scope, $log, utilService, compOffService, dateService) {

        function handleError(err) {
            $scope.error = true;
            if (err)
                $scope.errorMessage = err;
            else
                $scope.errorMessage = "Retrieving compensatory-Off history failed due to internal error";
        }

        $scope.cancelCompOff = function(compOffRequestId) {
            return compOffService.cancelCompOff(compOffRequestId)
                .then(function(response) {
                    $scope.error = false;
                    utilService.refreshPage('/compOffHistory');
                }, handleError);
        }

        var getCompOffHistory = function() {
            compOffService.getCompOffForEmployee()
                .then(function(response) {
                    $scope.compOffHistory = response;

                    if (!$scope.compOffHistory || $scope.compOffHistory.length <= 0)
                        handleError("Compensatory-Off history is not available for the employee")

                }, handleError);
        };

        var getCompOffSummary = function() {
            compOffService.getCompOffSummary()
                .then(function(response) {
                    $scope.error = false;
                    $scope.compOffSummary = response;
                }, function(err) {
                    $scope.error = true;
                    $scope.compOffSummary = err;
                });
        }
        $scope.displayDateFormat = dateService.getUIDateFormat();

        function activate() {
            getCompOffHistory();
            getCompOffSummary();
        }

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'leaveDetailsCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$log', '$routeParams', 'utilService', 'leaveService', 'dateService', leaveDetailsCtrl]);

    function leaveDetailsCtrl($scope, $log, $routeParams, utilService, leaveService, dateService) {

        $scope.leaveRequestId = null;

        var handleError = function(err) {
            $scope.error = true;
            if (err)
                $scope.errorMessage = err;
            else
                $scope.errorMessage = "Retrieving leave history failed due to internal error. ";
        }

        $scope.cancelLeave = function() {
            return leaveService.cancelLeave($scope.leaveRequestId)
                .then(function(response) {
                    $scope.error = false;
                    utilService.refreshPage("/leaveDetails/"+ $scope.leaveRequestId);
                }, handleError);
        }

        var getLeaveDetails = function() {
            leaveService.getLeaveDetails($scope.leaveRequestId)
                .then(function(response) {
                    $scope.leaveDetails = response;
                    if (!$scope.leaveDetails)
                        handleError("Leave details are not available for the employee");

                }, handleError);
        };

        var getLeaveStatusHistory = function() {
            leaveService.getLeaveStatusHistory($scope.leaveRequestId)
                .then(function(response) {
                    $scope.leaveStatusHistory = response;
                    if (!$scope.leaveStatusHistory)
                        handleError("Leave status history is not available for the employee");
                }, handleError);
        };

        $scope.displayDateFormat = dateService.getUIDateFormat();

        var activate = function() {
            if ($routeParams.leaveRequestId > 0) {
                $scope.leaveRequestId = $routeParams.leaveRequestId;
                getLeaveDetails();
                getLeaveStatusHistory();
            }
            else{
              handleError("Invalid leave request id.")
            }
        }

        activate();
    }
})();

(function() {
    'use strict';

    // Controller name is handy for logging
    var controllerId = 'leaveHistoryCtrl';

    // Define the controller on the module.
    // Inject the dependencies.
    // Point to the controller definition function.
    angular.module('lms').controller(controllerId, ['$scope', '$log', 'utilService', 'leaveService', 'dateService', 'leaveCreditService', getLeaveHistory]);

    function getLeaveHistory($scope, $log, utilService, leaveService, dateService, leaveCreditService) {

        function handleError(err) {
            $scope.error = true;
            if (err)
                $scope.errorMessage = err;
            else
                $scope.errorMessage = "Retrieving leave history failed due to internal error. ";
        }

        $scope.cancelLeave = function(leaveRequestId) {
            return leaveService.cancelLeave(leaveRequestId)
                .then(function(response) {
                    $scope.error = false;
                    utilService.refreshPage('/leaveHistory');
                }, handleError);
        }

        var getLeaveHistory = function() {
            leaveService.getLeaveHistoryForEmployee()
                .then(function(response) {
                    $scope.leaveHistory = response;

                    if (!$scope.leaveHistory || $scope.leaveHistory.length <= 0)
                        handleError("Leave history is not available for the employee")

                }, handleError);
        };

        var getLeaveAccrualHistory = function() {
            leaveCreditService.getLeaveAccrualHistoryForEmployee()
                .then(function(response) {
                    $scope.leaveAccrualHistory = response;
                    if (!$scope.leaveAccrualHistory || $scope.leaveAccrualHistory.length <= 0)
                        handleError("Leave accrual history is not available for the employee")

                }, handleError);
        };

        var getLeaveEncashment = function() {
            leaveCreditService.getLeaveEncashmentDetailsForEmployee()
                .then(function(response) {
                    $scope.error = false;
                    $scope.leaveEncashmentDetails = response;
                }, function(err) {
                    $scope.error = true;
                    $scope.errorMessage = "Leave Encahsment Details not available for the employee";
                });
        };

        $scope.displayDateFormat = dateService.getUIDateFormat();

        $scope.viewLeaveDetails = function(leaveRequestId) {
            var nextPage = "#/leaveDetails/" + leaveRequestId
            window.location = nextPage;
        };

        function activate() {
            getLeaveHistory();
            getLeaveAccrualHistory();
            getLeaveEncashment();
        }

        activate();
    }
})();
(function () {
    'use strict';

    var controllerId = 'holidayListCtrl';

    angular.module('lms').controller(controllerId, ['$scope', 'holidayService', 'leaveService', 'authService','appDataStoreService', showHolidayList]);

    function showHolidayList($scope, holidayService, leaveService, authService, appDataStoreService) {

        var getHolidaylist = function () {
            holidayService.getHolidaylist()
                .then(function (response) {
                    $scope.error = false;
                    $scope.holidayList = response;
                }, function (err) {
                    $scope.error = true;
                    $scope.errorMessage = err;
                });
        };

        var getRestrictedHoliday = function () {

            holidayService.getRestrictedHolidayList().then(function (response) {
                $scope.error = false;
                $scope.restrictedHolidaySelected = response;
            }, function (err) {
                $scope.error = true;
                $scope.errorMessage = err;
            });
        };


        var cancelLeave = function (selectedDate) {
            leaveService.getLeaveRequestsForEmployeeOnADay(selectedDate).then(function (leaveRequests) {
                
                var leaveType = 0;

                leaveRequests.forEach(function (leaveRequest) {
                    leaveType = leaveRequest.LeaveTypeId;                   
                    leaveService.cancelLeave(leaveRequest.id);
                });

            }, function (err) {
                console.log(err);
            });
        };

        $scope.submit = function () {
            holidayService.setRestrictedHolidayList($scope.restrictedHolidayId).then(function (params) {
                var selectedDate = '';
                if ($scope.restrictedHolidayId === 1) {
                    selectedDate = '03-29-2017';
                } else {
                    selectedDate = '06-26-2017';
                }
                //cancelLeave(selectedDate);
                getHolidaylist();
                getRestrictedHoliday();
            }, function (err) {
                $scope.error = true;
                $scope.errorMessage = err;
            });
        };

        var activate = function () {
            $scope.restrictedHolidayId = null;
            
            var userInfo = authService.getUserInfo();
            var impersonatedUserLocation = appDataStoreService.getImpersonatedUserLocation(userInfo.id);            
            if(impersonatedUserLocation) {
                $scope.showRestrictedHolidaySection = impersonatedUserLocation === 1;    
            } else {
                $scope.showRestrictedHolidaySection = userInfo.LocationId === 1;
            }            
            getHolidaylist();
            getRestrictedHoliday();
        };
        
        $scope.showRestrictedHolidaySection = false;
        activate();
    }
})();
(function() {
    'use strict';

    var controllerId = 'navbarCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', '$location', '$interval', 'authService', 'locationService', 'appDataStoreService', navbarCtrl]);

    function navbarCtrl($scope, $rootScope, $log, $location, $interval, authService, locationService, appDataStoreService) {

        $scope.$on('userInfoUpdate', function(event, data) {
            $scope.userInfo = data;
            SetUserBoxData();
        });

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred";
        }

        var activate = function() {
            SetUserInfo();
        };

        var setUserInfoExecution;

        var SetUserInfo = function() {
            setUserInfoExecution = $interval(SetUserInfoWithRetry, 100, 10);
        }

        var SetUserInfoWithRetry = function() {
            if ($scope.userInfo)
                $interval.cancel(setUserInfoExecution);

            $scope.userInfo = authService.getUserInfo();
            if ($scope.userInfo) {
                SetUserBoxData();
                SetLocationDropDownData();
            }
        }

        var SetLocationDropDownData = function() {
            if (authService.isAdmin()) {
                locationService.getLocationDropdownList()
                    .then(function(response) {
                        $scope.locationOptions = response;
                        $scope.selectedLocation = $scope.locationOptions[0];
                    }, handleError);
            }
        }

        $scope.getLocationBoxClass = function() {
            switch ($scope.selectedLocation.id) {
                case 0: return 'AllLocationBox';
                case 1: return 'BangaloreLocationBox';
                case 2: return 'PuneLocationBox';
            }
        }

        $scope.onUserBoxClicked = function() {
            $location.path('/impersonateUser');
        }

        $rootScope.onLocationChanged = function(usersLocation) {
            appDataStoreService.setUsersSelectedLocation(usersLocation.id);
            $scope.$emit('location-updated', {});
        }

        var SetUserBoxData = function() {
            var getCurrentImpersonatedUserName = appDataStoreService.getCurrentImpersonatedUserName();
            if ($scope.userBox == null) {
                $scope.userBox = {};
            }

            if (getCurrentImpersonatedUserName != null) {
                $scope.userBox.Name = getCurrentImpersonatedUserName;
                $scope.userBox.Image = 'images/impersonate.png'
                $scope.userBox.BoxClass = 'ImpersonatedUserBox';
            } else {
                if ($scope.userInfo == null) {
                    $scope.userBox.Name = null;
                } else {
                    $scope.userBox.Name = $scope.userInfo.FirstName;
                }

                $scope.userBox.Image = 'images/user.jpg'
                $scope.userBox.BoxClass = 'LoggedInUserBox';
            }
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterImpersonatedUserUpdated = $rootScope.$on('impersonatedUser-updated', function(event, eventObj) {
            SetUserBoxData();
        });

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterImpersonatedUserUpdated);

        activate();
    };
})();

(function() {
    'use strict';

    var adminApprovalsService = function($q, $log, webServiceInvoker, appDataStoreService, dateService) {

        var getCompOffsToApprove = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/compOffsToApprove', 'CompOff List is not available due to internal error')
                .then(function(response) {
                        deferred.resolve(formatCompOffRequests(response));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getLeavesToApprove = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leavesToApprove', 'Leave List is not available due to internal error')
                .then(function(response) {
                        deferred.resolve(formatLeaveRequests(response));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var formatLeaveRequests = function(leaveRequests) {
            leaveRequests.forEach(function(leaveRequest, index) {
                leaveRequest.FromDate = dateService.getSortableDateFormat(leaveRequest.FromDate);
                leaveRequest.ToDate = dateService.getSortableDateFormat(leaveRequest.ToDate);
                leaveRequest.AppliedOn = dateService.getSortableDateFormat(leaveRequest.AppliedOn);
                leaveRequest.TotalLeaveDays = leaveRequest.TotalLeaveHours / 8;
                leaveRequest.TotalCompOffDays = leaveRequest.TotalCompOffHours / 8;
                if (leaveRequest.TotalCompOffDays == 0)
                    leaveRequest.TotalCompOffDays = null;

            });

            return leaveRequests;
        }

        var formatCompOffRequests = function(compOffRequests) {
            compOffRequests.forEach(function(compOffRequest, index) {
                compOffRequest.FromDate = dateService.getSortableDateFormat(compOffRequest.FromDate);
                compOffRequest.ToDate = dateService.getSortableDateFormat(compOffRequest.ToDate);
                compOffRequest.AppliedOn = dateService.getSortableDateFormat(compOffRequest.AppliedOn);
                compOffRequest.TotalDays = compOffRequest.TotalCompOffHours / 8;
            });

            return compOffRequests;
        }

        var getSourceGridRows = function(requestType, selectedLeaves, selectedCompOffs) {
            switch (requestType) {
                case 'Leave':
                    return selectedLeaves;
                case 'CompOff':
                    return selectedCompOffs;
            }
        }

        var buildSelectedIds = function(sourceGridRows) {
            var selectedIds = [];

            sourceGridRows.forEach(function(selectedGridRow) {
                selectedIds.push(selectedGridRow.Id);
            });

            return selectedIds;
        }

        var isApprovingOwnRequest = function(sourceGridRows) {
            var currentLoggedInUserId = appDataStoreService.getLoggedInUserId();
            var doesUserMatchForTheRow = false;

            sourceGridRows.forEach(function(selectedGridRow) {
                if (selectedGridRow.EmployeeId == currentLoggedInUserId) {
                    doesUserMatchForTheRow = true;
                }
            });

            return doesUserMatchForTheRow;
        }

        var getUpdateRequestStatusUrl = function(requestType) {
            var updateRequestStatusUrl = null;
            switch (requestType) {
                case 'Leave':
                    updateRequestStatusUrl = '/api/updateLeaveStatus';
                    break;
                case 'CompOff':
                    updateRequestStatusUrl = '/api/updateCompOffStatus';
                    break;
            }
            return updateRequestStatusUrl;
        }

        var submitRequestStatusChange = function(requestType, newStatus, selectedLeaves, selectedCompOffs, comments) {
            var deferred = $q.defer();

            var sourceGridRows = getSourceGridRows(requestType, selectedLeaves, selectedCompOffs);

            if (isApprovingOwnRequest(sourceGridRows)) {
                deferred.reject('You cannot modify your own requests. Please request another admin to approve the same.');
            } else {
                var selectedIds = buildSelectedIds(sourceGridRows);
                if (selectedIds != null) {
                    var updateRequestStatusUrl = getUpdateRequestStatusUrl(requestType);

                    webServiceInvoker.performPost(updateRequestStatusUrl, {
                            selectedIds: selectedIds,
                            status: newStatus,
                            comments: comments
                        })
                        .then(function(data) {
                            deferred.resolve('');
                        }, function(err) {
                            deferred.reject(err);
                        });
                } else {
                    deferred.reject('No rows selected for update.');
                }
            }

            return deferred.promise;
        }

        return {
            getCompOffsToApprove: getCompOffsToApprove,
            getLeavesToApprove: getLeavesToApprove,
            submitRequestStatusChange: submitRequestStatusChange
        };

    };

    angular.module('lms').factory("adminApprovalsService", ['$q', '$log', 'webServiceInvoker', 'appDataStoreService', 'dateService', adminApprovalsService]);
})();

(function() {
    'use strict';

    var adminReportsService = function($q, $log, webServiceInvoker, dateService) {

        var getLeaveRequestReport = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveRequestReport', 'Leave Request Report is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].FromDate = dateService.getSortableDateFormat(data[i].FromDate);
                            data[i].ToDate = dateService.getSortableDateFormat(data[i].ToDate);
                            data[i].TotalLeaveDays = data[i].TotalLeaveHours / 8;
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getLeaveSummaryReport = function(fromMonth, toMonth) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveSummaryReport?fromMonth=' + fromMonth + '&toMonth=' + toMonth, 'Leave Summary Report is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].LeaveDate = dateService.getSortableDateFormat(data[i].LeaveDate);
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getLeaveBalanceReport = function(fromMonth, toMonth) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveBalanceReport', 'Leave Balance Report is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].LeaveDate = dateService.getSortableDateFormat(data[i].LeaveDate);
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getCompOffSummaryReport = function(fromMonth, toMonth) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/compOffSummaryReport?fromMonth=' + fromMonth + '&toMonth=' + toMonth, 'Comp-Off Summary Report is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].LeaveDate = dateService.getSortableDateFormat(data[i].LeaveDate);
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getNewJoineeReport = function(fromMonth, toMonth) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/newJoineeReport?fromMonth=' + fromMonth + '&toMonth=' + toMonth, 'New Joinee Report is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].DateOfJoining = dateService.getSortableDateFormat(data[i].DateOfJoining);
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getEmployeeExitReport = function(fromMonth, toMonth) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/employeeExitReport?fromMonth=' + fromMonth + '&toMonth=' + toMonth, 'Employee Exit Report is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].DateOfJoining = dateService.getSortableDateFormat(data[i].DateOfExit);
                            if (data[i].UtilizedLeave == null)
                                data[i].UtilizedLeave = 0;
                            else {
                                data[i].UtilizedLeave = data[i].UtilizedLeave / 8;
                            }

                            if (data[i].AccruedDuringYear == null)
                                data[i].AccruedDuringYear = 0;
                            if (data[i].ClosingBalance == null)
                                data[i].ClosingBalance = 0;
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getLossOfPayReport = function(fromMonth, toMonth, year) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/lossOfPayReport?fromMonth=' + fromMonth + '&toMonth=' + toMonth + '&year=' + year, 'Loss Of Pay Report is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].FromDate = dateService.getSortableDateFormat(data[i].FromDate);
                            data[i].ToDate = dateService.getSortableDateFormat(data[i].ToDate);                            

                            if (data[i].TotalLeaveHours == null)
                                data[i].TotalLeaveDays = 0;
                            else {
                                data[i].TotalLeaveDays = data[i].TotalLeaveHours / 8;
                            }
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getLeaveEncashmentReport = function(fromMonth, toMonth) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveEncashmentReport', 'Leave Encashment Report is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].LeaveDate = dateService.getSortableDateFormat(data[i].LeaveDate);
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getCompOffExpiryReport = function(fromMonth, toMonth) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/compOffExpiryReport', 'Comp-Off Expiry Report is not available due to internal error')
                .then(function(data) {                                               
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        return {
            getLeaveRequestReport: getLeaveRequestReport,
            getNewJoineeReport: getNewJoineeReport,
            getEmployeeExitReport: getEmployeeExitReport,
            getLossOfPayReport: getLossOfPayReport,
            getLeaveSummaryReport: getLeaveSummaryReport,
            getCompOffSummaryReport: getCompOffSummaryReport,
            getLeaveBalanceReport: getLeaveBalanceReport,
            getLeaveEncashmentReport: getLeaveEncashmentReport,
            getCompOffExpiryReport: getCompOffExpiryReport  
        };

    };

    angular.module('lms').factory("adminReportsService", ['$q', '$log', 'webServiceInvoker', 'dateService', adminReportsService]);
})();

(function() {

    'use strict';

    var appDataStoreService = function($q, $log, authService) {
        var userLocation = {};
        var impersonatedUserId = {};
        var impersonatedUserName = {};
        var impersonatedUserLocation = {};

        var getUserLocation = function(userId) {
            return userLocation[userId];
        }
        var storeUserLocation = function(userId, location) {
            userLocation[userId] = location;
        }

        var getImpersonatedUserId = function(userId) {
            return impersonatedUserId[userId];
        }
        var storeImpersonatedUserId = function(userId, impersonatedUser) {
            impersonatedUserId[userId] = impersonatedUser;
        }

        var setImpersonatedUserLocation = function(location) {
            var userInfo = authService.getUserInfo();
            impersonatedUserLocation[userInfo.id] = location;
        };

        var getImpersonatedUserLocation = function(userId) {
            return impersonatedUserLocation[userId];
        };

        var getImpersonatedUserName = function(userId) {
            return impersonatedUserName[userId];
        }
        var storeImpersonatedUserName = function(userId, impersonatedUser) {
            impersonatedUserName[userId] = impersonatedUser;
        }

        var setUsersSelectedLocation = function(location) {
            var userInfo = authService.getUserInfo();
            storeUserLocation(userInfo.id, location);
        }

        var getUsersSelectedLocation = function() {
            var userInfo = authService.getUserInfo();
            if (userInfo != null) {
                var usersSelectedLocation = getUserLocation(userInfo.id);
                if (usersSelectedLocation != null) {
                    return usersSelectedLocation;
                }
            }
            return 0;
        }

        var setImpersonatedUserId = function(impersonatedUserId) {
            var userInfo = authService.getUserInfo();
            storeImpersonatedUserId(userInfo.id, impersonatedUserId);
        }

        var getCurrentImpersonatedUserId = function() {
            var userInfo = authService.getUserInfo();
            if (userInfo != null) {
                var currentUsersImpersonatedUserId = getImpersonatedUserId(userInfo.id);
                if (currentUsersImpersonatedUserId != null) {
                    return currentUsersImpersonatedUserId;
                }
            }
            return null;
        }

        var setImpersonatedUserName = function(impersonatedUserName) {
            var userInfo = authService.getUserInfo();
            storeImpersonatedUserName(userInfo.id, impersonatedUserName);
        }

        var getCurrentImpersonatedUserName = function() {
            var userInfo = authService.getUserInfo();
            if (userInfo != null) {
                return getImpersonatedUserName(userInfo.id);
            }
            return null;
        }

        var getLoggedInUserId = function() {
            var user = authService.getUserInfo();
            if (user == null)
                return null;
            else
                return user.id;
        }

        var getEffectiveUserId = function() {
            var effectiveUserId = getCurrentImpersonatedUserId();
            if (!effectiveUserId) {
                effectiveUserId = getLoggedInUserId();
            }
            return effectiveUserId;
        };

        return {
            getUsersSelectedLocation: getUsersSelectedLocation,
            setUsersSelectedLocation: setUsersSelectedLocation,
            getCurrentImpersonatedUserId: getCurrentImpersonatedUserId,
            getImpersonatedUserLocation : getImpersonatedUserLocation,
            setImpersonatedUserId: setImpersonatedUserId,
            setImpersonatedUserLocation: setImpersonatedUserLocation,
            getCurrentImpersonatedUserName: getCurrentImpersonatedUserName,
            setImpersonatedUserName: setImpersonatedUserName,
            getLoggedInUserId: getLoggedInUserId,
            getEffectiveUserId: getEffectiveUserId
        };

    };

    angular.module('lms').factory("appDataStoreService", ['$q', '$log', 'authService', appDataStoreService]);

})();

(function() {

    'use strict';

    var applyService = function($http, $q, $log, utilService, appDataStoreService, dateService) {

        var applyForLeave = function(selectedLeaveType, validLeaveDays, comments, emailRecipients) {

            var deferred = $q.defer();
            var leaveDetail = {
                leaveDays: [],
                selectedLeaveType: selectedLeaveType,
                comments: comments,
                emailRecipients: _.pluck(emailRecipients, 'OfficialEmailId')
            };

            var totalLeaveHours = 0;
            validLeaveDays.forEach(function(validLeaveDay) {
                if (validLeaveDay.ApplyForLeave > 0) {
                    totalLeaveHours = totalLeaveHours + (validLeaveDay.ApplyForLeave === 1 ? 4 : 8);
                    var leaveAppliedDate = dateService.formatDateWithCustomFormat(validLeaveDay.ApplyDate, 'MM-dd-yyyy');
                    var selectedCompOffDays = validLeaveDay.CompOff;
                    var processedCompOffDays = [];

                    if (selectedCompOffDays != null && selectedCompOffDays.length > 0) {
                        if (validLeaveDay.FullDayCompOffSelected || (validLeaveDay.FirstHalfDayCompOffSelected && validLeaveDay.SecondHalfDayCompOffSelected)) {
                            processedCompOffDays = selectedCompOffDays;
                        } else if (validLeaveDay.FirstHalfDayCompOffSelected) {
                            if (selectedCompOffDays.length > 1)
                                processedCompOffDays = selectedCompOffDays.splice(0, 1);
                            else
                                processedCompOffDays = selectedCompOffDays;
                        } else if (validLeaveDay.SecondHalfDayCompOffSelected) {
                            processedCompOffDays = selectedCompOffDays.splice(1, validLeaveDay.ApplyForLeave);
                        }

                        totalLeaveHours = totalLeaveHours - (processedCompOffDays.length > 0 ? (processedCompOffDays.length === 1 ? 4 : 8) : 0);
                    }
                    leaveDetail.leaveDays.push({
                        'leaveDay': leaveAppliedDate,
                        'isHalfDay': validLeaveDay.ApplyForLeave === 1,
                        'compOffs': processedCompOffDays
                    });
                }
            });

            var availableLeaveHours = selectedLeaveType.AvailableBalance * 8;

            if (selectedLeaveType.LeaveTypeName == 'LossOfPay') {
                //Identify starting and ending days for leave
                var leaveStartingDay, leaveEndingDay = null;
                validLeaveDays.forEach(function(validLeaveDay) {
                    if ((validLeaveDay.ApplyForLeave == 1) || (validLeaveDay.ApplyForLeave == 2)) {
                        if (leaveStartingDay == null)
                            leaveStartingDay = new Date(validLeaveDay.ApplyDate);
                        leaveEndingDay = new Date(validLeaveDay.ApplyDate);
                    }
                });

                if (leaveStartingDay < leaveEndingDay) {
                    var overlappingAnnualLeavesFound = false;
                    validLeaveDays.forEach(function(validLeaveDay) {
                        if (((validLeaveDay.LeaveType == 'Annual') || (validLeaveDay.CompOffStatus == 'Approved')) &&
                            (new Date(validLeaveDay.ApplyDate) > leaveStartingDay.setHours(0, 0, 0, 0)) &&
                            (new Date(validLeaveDay.ApplyDate) <= leaveEndingDay.setHours(0, 0, 0, 0))) {
                            overlappingAnnualLeavesFound = true;
                        }
                    });
                    if (overlappingAnnualLeavesFound) {
                        deferred.reject("There are some Annual leaves within the selected date range. Please select a different date range OR cancel and reapply the Annual leaves towards the beginning of the range and then apply for LOP.");
                    }
                }
            }


            if (!leaveDetail.comments) {
                deferred.reject("Please enter a comment");
            } else if (leaveDetail.leaveDays.length == 0) {
                deferred.reject("Please select at least half a day to apply leave");
            } else if (totalLeaveHours > availableLeaveHours) {
                deferred.reject("Leave balance for selected leave type (" + selectedLeaveType.LeaveTypeDesc + ") is only " + (selectedLeaveType.AvailableBalance) + ", but you have selected " + (totalLeaveHours / 8));
            }else if(!angular.isDefined(emailRecipients) || emailRecipients.length == 0){
              deferred.reject("Please select at least one email recipient.");
            }else if (!areEmailIdsValid(emailRecipients)){
                deferred.reject("Email id (" + getInvalidEmailID(emailRecipients) + ") is invalid. Email id should end with either spiderlogic.com or wipfli.com");
            }else {
                var selectedUserId = appDataStoreService.getEffectiveUserId();
                $http.post(utilService.getUrlWithParams('/api/applyLeave', selectedUserId), JSON.stringify(leaveDetail)).then(
                    function(response) {
                        if (response.data.error) {
                            deferred.reject(response.data.error);
                        } else {
                            deferred.resolve('');
                        }
                    });
            }

            return deferred.promise;
        };

        var areEmailIdsValid = function(emailIds){
          if (angular.isDefined(emailIds)){
            for (var i = 0; i < emailIds.length; ++i) {
              if(!utilService.isSpiderOrWipfliEmail(emailIds[i].OfficialEmailId)){
                return false;
              }
            }
            return true;
          }
        }

        var getInvalidEmailID = function(emailIds){
          if (angular.isDefined(emailIds)){
            for (var i = 0; i < emailIds.length; ++i) {
              if(!utilService.isSpiderOrWipfliEmail(emailIds[i].OfficialEmailId)){
                return emailIds[i].OfficialEmailId;
              }
            }
            return "";
          }
        }

        var applyForCompOff = function(validCompOffDays, comments, emailRecipients) {

            var deferred = $q.defer();
            var compOffDetail = {
                compOffDays: [],
                comments: comments,
                emailRecipients: _.pluck(emailRecipients, 'OfficialEmailId')
            };

            validCompOffDays.forEach(function(validCompOffDay) {

                if (validCompOffDay.Apply > 0) {
                    var compOffAppliedDate = dateService.formatDateWithCustomFormat(validCompOffDay.ApplyDate, 'MM-dd-yyyy');
                    compOffDetail.compOffDays.push({
                        'CompOffDay': compOffAppliedDate,
                        'IsHalfDay': validCompOffDay.Apply === 1,
                    });
                }
            });

            if (!compOffDetail.comments) {
                deferred.reject("Please enter a comment");
            } else if (compOffDetail.compOffDays.length == 0) {
                deferred.reject("Please select at least half a day to apply for Comp-Off");
            }else if(!angular.isDefined(emailRecipients) || emailRecipients.length == 0){
              deferred.reject("Please select at least one email recipient.");
            }else if (!areEmailIdsValid(emailRecipients)){
                deferred.reject("Email id (" + getInvalidEmailID(emailRecipients) + ") is invalid. Email id should end with either spiderlogic.com or wipfli.com");
            }else {
                var selectedUserId = appDataStoreService.getEffectiveUserId();
                $http.post(utilService.getUrlWithParams('/api/applyCompOffSubmit', selectedUserId), JSON.stringify(compOffDetail)).then(
                    function(response) {
                        if (response.data.error) {
                            deferred.reject(response.data.error);
                        } else {
                            deferred.resolve('');
                        }
                    });
            }

            return deferred.promise;
        };

        return {
            applyForLeave: applyForLeave,
            applyForCompOff: applyForCompOff
        };
    };

    angular.module('lms').factory("applyService", ['$http', '$q', '$log', 'utilService', 'appDataStoreService', 'dateService', applyService]);

})();

(function () {
    'use strict';

    var attendanceService = function ($q, $log, webServiceInvoker) {

        var markAttendance = function (attendance) {
            var deferred = $q.defer();
            console.log('attendanceService hitting');
            webServiceInvoker.performPost('/api/markAttendance', attendance)
                .then(function (data) {
                    deferred.resolve('');
                }, function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var updateAttendance = function (attendance) {
            var deferred = $q.defer();
            console.log('attendanceService updateAttendance hitting');
            webServiceInvoker.performPost('/api/updateAttendance', attendance)
                .then(function (data) {
                    deferred.resolve('');
                }, function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var getUserAttendance = function (date) {
            var deferred = $q.defer();
            console.log('attendanceService hitting');
            webServiceInvoker.performGet('/api/attendance?Date=' + date)
                .then(function (data) {
                    deferred.resolve(data);
                }, function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        return {
            markAttendance: markAttendance,
            getUserAttendance: getUserAttendance,
            updateAttendance: updateAttendance
        };
    };

    angular.module('lms').factory("attendanceService", ['$q', '$log', 'webServiceInvoker', attendanceService]);
})();

(function() {
    'use strict';

    var attendanceStatusService = function($q, $log, webServiceInvoker) {

        var getAttendnaceStatusName = function() {
            var defered = $q.defer();
            console.log('attendanceService hitting');
            webServiceInvoker.performGet('/api/attendanceStatusName','Attendnace status not available due to internal error')
                .then(function(data) {
                    defered.resolve(data)
                }, function(err) {
                    defered.reject(err);
                });

            return defered.promise;
        }


        return {
            getAttendnaceStatusName: getAttendnaceStatusName
        };
    };

    angular.module('lms').factory("attendanceStatusService", ['$q', '$log', 'webServiceInvoker', attendanceStatusService]);
})();
(function() {
  'use strict';

  var authService = function($log, store) {

    var getUserInfo = function() {
      return store.get('userInfo');
    };

    var getAuthToken = function() {
      return store.get('authToken');
    };

    var setAuthToken = function(token) {
      store.set('authToken', token);
    };

    var isUserLoggedIn = function() {
      return (getUserInfo() != null);
    }

    var isAdmin = function() {
      var userInfo = getUserInfo();
      if (!userInfo) {
        return false;
      } else {
        return userInfo.IsAdmin;
      }
    }

    var exculdeLoggedInEmp = function(employeeList) {
       var loggedInAdminUser = getUserInfo();
       if (loggedInAdminUser != null) {
           return employeeList.filter(function(employee) {
               return employee.id != loggedInAdminUser.id
           });
       } else {
           return employeeList;
       }
   }

    return {
      token : '',
      getUserInfo : getUserInfo,
      getAuthToken : getAuthToken,
      setAuthToken: setAuthToken,
      isUserLoggedIn : isUserLoggedIn,
      isAdmin : isAdmin,
      exculdeLoggedInEmp:exculdeLoggedInEmp
    };
  }

  angular.module('lms').factory('authService', ['$log', 'store', authService]);
})();

(function() {
    'use strict';

    var calenderService = function($q, $log) {

        var getMonthDropDownDetails = function() {
            var monthDropDownDetails = {};

            monthDropDownDetails.months = [{
                id: 1,
                Description: 'January'
            }, {
                id: 2,
                Description: 'February'
            }, {
                id: 3,
                Description: 'March'
            }, {
                id: 4,
                Description: 'April'
            }, {
                id: 5,
                Description: 'May'
            }, {
                id: 6,
                Description: 'June'
            }, {
                id: 7,
                Description: 'July'
            }, {
                id: 8,
                Description: 'August'
            }, {
                id: 9,
                Description: 'September'
            }, {
                id: 10,
                Description: 'October'
            }, {
                id: 11,
                Description: 'November'
            }, {
                id: 12,
                Description: 'December'
            }];

            var currentDate = new Date();
            monthDropDownDetails.currentMonth = currentDate.getMonth() + 1 ; //javascript months are 0 based

            return monthDropDownDetails;
        }

        var getYearDropDownDetails = function() {
            var yearDropDownDetails = {};           

            var currentDate = new Date();

            yearDropDownDetails.years = [{
                id: currentDate.getFullYear(),
                Description: currentDate.getFullYear() 
            }, {
                id: currentDate.getFullYear() - 1,
                Description: currentDate.getFullYear() - 1 
            }, {
                id: currentDate.getFullYear() - 2,
                Description: currentDate.getFullYear() - 2 
            }, {
                id: currentDate.getFullYear() - 3,
                Description: currentDate.getFullYear() - 3
            }, {
                id: currentDate.getFullYear() - 4,
                Description: currentDate.getFullYear() - 4 
            }];
            
            yearDropDownDetails.currentYear = currentDate.getFullYear();
            return yearDropDownDetails;
        }

        return {
            getMonthDropDownDetails: getMonthDropDownDetails,
            getYearDropDownDetails: getYearDropDownDetails
        };

    };

    angular.module('lms').factory('calenderService', ['$q', '$log', calenderService]);
})();

(function() {

    'use strict';

    var compOffService = function($q, $log, webServiceInvoker, dateService) {

        var cancelCompOff = function(compOffRequestId) {
            var deferred = $q.defer();

            webServiceInvoker.performPost('/api/cancelCompOff', {
                    compOffRequestId: compOffRequestId,
                })
                .then(function(data) {
                    deferred.resolve('');
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var getCompOffForEmployee = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/compOffHistory', 'Compensatory-Off history is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(formatCompOffHistoryResponse(data));
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }

        var formatCompOffHistoryResponse = function(data) {
            var compOffRequests = data;
            compOffRequests.forEach(function(compOffRequest) {
                compOffRequest.NoOfDays = compOffRequest.TotalCompOffHours / 8;
                compOffRequest.AppliedOn = dateService.getSortableDateFormat(compOffRequest.AppliedOn);
                compOffRequest.CompOffDate = dateService.getSortableDateFormat(compOffRequest.CompOffDate);
                compOffRequest.ExpiresOn = dateService.getSortableDateFormat(compOffRequest.ExpiresOn);

                if (compOffRequest.AvailedOnDateMin != null)
                    compOffRequest.AvailedOnDateMin = dateService.formatDate(compOffRequest.AvailedOnDateMin);
                if (compOffRequest.AvailedOnDateMax != null)
                    compOffRequest.AvailedOnDateMax = dateService.formatDate(compOffRequest.AvailedOnDateMax);

                if (compOffRequest.AvailedOnDateMin == compOffRequest.AvailedOnDateMax) {
                    compOffRequest.AvailedOn = compOffRequest.AvailedOnDateMin;
                } else {
                    compOffRequest.AvailedOn = compOffRequest.AvailedOnDateMin + '(0.5d); ' + compOffRequest.AvailedOnDateMax + '(0.5d)';
                }
            });

            return compOffRequests;
        }

        var getCompOffSummary = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/compOffSummaryForUser', 'CompOff summary is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(formatCompOffSummary(data));
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var formatCompOffSummary = function(compOffData) {
            if (compOffData && compOffData.length > 0) {
                var compOffSummary = [];
                var formattedName, formattedValue = null;

                compOffData.forEach(function(compOffDataRow) {
                    formattedName = getFormattedName(compOffDataRow);
                    formattedValue = getFormattedValue(compOffDataRow);
                    compOffSummary.push({
                        'Name': formattedName,
                        'Value': formattedValue
                    });
                });
                return compOffSummary;
            }

            return null;
        }

        var getFormattedName = function(compOffDataRow) {
            switch (compOffDataRow.Name) {
                case 'AvailableCompOffHours':
                    return 'Comp-Off Balance';
                case 'YtdAvailedCompOffHours':
                    return 'Availed Comp-Offs (YTD)';
                case 'AppliedCompOffHours':
                    return 'Comp-Offs pending approval';
                default:
                    return compOffDataRow.Name;
            }
        }

        var getFormattedValue = function(compOffDataRow) {
            switch (compOffDataRow.Name) {
                case 'AvailableCompOffHours':
                case 'YtdAvailedCompOffHours':
                case 'AppliedCompOffHours':
                    return compOffDataRow.Value / 8;
                default:
                    return compOffDataRow.Value;
            }
        }

        return {
            cancelCompOff: cancelCompOff,
            getCompOffForEmployee: getCompOffForEmployee,
            getCompOffSummary: getCompOffSummary
        };

    };

    angular.module('lms').factory("compOffService", ['$q', '$log', 'webServiceInvoker', 'dateService', compOffService]);

})();

(function() {
    'use strict';

    var customFormatService = function($log, $filter) {

        var getFullDateWithDay = function(inputDate) {
            if (inputDate)
                return $filter('date')(inputDate, "EEEE, MMMM d, y");
            else {
                return null;
            }
        };

        return {
            getFullDateWithDay: getFullDateWithDay
        };
    }

    angular.module('lms').factory('customFormatService', ['$log', '$filter', customFormatService]);
})();

(function() {
    'use strict';

    var dashboardService = function($q, $log, webServiceInvoker, customFormatService, dateService) {

        var getDashboardSummary = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/dashboardSummaryForUser', 'Dashboard summary is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(formatDashboardSummary(data));
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var formatDashboardSummary = function(dashboardData) {
            if (dashboardData && dashboardData.length > 0) {
                var dashboardSummary = [];
                var formattedName, formattedValue = null;

                dashboardData.forEach(function(dashboardDataRow) {
                    formattedName = getFormattedName(dashboardDataRow);
                    formattedValue = getFormattedValue(dashboardDataRow);
                    dashboardSummary.push({
                        'Name': formattedName,
                        'Value': formattedValue
                    });
                });
                return dashboardSummary;
            }

            return null;
        }

        var getFormattedName = function(dashboardDataRow) {
            switch (dashboardDataRow.Name) {
                case 'AnnualLeaveBalanceDays':
                    return 'Current Leave Balance';
                case 'AppliedLeaveHours':
                    return 'Annual Leaves (Applied)';
                case 'ApprovedLeaveHours':
                    return 'Annual Leaves (Approved)';
                case 'AvailableCompOffHours':
                    return 'Comp-Off Balance';
                default:
                    return dashboardDataRow.Name;
            }
        }

        var getFormattedValue = function(dashboardDataRow) {
            switch (dashboardDataRow.Name) {
              case 'AppliedLeaveHours':
              case 'ApprovedLeaveHours':
              case 'AvailableCompOffHours':
                  return dashboardDataRow.Value / 8;
              default:
                  return dashboardDataRow.Value;
            }
        }

        var getRecentLeaveHistory = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/recentLeaveHistory', 'Leave History is not available due to internal error')
                .then(function(data) {
                    for (var i = 0; i < data.length; i++) {
                        data[i].FromDate = dateService.formatDate(data[i].FromDate);
                        data[i].ToDate = dateService.formatDate(data[i].ToDate);
                    }
                    deferred.resolve(formatLeaveList(data));
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var formatLeaveList = function(leaveList) {
            if (leaveList) {
                var statusClass = 'InProgressStatus';
                leaveList.forEach(function(leaveEntry) {
                    switch (leaveEntry.Status) {
                        case 'Applied':
                            statusClass = 'InProgressStatus';
                            break;
                        case 'Approved':
                            statusClass = 'SuccessStatus';
                            break;
                        case 'Rejected':
                            statusClass = 'FailureStatus';
                            break;
                        case 'Canceled':
                            statusClass = 'FailureStatus';
                            break;
                    }

                    leaveEntry.StatusClass = statusClass;
                });
            }
            return leaveList;
        }

        var getRecentHolidaylist = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/recentHolidaylist', 'Holiday list is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(formatHolidayList(data));
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var formatHolidayList = function(holidayList) {
            if (holidayList) {
                holidayList.forEach(function(holidayEntry) {
                    holidayEntry.ActualDate = customFormatService.getFullDateWithDay(holidayEntry.ActualDate);
                });
            }
            return holidayList;
        }

        return {
            getDashboardSummary: getDashboardSummary,
            getRecentLeaveHistory: getRecentLeaveHistory,
            getRecentHolidaylist: getRecentHolidaylist
        };
    };

    angular.module('lms').factory("dashboardService", ['$q', '$log', 'webServiceInvoker', 'customFormatService', 'dateService', dashboardService]);
})();

(function() {
  'use strict';
    var dateService = function($filter) {
    var sortableDateFormat='yyyy-MM-dd';
    var displayDateFormat='dd/MMM/yyyy';

    var formatDate = function(date) {
        return $filter('date')(new Date(date),displayDateFormat);
    };

    var formatDateObject = function(inputDate) {
        return $filter('date')(inputDate, displayDateFormat);
    };

    var getDateObjectFromDateString = function(dateString){
      if(dateString == null){
        return null;
      }
      else{
        var dateObject = new Date(dateString);
        dateObject.setHours(0, 0, 0, 0);
        return dateObject;
      }
    }

    var formatDateWithCustomFormat = function(date , format) {
        return $filter('date')(new Date(date),format);
    };

    var getUIDateFormat = function(){
      return displayDateFormat;
    }

    var getDateString = function(date) {
      return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    };

    var getSortableDateFormat = function(date){
      return $filter('date')(new Date(date), sortableDateFormat);
    }

    return {
      formatDate: formatDate,
      formatDateWithCustomFormat: formatDateWithCustomFormat,
      getUIDateFormat: getUIDateFormat,
      getDateString: getDateString,
      getSortableDateFormat: getSortableDateFormat,
      formatDateObject: formatDateObject,
      getDateObjectFromDateString: getDateObjectFromDateString
    };
  };
  angular.module('lms').factory("dateService", ['$filter', dateService]);
})();

(function() {
    'use strict';

    var dialogService = function($mdDialog) {

        var showAlert = function(message) {
            alert = $mdDialog.alert({
                title: 'Attention',
                textContent: message,
                ok: 'Close'
            });
            $mdDialog
                .show(alert)
                .finally(function() {
                    alert = undefined;
                });
        }

        return {
            showAlert: showAlert
        };
    };
    angular.module('lms').factory("dialogService", ["$mdDialog", dialogService]);
})();

(function() {

    'use strict';

    var employeeService = function($q, $log, webServiceInvoker,dateService) {

        var getEmployee = function(id) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/user?id='+ id, 'User is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(formatEmployee(data));
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var formatEmployee = function(employee) {
            employee.DateOfBirth = new Date(employee.DateOfBirth);
            employee.DateOfJoining = new Date(employee.DateOfJoining);
            employee.DateOfExit = new Date(employee.DateOfExit);
            return employee;
        }

        var getEmployees = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/userListing', 'UserList could not be fetched due to internal error')
              .then(function(data) {
                for(var i=0;i<data.length;i++)
                {
                  data[i].DateOfBirth = dateService.getSortableDateFormat(data[i].DateOfBirth);
                  data[i].DateOfJoining =dateService.getSortableDateFormat(data[i].DateOfJoining);
                  data[i].DateOfExit = dateService.getSortableDateFormat(data[i].DateOfExit);
                }

                deferred.resolve(data);
              }, function(err) {
                deferred.reject(err);
              });

            return deferred.promise;
        };

        var getActiveEmployeesEmails = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/getActiveEmployeesEmails', 'getActiveEmployeesEmails could not be fetched due to internal error')
              .then(function(data) {
                deferred.resolve(data);
              }, function(err) {
                deferred.reject(err);
              });

            return deferred.promise;
        };

        var searchEmployee = function(val) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/searchEmployee?searchString=' + val, 'User search is not available due to internal error')
                .then(function(data) {
                    var employees = [];
                    data.forEach(function(emp) {
                        emp.toString = function() {
                            return this.DisplayString;
                        };
                        employees.push(emp);
                    });
                    deferred.resolve(employees);
                }, function(err) {
                    deferred.reject(err);
                });
            return deferred.promise;
        };


        var addEmployee = function(employee) {
            var deferred = $q.defer();

            webServiceInvoker.performPost('/api/addEmployee', employee)
              .then(function(data) {
                deferred.resolve('');
              }, function(err) {
                deferred.reject(err);
              });

            return deferred.promise;
        };

        var updateEmployee = function(employee) {
            var deferred = $q.defer();

            webServiceInvoker.performPost('/api/updateEmployee', employee)
              .then(function(data) {
                deferred.resolve('');
              }, function(err) {
                deferred.reject(err);
              });

            return deferred.promise;
        };

        var validateEmployee = function(employee) {
            var validationErrors = [];
            var validationText = "";
            if (!employee) {
                validationErrors.push('Please enter mandatory fields');
            } else {
                if (!employee.id)
                    validationErrors.push('Employee Id');
                if (!employee.FirstName)
                    validationErrors.push('First Name');
                if (!employee.LastName)
                    validationErrors.push('Last Name');
                if (!employee.Location)
                    validationErrors.push('Location');
                if (!employee.EmployeeStatus)
                    validationErrors.push('Status');
                if (!employee.UserName)
                    validationErrors.push('Email');

                for(var intCount=0; intCount < validationErrors.length; intCount++)
                {
                   validationText += validationText.length <= 0 ? validationErrors[intCount] :
                                                                  ", " + validationErrors[intCount];
                }

                if(validationText.length > 0)
                {
                  if(validationErrors.length > 1)
                    validationText += " are mandatory.";
                   else {
                      validationText += " is mandatory.";
                    }

                  if(validationText.indexOf('Email') >= 0)
                  {
                    validationText += ' Email should be valid and end with spiderlogic.com.'
                  }
                }
            }

            if (validationText.length <= 0) {
                return null;
            } else {
                return validationText;
            }
        };

        return {
            searchEmployee: searchEmployee,
            addEmployee: addEmployee,
            updateEmployee: updateEmployee,
            getEmployee: getEmployee,
            getEmployees: getEmployees,
            validateEmployee: validateEmployee,
            getActiveEmployeesEmails: getActiveEmployeesEmails
        };

    };

    angular.module('lms').factory("employeeService", ['$q', '$log', 'webServiceInvoker','dateService', employeeService]);

})();

(function() {
    'use strict';

    var holidayService = function($q, $log, webServiceInvoker, customFormatService) {

        var getHolidaylist = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/holidayList', 'Holiday list is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(formatHolidayList(data));
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var formatHolidayList = function(holidayList) {
            if (holidayList) {
                holidayList.forEach(function(holidayEntry) {
                    holidayEntry.ActualDate = customFormatService.getFullDateWithDay(holidayEntry.ActualDate);
                });
            }
            return holidayList;
        }

        var getRestrictedHolidayList = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/employeeRestrictedHolidays', 'Holiday list is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(data);
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };


        
        var setRestrictedHolidayList = function(restrictedHolidayId) {
            var deferred = $q.defer();

            webServiceInvoker.performPost('/api/setEmployeeRestrictedHolidays', {RestrictedHolidayId: restrictedHolidayId})
                .then(function(data) {
                    deferred.resolve(data);
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        return {
            getHolidaylist: getHolidaylist,
            getRestrictedHolidayList : getRestrictedHolidayList,
            setRestrictedHolidayList : setRestrictedHolidayList
        };
    };

    angular.module('lms').factory("holidayService", ['$q', '$log', 'webServiceInvoker', 'customFormatService', holidayService]);
})();

(function() {

    'use strict';

    var leaveCreditService = function($q, $log, webServiceInvoker, dateService) {

        var getLeaveCredit = function(id) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveCredit?id=' + id, 'Leave Credit is not available due to internal error')
                .then(function(data) {
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getAllLeaveCredits = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveCredits', 'Leave Credit History is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].createdOn = dateService.getSortableDateFormat(data[i].createdOn);
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var getAllLeaveBalances = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveBalances', 'Leave Balance list is not available due to internal error')
                .then(function(data) {
                        for (var i = 0; i < data.length; i++) {
                            data[i].createdOn = dateService.getSortableDateFormat(data[i].createdOn);
                        }
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var performMonthlyLeaveCredit = function(numberOfLeavesCredited, forceInsert, doNotRepeatForEmployee) {
            var deferred = $q.defer();
            if (!numberOfLeavesCredited || isNaN(numberOfLeavesCredited) || numberOfLeavesCredited <= 0) {
                deferred.reject('numberOfLeavesCredited should be greater than 0');
            } else {
                webServiceInvoker.performPost('/api/monthlyLeaveCredit', {
                        numberOfLeavesCredited: numberOfLeavesCredited,
                        forceInsert: forceInsert,
                        doNotRepeatForEmployee: doNotRepeatForEmployee,
                    })
                    .then(function(data) {
                        deferred.resolve('');
                    }, function(err) {
                        deferred.reject(err);
                    });
            }
            return deferred.promise;
        };

        var deleteMonthlyLeaveCredit = function(selectedMonth) {
            var deferred = $q.defer();

            webServiceInvoker.performPost('/api/deleteMonthlyLeaveCredit', {
                    selectedMonth: selectedMonth
                })
                .then(function(data) {
                    deferred.resolve('');
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var addLeaveCredit = function(leaveCredit) {
            var deferred = $q.defer();

            webServiceInvoker.performPost('/api/addLeaveCredit', leaveCredit)
                .then(function(data) {
                    deferred.resolve('');
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var updateLeaveCredit = function(leaveCredit) {
            var deferred = $q.defer();

            webServiceInvoker.performPost('/api/updateLeaveCredit', leaveCredit)
                .then(function(data) {
                    deferred.resolve('');
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var getLeaveAccrualHistoryForEmployee = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/employeeLeaveAccrualHistory', 'Leave Accrual History is not available due to internal error')
                .then(function(data) {
                        deferred.resolve(formatLeaveAccrualHistoryResponse(data));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var formatLeaveAccrualHistoryResponse = function(data) {
            var leaveCredits = data;
            leaveCredits.forEach(function(leaveCredit) {
                leaveCredit.CreatedOn = dateService.getSortableDateFormat(leaveCredit.CreatedOn);
            });

            return leaveCredits;
        }

        var getLeaveEncashmentDetailsForEmployee = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/employeeLeaveUpdatedHistory', 'Leave Updated History is not available due to internal error')
                .then(function(data) {
                        deferred.resolve(formattedLeaveUpdatedHistory(data));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var formattedLeaveUpdatedHistory = function(data) {
            if (data && data.length > 0) {
                var leaveEncashmentDetails = [];
                leaveEncashmentDetails.push({ Name: '2016 Year End Balance', Value: data[0].Balance ? data[0].Balance : 0 });
                leaveEncashmentDetails.push({ Name: '2016 Encashed', Value: data[0].Encashable ? data[0].Encashable : 0 });
                leaveEncashmentDetails.push({ Name: '2016 Lapsed', Value: data[0].Expiring ? data[0].Expiring : 0 });
                leaveEncashmentDetails.push({ Name: '2017 Opening Balance', Value: data[0].BalanceAfterEncashment ? data[0].BalanceAfterEncashment : 0 });

                return leaveEncashmentDetails;
            }

            return null;

        };







        return {
            getAllLeaveCredits: getAllLeaveCredits,
            getLeaveCredit: getLeaveCredit,
            getAllLeaveBalances: getAllLeaveBalances,
            performMonthlyLeaveCredit: performMonthlyLeaveCredit,
            deleteMonthlyLeaveCredit: deleteMonthlyLeaveCredit,
            addLeaveCredit: addLeaveCredit,
            updateLeaveCredit: updateLeaveCredit,
            getLeaveAccrualHistoryForEmployee: getLeaveAccrualHistoryForEmployee,
            getLeaveEncashmentDetailsForEmployee: getLeaveEncashmentDetailsForEmployee
        };

    };

    angular.module('lms').factory("leaveCreditService", ['$q', '$log', 'webServiceInvoker', 'dateService', leaveCreditService]);

})();
(function() {
    'use strict';
    var leaveLookupService = function($q, $log, appDataStoreService, webServiceInvoker, dateService) {

        var adjustDateRows = function(validLeaveDays, compOffs, leaveType, fromDate, toDate) {

            var compOffsForAdjustment = compOffs ? compOffs.slice() : [];

            validLeaveDays.forEach(function(leaveDay) {
                leaveDay = resetCompOffData(leaveDay);

                if (leaveDay.Holiday || (leaveDay.IsFullDayApplied) || leaveDay.ApplyForLeave == 0)
                    return;

                if (leaveDay.ApplyForLeave == null || leaveDay.ApplyForLeave == 0)
                    leaveDay.ApplyForLeave = leaveDay.IsHalfDayApplied ? 1 : 2;

                if (leaveType == 'Annual') {
                    if (compOffsForAdjustment.length <= 0)
                        return;

                    for (var i = compOffsForAdjustment.length; i--;) {
                        if (compOffsForAdjustment[i].ExpiresOn.getTime() <= leaveDay.ApplyDate.getTime()) {
                            compOffsForAdjustment.splice(0, i + 1);
                            break;
                        }
                    }
                    leaveDay.CompOff = compOffsForAdjustment.splice(0, leaveDay.ApplyForLeave);

                    leaveDay = adjustCompOffData(leaveDay);
                }
            });

            if (leaveType == 'LossOfPay') {
                var effectiveStartDate = dateService.getDateObjectFromDateString(fromDate);

                //show dates from previous holiday start, if there are adjacent holidays and those holidays are LOP
                var daysBeforeStartDay = validLeaveDays.filter(function(element) {
                    return (effectiveStartDate.getTime() > element.ApplyDate.getTime());
                });

                for (var j = daysBeforeStartDay.length - 1; j >= 0; j--) {
                    if (daysBeforeStartDay[j].Holiday == true) {
                        continue;
                    } else {
                        if ((j < (daysBeforeStartDay.length - 1)) && ((daysBeforeStartDay[j].LeaveType == 'LossOfPay') && (daysBeforeStartDay[j].LeaveHours >= 8))) {
                            effectiveStartDate = daysBeforeStartDay[j].ApplyDate;
                        }
                        break;
                    }
                }

                var effectiveEndDate = dateService.getDateObjectFromDateString(toDate);

                //show dates from next holiday start, if there are adjacent holidays and those holidays are LOP
                var daysAfterEndDay = validLeaveDays.filter(function(element) {
                    return (effectiveEndDate.getTime() < element.ApplyDate.getTime());
                });

                for (var k = 0; k < daysAfterEndDay.length; k++) {
                    if (daysAfterEndDay[k].Holiday == true) {
                        continue;
                    } else {
                        if ((k > 0) && ((daysAfterEndDay[k].LeaveType == 'LossOfPay') && (daysAfterEndDay[k].LeaveHours >= 8))) {
                            effectiveEndDate = daysAfterEndDay[k].ApplyDate;
                        }
                        break;
                    }
                }

                validLeaveDays = validLeaveDays.filter(function(element) {
                    return ((element.ApplyDate.getTime() >= effectiveStartDate.getTime()) && (element.ApplyDate.getTime() <= effectiveEndDate.getTime()));
                });

                //set LOP for holidys if surrounded by LOP days
                validLeaveDays.filter(function(element) {
                    return ((element.Holiday == true) && (element.LeaveType == null));
                }).forEach(function(leaveDay) {
                    if (leaveDay.Holiday) {
                        leaveDay = setHolidayLOPData(validLeaveDays, leaveDay);
                    }
                });
            }

            //reset dates for display format
            for (var i = 0; i < validLeaveDays.length; i++) {
                validLeaveDays[i].ApplyDateForDisplay = dateService.formatDateObject(validLeaveDays[i].ApplyDate);
                if (validLeaveDays[i].CompOff != null && validLeaveDays[i].CompOff.length > 0) {
                    for (var j = 0; j < validLeaveDays[i].CompOff.length; j++) {
                        validLeaveDays[i].CompOff[j].ExpiresOnForDisplay = dateService.formatDateObject(validLeaveDays[i].CompOff[j].ExpiresOn);
                        validLeaveDays[i].CompOff[j].CompOffDateForDisplay = dateService.formatDateObject(validLeaveDays[i].CompOff[j].CompOffDate);
                    }
                }
            }

            return validLeaveDays;
        };

        var setHolidayLOPData = function(validLeaveDays, leaveDay) {
            var hasAppliedForFullDayLOPBeforeHoliday = false,
                hasAppliedForFullDayLOPAfterHoliday = false;

            var daysBeforeHoliday = validLeaveDays.filter(function(element) {
                return (leaveDay.ApplyDate.getTime() > element.ApplyDate.getTime());
            });

            for (var j = daysBeforeHoliday.length - 1; j >= 0; j--) {
                if ((daysBeforeHoliday[j] == null) || (daysBeforeHoliday[j].Holiday == true))
                    continue;
                if (((daysBeforeHoliday[j].LeaveType == 'LossOfPay') && (daysBeforeHoliday[j].LeaveHours >= 8))
                    //If the previous non-holiday had full day LOP
                    ||
                    (daysBeforeHoliday[j].ApplyForLeave == 2))
                //or if the day is selected as full day LOP currently
                {
                    hasAppliedForFullDayLOPBeforeHoliday = true;
                    break;
                } else {
                    hasAppliedForFullDayLOPBeforeHoliday = false;
                    break;
                }
            }

            var daysAfterHoliday = validLeaveDays.filter(function(element) {
                return (leaveDay.ApplyDate.getTime() < element.ApplyDate.getTime());
            });

            for (var k = 0; k < daysAfterHoliday.length; k++) {
                if ((daysAfterHoliday[k] == null) || (daysAfterHoliday[k].Holiday == true))
                    continue;
                if (((daysAfterHoliday[k].LeaveType == 'LossOfPay') && (daysAfterHoliday[k].LeaveHours >= 8))
                    //If the next non-holiday had full day LOP
                    ||
                    (daysAfterHoliday[k].ApplyForLeave == 2))
                //or if the day is selected as full day LOP currently
                {
                    hasAppliedForFullDayLOPAfterHoliday = true;
                    break;
                } else {
                    hasAppliedForFullDayLOPAfterHoliday = false;
                    break;
                }
            }

            if (hasAppliedForFullDayLOPBeforeHoliday && hasAppliedForFullDayLOPAfterHoliday) {
                leaveDay.ApplyForLeave = 2;
            } else {
                leaveDay.ApplyForLeave = 0;
            }
            return leaveDay;
        }

        var resetCompOffData = function(leaveDay) {
            leaveDay.CompOff = null;
            leaveDay.FullDayCompOffSelected = false;
            leaveDay.FirstHalfDayCompOffSelected = false;
            leaveDay.SecondHalfDayCompOffSelected = false;
            leaveDay.SplitCompOffIsAvailable = false;
            leaveDay.FullDayCompOffIsAvailable = false;
            leaveDay.HalfDayCompOffIsAvailable = false;
            return leaveDay;
        }

        var adjustCompOffData = function(leaveDay) {
            if (!leaveDay.CompOff || leaveDay.CompOff.length == 0)
                leaveDay;

            if (leaveDay.CompOff.length == 1) {
                leaveDay.FirstHalfDayCompOffSelected = true;
            } else if ((leaveDay.CompOff.length == 2) && (leaveDay.CompOff[0] == leaveDay.CompOff[1].CompOffDate)) {
                leaveDay.FullDayCompOffSelected = true;
            } else if (leaveDay.CompOff.length == 2) {
                leaveDay.FirstHalfDayCompOffSelected = true;
                leaveDay.SecondHalfDayCompOffSelected = true;
            }

            if (leaveDay.CompOff.length > 1) {
                if (leaveDay.CompOff[0].CompOffDate != leaveDay.CompOff[1].CompOffDate) {
                    leaveDay.SplitCompOffIsAvailable = true;
                } else {
                    leaveDay.FullDayCompOffIsAvailable = true;
                }
            } else if (leaveDay.CompOff.length == 1) {
                leaveDay.HalfDayCompOffIsAvailable = true;
            }
            return leaveDay;
        };

        var getValidDays = function(fromDate, toDate, leaveType) {
            var deferred = $q.defer();
            var url = '/api/validDays?selectedUserId=' +
                appDataStoreService.getEffectiveUserId() +
                '&startDate=' + dateService.getDateString(fromDate) + "&endDate=" + dateService.getDateString(toDate) +
                '&leaveType=' + leaveType;

            webServiceInvoker.performGet(url, 'Planning day data is not available due to internal error')
                .then(function(response) {
                        deferred.resolve(formatValidDays(response, leaveType));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var formatValidDays = function(response, leaveType) {
            var validDays = response;

            //initiate date field and create an array for leaves, as there is a possibility that more than one type of leave can be availed on a day
            for (var i = 0; i < validDays.length; i++) {
                validDays[i].ApplyDate = dateService.getDateObjectFromDateString(validDays[i].ApplyDate);
                if (validDays[i].LeaveType != null) {
                    validDays[i].Leaves = [];
                    validDays[i].Leaves.push({
                        leaveType: validDays[i].LeaveType,
                        leaveStatus: validDays[i].LeaveStatus,
                        leaveHours: validDays[i].LeaveHours
                    })
                } else {
                    validDays[i].Leaves = null;
                }
            }

            //sort valid leave days in ascending order of date
            validDays.sort(function(firstLeaveDay, secondLeaveDay) {
                return firstLeaveDay.ApplyDate.getTime() > secondLeaveDay.ApplyDate.getTime() ? 1 : firstLeaveDay.ApplyDate.getTime() < secondLeaveDay.ApplyDate.getTime() ? -1 : 0;
            });

            //merge dates if more than one type of leave is availed on a given day
            var numberOfvalidDays = validDays.length;
            for (var i = 0; i < numberOfvalidDays - 1; i++) {
                for (var j = i + 1; j < numberOfvalidDays; j++) {
                    if ((validDays[i] != null && validDays[j] != null) && (validDays[i].ApplyDate.getTime() == validDays[j].ApplyDate.getTime())) {
                        if (validDays[i].Leaves == null && validDays[j].Leaves != null) {
                            validDays[i].Leaves = validDays[j].Leaves;
                            validDays[i].LeaveType = validDays[j].LeaveType;
                            validDays[i].LeaveStatus = validDays[j].LeaveStatus;
                            validDays[i].LeaveHours = validDays[j].LeaveHours;
                        } else if (validDays[i].Leaves != null && validDays[j].Leaves != null) {
                            validDays[i].Leaves = validDays[i].Leaves.concat(validDays[j].Leaves);
                            validDays[i].LeaveType = validDays[i].LeaveType + ', ' + validDays[j].LeaveType;
                            validDays[i].LeaveStatus = validDays[i].LeaveStatus + ', ' + validDays[j].LeaveStatus;
                            validDays[i].LeaveHours = validDays[i].LeaveHours + validDays[j].LeaveHours;
                        }
                        validDays.splice(j, 1);
                    } else {
                        break;
                    }
                }
            }

            validDays.forEach(function(day) {
                day.Apply = 0;

                var datePart = day.ApplyDate.getDay();
                if (datePart == 6) {
                    day.Holiday = true;
                    day.HolidayName = "Saturday";
                }
                if (datePart == 0) {
                    day.Holiday = true;
                    day.HolidayName = "Sunday";
                }

                day.IsFullDayApplied = (day.LeaveHours + day.CompOffHours) >= 8 ? true : false;
                day.IsHalfDayApplied = (day.LeaveHours + day.CompOffHours) == 4 ? true : false;

                if (day.Holiday) {
                    day.Comments = 'Holiday - ' + day.HolidayName;
                }

                if (day.LeaveHours >= 8) {
                    day.Comments = 'Full Day Leave : (' + day.LeaveType + ')';
                } else if (day.LeaveHours == 4) {
                    day.Comments = 'Half Day Leave : (' + day.LeaveType + ')';
                }

                if (day.CompOffHours >= 8) {
                    var comment = '';
                    if (day.Comments != null)
                        comment = day.Comments + ' - ';
                    day.Comments = comment + 'Full Day Comp-off';
                } else if (day.CompOffHours == 4) {
                    var comment = '';
                    if (day.Comments != null)
                        comment = day.Comments + ' - ';
                    day.Comments = comment + 'Half Day Comp-off';
                }

                day.showThreeStateApply = false;
                day.showTwoStateApply = false;

                if (leaveType == 'Annual') {
                    day.showThreeStateApply = day.Holiday != true && day.IsHalfDayApplied != true && day.IsFullDayApplied != true;
                    day.showTwoStateApply = day.Holiday != true && day.IsHalfDayApplied === true;
                } else if (leaveType == 'LossOfPay') {
                    day.showThreeStateApply = day.IsHalfDayApplied != true && day.IsFullDayApplied != true;
                    day.showTwoStateApply = day.IsHalfDayApplied === true;

                    if (day.ApplyForLeave == null)
                        day.ApplyForLeave = day.IsFullDayApplied ? 0 : (day.IsHalfDayApplied ? 1 : 2);

                    if (day.Holiday) {
                        day.isDisabled = true;
                    } else
                        day.isDisabled = false;
                }

            });
            return validDays;

        }

        var lookupCompOffs = function() {
            var deferred = $q.defer();
            webServiceInvoker.performGet('/api/compOffs', 'Compoff data is not available due to internal error')
                .then(function(response) {
                        deferred.resolve(formatCompOffs(response));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        }

        var formatCompOffs = function(response) {
          var compOffs = response;

          //initiate date field
          for (var i = 0; i < compOffs.length; i++) {
              compOffs[i].CompOffDate = dateService.getDateObjectFromDateString(compOffs[i].CompOffDate);
              compOffs[i].ExpiresOn = dateService.getDateObjectFromDateString(compOffs[i].ExpiresOn);
          }

          //sort compOffs in ascending order of date
          compOffs.sort(function(firstCompOff, secondCompOff) {
              return firstCompOff.ExpiresOn.getTime() > secondCompOff.ExpiresOn.getTime() ? 1 : firstCompOff.ExpiresOn.getTime() < secondCompOff.ExpiresOn.getTime() ? -1 : 0;
          });

          return compOffs;
        }

        return {
            getValidDays: getValidDays,
            adjustDateRows: adjustDateRows,
            lookupCompOffs: lookupCompOffs,
        };

    };

    angular.module('lms').factory("leaveLookupService", ['$q', '$log', 'appDataStoreService', 'webServiceInvoker', 'dateService', leaveLookupService]);


})();

(function() {

    'use strict';

    var leaveService = function($q, $log, webServiceInvoker, dateService) {

        var cancelLeave = function(leaveRequestId) {
            var deferred = $q.defer();

            webServiceInvoker.performPost('/api/cancelLeave', {
                    leaveRequestId: leaveRequestId,
                })
                .then(function(data) {
                    deferred.resolve('');
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var getLeaveHistoryForEmployee = function() {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/employeeLeaveHistory', 'Leave History is not available due to internal error')
                .then(function(data) {
                        deferred.resolve(formatLeaveHistoryResponse(data));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };


        var getLeaveRequestsForEmployeeOnADay = function(dateToCheck) {
              var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveRequestsOnADay?dateToCheck='+dateToCheck, 'Leave History is not available due to internal error')
                .then(function(data) {
                        deferred.resolve(data);
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var formatLeaveHistoryResponse = function(data) {
            var leaveRequests = data;
            leaveRequests.forEach(function(leaveRequest) {
                leaveRequest.FromDate = dateService.getSortableDateFormat(leaveRequest.FromDate);
                leaveRequest.ToDate = dateService.getSortableDateFormat(leaveRequest.ToDate);
                leaveRequest.AppliedOn = dateService.getSortableDateFormat(leaveRequest.AppliedOn);

                leaveRequest.LeaveDays = leaveRequest.TotalLeaveHours / 8;
                leaveRequest.CompOffDays = leaveRequest.CompOffHours / 8;
                if (leaveRequest.CompOffDays == 0)
                    leaveRequest.CompOffDays = null;
            });

            return leaveRequests;
        };

        var formatLeaveDetailsResponse = function(data) {
            var leaveDetails = data;
            if (leaveDetails) {
                leaveDetails.appliedOn = dateService.getSortableDateFormat(leaveDetails.appliedOn);
                leaveDetails.canCancel = leaveDetails.leaveStatus == "Applied" ? true : false;
                if (leaveDetails.leaveDays && leaveDetails.leaveDays.length > 0) {

                    leaveDetails.leaveDays.forEach(function(leaveDay) {
                        leaveDay.leaveDate = dateService.getSortableDateFormat(leaveDay.leaveDate);
                        leaveDay.leavePart = (leaveDay.leaveHours >= 8) ? "Full Day" : "Half Day";

                        if (leaveDay.minCompOffDate || leaveDay.maxCompOffDate) {
                            if (leaveDay.minCompOffDate == leaveDay.maxCompOffDate) {
                                leaveDay.compOffDates = [dateService.getSortableDateFormat(leaveDay.minCompOffDate)];
                            } else {
                                leaveDay.compOffDates = [dateService.getSortableDateFormat(leaveDay.minCompOffDate), dateService.getSortableDateFormat(leaveDay.maxCompOffDate)];
                            }
                            leaveDay.compOffPart = (leaveDay.compOffHours >= 8) ? "Full Day" : "Half Day";
                        }
                    });
                }
            }

            return leaveDetails;
        };

        var getLeaveDetails = function(leaveRequestId) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveDetails?leaveRequestId=' + leaveRequestId, 'Leave details are not available due to internal error')
                .then(function(data) {
                        deferred.resolve(formatLeaveDetailsResponse(data));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        var formatLeaveStatusHistory = function(data) {
            var leaveStatusHistory = data;
            if (leaveStatusHistory) {
                if (leaveStatusHistory.statusEntries && leaveStatusHistory.statusEntries.length > 0) {

                    leaveStatusHistory.statusEntries.forEach(function(statusEntry) {
                        statusEntry.statusDate = dateService.getSortableDateFormat(statusEntry.statusDate);
                    });
                }
            }

            return leaveStatusHistory;
        };

        var getLeaveStatusHistory = function(leaveRequestId) {
            var deferred = $q.defer();

            webServiceInvoker.performGet('/api/leaveStatusHistory?leaveRequestId=' + leaveRequestId, 'Leave status history is not available due to internal error')
                .then(function(data) {
                        deferred.resolve(formatLeaveStatusHistory(data));
                    },
                    function(err) {
                        deferred.reject(err);
                    });

            return deferred.promise;
        };

        return {
            cancelLeave: cancelLeave,
            getLeaveHistoryForEmployee: getLeaveHistoryForEmployee,
            getLeaveDetails: getLeaveDetails,
            getLeaveStatusHistory: getLeaveStatusHistory,
            getLeaveRequestsForEmployeeOnADay : getLeaveRequestsForEmployeeOnADay
        };

    };

    angular.module('lms').factory("leaveService", ['$q', '$log', 'webServiceInvoker', 'dateService', leaveService]);

})();

(function() {
    'use strict';

    var listService = function($q, $log) {

      var findElementById = function(inputArray, expectedId) {
          var matchedElement = null;

          inputArray.forEach(function(arrayElement, index, arr) {
              if (arrayElement.id == expectedId)
                  matchedElement = arrayElement;
          });
          return matchedElement;
      };

        return {
            findElementById: findElementById
        };

    };

    angular.module('lms').factory('listService', ['$q', '$log', listService]);
})();

(function() {

    'use strict';

    var locationService = function($q, $log, webServiceInvoker) {
      
        var getLocationList = function() {
            var deferred = $q.defer();
            webServiceInvoker.performGet('/api/locationList', 'Location list is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(data);
                }, function(err) {
                    deferred.reject(err);
                });
            return deferred.promise;
        }

        var getLocationDropdownList = function() {
            var deferred = $q.defer();
            getLocationList().then(function(response) {
                var locationOptions = [];

                locationOptions.push({
                    id: 0,
                    Name: "All",
                    Description: "All"
                })

                response.forEach(function(location) {
                    locationOptions.push(location);
                });

                deferred.resolve(locationOptions);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        return {
            getLocationList: getLocationList,
            getLocationDropdownList: getLocationDropdownList
        };

    };

    angular.module('lms').factory("locationService", ['$q', '$log', 'webServiceInvoker', locationService]);

})();

(function() {

    'use strict';

    var loginService = function($q, $log, $http, webServiceInvoker) {

        var performFormBasedLogin = function(username, password) {
            var deferred = $q.defer();

            $http.post('/api/login', {
                username: username,
                password: password
            }).then(function(response) {
                deferred.resolve(response.data);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        };

        return {
            performFormBasedLogin: performFormBasedLogin
        };

    };

    angular.module('lms').factory("loginService", ['$q', '$log', '$http', 'webServiceInvoker', loginService]);

})();

(function() {
    'use strict';
    var referenceDataService = function($q, webServiceInvoker, appDataStoreService) {

        var getleaveTypesWithBalance = function() {
            var deferred = $q.defer();

            var selectedUserId = appDataStoreService.getEffectiveUserId();
            webServiceInvoker.performGet('/api/leaveTypesWithBalance?selectedUserId=' + selectedUserId, 'Leave Type list is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(data);
                }, function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        };

        var getLeaveTypes = function() {
            var deferred = $q.defer();
            webServiceInvoker.performGet('/api/leaveTypeList', 'Leave Type list is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(data);
                }, function(err) {
                    deferred.reject(err);
                });
            return deferred.promise;
        }

        var getLeaveCreditTypes = function() {
            var deferred = $q.defer();
            webServiceInvoker.performGet('/api/leaveCreditTypeList', 'Leave Credit Type list is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(data);
                }, function(err) {
                    deferred.reject(err);
                });
            return deferred.promise;
        }

        var getEmployeeStatuses = function() {
            var deferred = $q.defer();
            webServiceInvoker.performGet('/api/employeeStatusList', 'Employee Status list is not available due to internal error')
                .then(function(data) {
                    deferred.resolve(data);
                }, function(err) {
                    deferred.reject(err);
                });
            return deferred.promise;
        }

        return {
            getleaveTypesWithBalance : getleaveTypesWithBalance,
            getLeaveTypes : getLeaveTypes,
            getLeaveCreditTypes : getLeaveCreditTypes,
            getEmployeeStatuses : getEmployeeStatuses
        };
    };

    angular.module('lms').factory("referenceDataService", ['$q', 'webServiceInvoker', 'appDataStoreService', referenceDataService]);
})();

(function() {

    'use strict';

    var utilService = function($location, $route, authService) {

        var getUrlWithToken = function(baseUrl) {
            var token = authService.getAuthToken();
            var url = baseUrl + '?token=' + token;
            return url;
        };

        var getUrlWithParams = function(baseUrl, selectedUserId) {
            var userInfo = authService.getUserInfo();
            var url = getUrlWithToken(baseUrl);

            if (userInfo && userInfo.IsAdmin && selectedUserId) {
                url = url + '&selectedUserId=' + selectedUserId;
            }

            return url;
        };

        var refreshPage = function(targetUrl) {
            if ($location.path() == targetUrl)
                $route.reload();
            else
                $location.path(targetUrl);
        };

        var isSpiderOrWipfliEmail = function (email) {
          if (email == null) {
             str = '';
          } else {
             email = email.toLowerCase();
          }
          return (endsWith(email, '@spiderlogic.com') || endsWith(email,'@wipfli.com')) ;
        }

        var endsWith = function (str, ends) {
          ends = '' + ends;
          var position = str.length - ends.length;
          return position >= 0 && str.indexOf(ends, position) === position;
        }

        return {
            getUrlWithParams: getUrlWithParams,
            refreshPage: refreshPage,
            isSpiderOrWipfliEmail: isSpiderOrWipfliEmail
        };

    };

    angular.module('lms').factory("utilService", ['$location', '$route', 'authService', utilService]);

})();

(function() {

    'use strict';

    var webServiceInvoker = function($http, $q, $log, appDataStoreService, authService) {

        var getUrlforGet = function(baseUrl) {
            var url = baseUrl;
            if (url == null)
                return null;

            var loggedInUserId = appDataStoreService.getLoggedInUserId();
            if (loggedInUserId == null)
                return null;

            var urlContainsParameters = url.indexOf('?') > -1;
            if (urlContainsParameters) {
                url = url + '&';
            } else {
                url = url + '?';
            }

            url = url + 'loggedInUserId=' + loggedInUserId;
            url = url + '&impersonatedUserId=' + getValueOrDefault(appDataStoreService.getCurrentImpersonatedUserId());
            url = url + '&selectedLocationId=' + getValueOrDefault(appDataStoreService.getUsersSelectedLocation());
            return url;
        }

        var getValueOrDefault = function(inputValue) {
            if (!inputValue || inputValue == null) {
                inputValue = -1;
            }

            return inputValue;
        }

        var performGet = function(baseUrl, errorMessage) {
            var deferred = $q.defer();

            var url = getUrlforGet(baseUrl);
            if (url == null)
                deferred.reject('Invalid user, or user is not logged in');
            else {
                var authToken = authService.getAuthToken();

                var request = {
                    method: 'GET',
                    url: url,
                    headers: {
                        'Authorization': 'Bearer ' + authToken
                    },
                }

                $http(request).
                then(function(response) {
                    if (response.data.error) {
                        deferred.reject(errorMessage);
                    } else {
                        deferred.resolve(response.data.data);
                    }
                }, function(err) {
                    deferred.reject(err);
                });
            }

            return deferred.promise;
        };

        var performPost = function(baseUrl, postData) {
            var deferred = $q.defer();

            var loggedInUserId = appDataStoreService.getLoggedInUserId();
            if (loggedInUserId == null)
                deferred.reject('User information is not found. Cannot proceed with the post operation.');

            postData.loggedInUserId = loggedInUserId;
            postData.impersonatedUserId = getValueOrDefault(appDataStoreService.getCurrentImpersonatedUserId());
            postData.selectedLocationId = getValueOrDefault(appDataStoreService.getUsersSelectedLocation());

            if (baseUrl == null)
                deferred.reject('Invalid url provided for post.');
            else {
                var authToken = authService.getAuthToken();
                var request = {
                    method: 'POST',
                    url: baseUrl,
                    headers: {
                        'Authorization': 'Bearer ' + authToken
                    },
                    data: postData
                }

                $http(request).
                then(function(response) {
                    if (response.data.error) {
                        deferred.reject(response.data.error);
                    } else {
                        deferred.resolve('');
                    }
                }, function(err) {
                    deferred.reject(err);
                });
            }

            return deferred.promise;
        };
        return {
            performGet: performGet,
            performPost: performPost
        };

    };

    angular.module('lms').factory("webServiceInvoker", ['$http', '$q', '$log', 'appDataStoreService', 'authService', webServiceInvoker]);

})();

(function() {
    'use strict';

    var controllerId = 'adminApprovalsCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$route', '$log', '$q', '$mdDialog', '$mdMedia', 'adminApprovalsService', 'utilService', 'dateService', 'dialogService', getApprovals]);

    function getApprovals($scope, $rootScope, $route, $log, $q, $mdDialog, $mdMedia, adminApprovalsService, utilService, dateService, dialogService) {

        var activate = function() {
            $scope.selectedCompOffs = [];
            $scope.selectedLeaves = [];
            populateGrids();
            selectActiveTab();
        }

        var selectActiveTab = function() {
            var landingPage = $route.current.$$route.landingPage;
            var leaveTabActive = false,
                compOffTabActive = false;

            switch (landingPage) {
                case "leave":
                    leaveTabActive = true;
                    break;
                case "compOff":
                    compOffTabActive = true;
                    break;
            }

            $scope.leaveTabActive = leaveTabActive;
            $scope.compOffTabActive = compOffTabActive;
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            if (!err)
                $scope.errorMessage = "An Internal error occurred";
            else {
                $scope.errorMessage = err;
            }
        }

        var populateGrids = function() {
            populateCompOffGrid();
            populateLeaveGrid();
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            populateGrids();
        });

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);

        var populateCompOffGrid = function() {
            adminApprovalsService.getCompOffsToApprove()
                .then(function(response) {
                    $scope.compOffList = response;
                }, function(err) {
                    handleError(err);
                });
        }

        var populateLeaveGrid = function() {
            adminApprovalsService.getLeavesToApprove()
                .then(function(response) {
                    $scope.leaveList = response;
                }, function(err) {
                    handleError(err);
                });
        }

        var isDataSelected = function(requestType) {
            var isRowsSelected = false;
            switch (requestType) {
                case 'Leave':
                    isRowsSelected = $scope.selectedLeaves.length > 0;
                    break;
                case 'CompOff':
                    isRowsSelected = $scope.selectedCompOffs.length > 0;
                    break;
            }
            return isRowsSelected;
        }

        $scope.doRequestStatusChange = function(event, requestType, newStatus) {
            $scope.error = false;
            $scope.requestType = requestType;
            $scope.newStatus = newStatus;
            $scope.details = {};
            $scope.details.comments = null;
            if (isDataSelected(requestType)) {
                confirmAction(event)
                    .then(function(response) {
                        if (response === true) {
                            adminApprovalsService.submitRequestStatusChange(requestType, newStatus, $scope.selectedLeaves, $scope.selectedCompOffs, $scope.details.comments)
                                .then(function(response) {
                                    $scope.error = false;
                                    reloadAppropriateTab(requestType);
                                }, function(err) {
                                    dialogService.showAlert(err);
                                    handleError(err);
                                });
                        }
                    });
            } else {
                dialogService.showAlert('Please select at least one row to proceed.');
            }
        }

        var confirmActionValidate = function(answer) {
            if ($scope.newStatus == 'Canceled' || $scope.newStatus == 'Rejected') {
                if ((answer === "Ok") && (!($scope.details.comments))) {
                    dialogService.showAlert('Comments are mandatory for Cancel and Reject.');
                    return false;
                }
            }

            return true;
        }

        var confirmAction = function(event) {
            var deferred = $q.defer();
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                    controller: dialogController,
                    templateUrl: 'app/components/dialog/adminApprovalDialog.tmpl.html',
                    scope: $scope,
                    preserveScope: true,
                    parent: angular.element(document.body),
                    targetEvent: event,
                    clickOutsideToClose: true,
                    fullscreen: useFullScreen,
                    locals: {
                        validate: confirmActionValidate
                    }
                })
                .then(function(answer) {
                    if (answer === "Ok") {
                        deferred.resolve(true);
                    } else {
                        deferred.reject(false);
                    }
                }, function() {
                    deferred.reject(false);
                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
            return deferred.promise;
        }

        $scope.displayDateFormat = dateService.getUIDateFormat();

        $scope.viewLeaveDetails = function(leaveRequestId) {
            var nextPage = "#/leaveDetails/" + leaveRequestId
            window.location = nextPage;
        };

        var reloadAppropriateTab = function(requestType) {
            switch (requestType) {
                case 'Leave':
                    utilService.refreshPage('/adminApprovals');
                    break;
                case 'CompOff':
                    utilService.refreshPage('/adminCompOffApprovals')
                    break;
            }
        }

        activate();
    }
})();

(function () {
    'use strict';

    var controllerId = 'adminAttendanceCtrl';

    angular.module('lms')
        .filter("giveSpaceBetweenTwoWord", function () {
            return function (input) {
                if (input != null && input != undefined) {
                    return input.replace(/([A-Z])/g, ' $1').trim()
                }
                return '';
            }
        })
        .controller(controllerId, ['$scope', '$log', 'attendanceService', 'attendanceStatusService', 'employeeService', 'authService', 'appDataStoreService', 'dateService', 'referenceDataService', adminAttendanceCtrl]);

    function adminAttendanceCtrl($scope, $log, attendanceService, attendanceStatusService, employeeService, authService, appDataStoreService, dateService, referenceDataService) {

        this.show = false;
        this.selectedUser = null;
        this.selectDate = "Select Date";
        this.isAdmin = authService.isAdmin();
        this.attendanceStatusNames = undefined;

        var thisObj = this;
        attendanceStatusService.getAttendnaceStatusName().then(function (response) {
            if (response.length > 0) {
                // response.splice(0, 0, { 'ID': 0, 'Name': 'Select' });
                thisObj.attendanceStatusNames = response;
            } return response;
        }, handleError);

        this.employeeAttendnace = {
            fistHalf: undefined,
            secondHalf: undefined,
            comments: undefined
        }


        this.isDateSet = function () {
            return this.selectDate != null && this.selectDate != '' && this.selectDate != 'Select Date';
        }

        function handleError(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred -" + JSON.stringify(err);
        }

        $scope.searchEmployee = function (val) {
            this.selectedUser = null;
            return employeeService.searchEmployee(val).then(function (response) {
                // As per requirement excluding logged-in user from searchEmployee result. So he cannot update his attendnace.
                return authService.exculdeLoggedInEmp(response);
            }, handleError);
        };

        var setupDateRanges = function () {
            var today = new Date();

            $scope.from.minDate.setDate(today.getDate() + -90);
            $scope.from.maxDate.setDate(today.getDate());
        }

        var initializeDateOptions = function () {
            $scope.from = {
                minDate: new Date(),
                maxDate: new Date(),
                opened: false
            };

            $scope.dateOptions = {
                formatYear: 'yyyy',
                startingDay: 1
            };

            $scope.openFrom = function ($event) {
                $scope.from.opened = true;
            };

        }

        $scope.dateFormat = dateService.getUIDateFormat();

        var activate = function () {
            initializeDateOptions();
            setupDateRanges();
        }

        activate();
    };

})();
(function() {
    'use strict';

    var controllerId = 'adminLeaveCreditCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'leaveCreditService', 'utilService', 'dateService', adminLeaveCreditCtrl]);

    function adminLeaveCreditCtrl($scope, $rootScope, $log, leaveCreditService, utilService, dateService) {
        var activate = function() {
            PopulateGrids();
            $scope.numberOfLeavesCredited = 1.75;
            $scope.forceInsert = false;
            $scope.doNotRepeatForEmployee = true;
            $scope.selectedMonth = -1;
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = err ? err : "An Internal error occurred";
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            PopulateGrids();
        });

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);

        var PopulateGrids = function() {
            PopulateLeaveCreditGrid();
        }

        var PopulateLeaveCreditGrid = function() {
            leaveCreditService.getAllLeaveCredits()
                .then(function(response) {
                    $scope.leaveCreditList = response;
                }, function(err) {
                    handleError(err);
                });
        }

        $scope.doIndividualLeaveCredit = function() {
            var nextPage = "#/leaveCreditEntry/0"
            window.location = nextPage;
        }

        $scope.doMonthlyLeaveCredit = function() {
            return leaveCreditService.performMonthlyLeaveCredit(
                    $scope.numberOfLeavesCredited,
                    $scope.forceInsert,
                    $scope.doNotRepeatForEmployee
                )
                .then(function(response) {
                    $scope.error = false;
                    utilService.refreshPage('/adminLeaveCredit');
                }, handleError);
        };

        $scope.deleteMonthlyLeaveCredit = function(options) {
            if (window.confirm("Do you really want to delete?")) {
                return leaveCreditService.deleteMonthlyLeaveCredit($scope.selectedMonth)
                    .then(function(response) {
                        $scope.error = false;
                        utilService.refreshPage('/adminLeaveCredit');
                    }, handleError);
            }
        }

        $scope.EditLeaveCredit = function(leaveCredit) {
            var nextPage = "#/leaveCreditEntry/" + leaveCredit.id
            window.location = nextPage;
        };

        $scope.displayDateFormat = dateService.getUIDateFormat();

        activate();
    }
})();

(function() {
  'use strict';

  // Controller name is handy for logging
  var controllerId = 'adminLeaveCreditEntryCtrl';

  // Define the controller on the module.
  // Inject the dependencies.
  // Point to the controller definition function.
  angular.module('lms').controller(controllerId, ['$scope', '$routeParams', '$log', 'referenceDataService', 'leaveCreditService', 'appDataStoreService', 'employeeService', 'dialogService', adminLeaveCreditEntryCtrl]);

  function adminLeaveCreditEntryCtrl($scope, $routeParams, $log, referenceDataService, leaveCreditService, appDataStoreService, employeeService, dialogService) {

    var activate = function() {
      setDropDowns();
      loadLeaveCreditDetails();
    };

    var setDropDowns = function() {
      setLeaveCreditTypeDropDownData();
      setLeaveTypeDropDownData();
    }

    var loadLeaveCreditDetails = function() {
      if ($routeParams.leaveCreditId <= 0) {
        $scope.newLeaveCredit = true;
        $scope.leaveCredit = {};
        loadEffectiveUserDetails();
      } else {
        $scope.newLeaveCredit = false;
        leaveCreditService.getLeaveCredit($routeParams.leaveCreditId)
          .then(function(response) {
            $scope.leaveCredit = response;
          }, handleError);
      }
    }

    var loadEffectiveUserDetails = function() {

      var effectiveUserId = appDataStoreService.getEffectiveUserId();
      employeeService.getEmployee(effectiveUserId)
        .then(function(response) {
          $scope.leaveCredit.Employee = response;
          $scope.leaveCredit.EmployeeId = response.id;
        }, handleError);
    }

    var handleError = function(err) {
      $scope.error = true;
      $scope.errorMessage = "An Internal error occurred -" + JSON.stringify(err);
    }

    var redirectToListingPage = function() {
      var nextPage = "#/adminLeaveCredit"
      window.location = nextPage;
    }

    $scope.updateLeaveCredit = function() {
      $scope.error = false;

      var validationErrors = validateLeaveCredit($scope.leaveCredit);
      if (validationErrors) {
        $scope.error = true;
        $scope.errorMessage = "Validation errors -" + validationErrors;
        dialogService.showAlert("Validation errors -" + validationErrors);
        return false;
      } else {
        return leaveCreditService.updateLeaveCredit($scope.leaveCredit).then(function(response) {
          redirectToListingPage();
        }, handleError);
      }
    };

    $scope.addLeaveCredit = function() {
      $scope.error = false;

      var validationErrors = validateLeaveCredit($scope.leaveCredit);
      if (validationErrors) {
        $scope.error = true;
        $scope.errorMessage = "Validation errors -" + validationErrors;
        dialogService.showAlert("Validation errors -" + validationErrors);
        return false;
      } else {
        return leaveCreditService.addLeaveCredit($scope.leaveCredit).then(function(response) {
          redirectToListingPage();
        }, handleError);
      }
    };

    var validateLeaveCredit = function(leaveCredit) {
      var validationErrors = [];
      if (!leaveCredit) {
        validationErrors.push('Please enter mandatory fields');
      } else {
        if (!leaveCredit.LeaveType)
          validationErrors.push('LeaveType cannot be null');
        if (!leaveCredit.LeaveCreditType)
          validationErrors.push('LeaveCreditType cannot be null');
        if (!leaveCredit.CreditedLeave || isNaN(leaveCredit.CreditedLeave))
          validationErrors.push('Credited leave should be a valid decimal');
        if (!leaveCredit.Description)
          validationErrors.push('Description cannot be null');
      }

      if (validationErrors.length <= 0) {
        return null;
      } else {
        return validationErrors.toString();
      }
    }

    var setLeaveCreditTypeDropDownData = function() {
      referenceDataService.getLeaveCreditTypes()
        .then(function(response) {
          $scope.leaveCreditTypeOptions = response;
        }, function(err) {
          handleError(err);
        });
    };

    var setLeaveTypeDropDownData = function() {
      referenceDataService.getLeaveTypes()
        .then(function(response) {
          $scope.leaveTypeOptions = response;
        }, function(err) {
          handleError(err);
        });
    };

    activate();
  };

})();

(function() {
    'use strict';

    var controllerId = 'compOffExpiryReportCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'adminReportsService', 'dateService', 'calenderService',  'listService', compOffExpiryReportCtrl]);

    function compOffExpiryReportCtrl($scope, $rootScope, $log, adminReportsService, dateService, calenderService, listService) {

        function activate() {
          populateReports();
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred";
        }

        var populateReports = function() {
            populateCompOffExpiryReportGrid();
        }

        var populateCompOffExpiryReportGrid = function() {
            adminReportsService.getCompOffExpiryReport()
                .then(function(response) {
                    $scope.compOffExpiryReportList = response;
                }, handleError);
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            populateReports();
        });

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);

        $scope.exportcompOffExpiry = function() {
            alasql('SELECT EmployeeId, EmployeeName, Accrued, Available, Availed, Expired, Location INTO XLSX("export.xlsx",{headers:true}) FROM ?', [$scope.compOffExpiryReportList]);
        };

        $scope.getReport = function() {
          populateReports();
        }

        $scope.displayDateFormat = dateService.getUIDateFormat();

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'employeeExitReportCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'adminReportsService', 'dateService', 'calenderService',  'listService', employeeExitReportCtrl]);

    function employeeExitReportCtrl($scope, $rootScope, $log, adminReportsService, dateService, calenderService, listService) {

        function activate() {
            populateMonths();
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred";
        }

        var populateReports = function() {
            populateEmployeeExitReportGrid();
        }

        var populateEmployeeExitReportGrid = function() {
            adminReportsService.getEmployeeExitReport($scope.selectedFromMonth.id, $scope.selectedToMonth.id)
                .then(function(response) {
                    $scope.employeeExitReportList = response;
                }, handleError);
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            populateReports();
        });

        var populateMonths = function() {
            var monthDropDownDetails = calenderService.getMonthDropDownDetails();
            $scope.fromMonths = monthDropDownDetails.months;
            $scope.toMonths = angular.copy($scope.fromMonths);

            $scope.selectedFromMonth = listService.findElementById($scope.fromMonths, monthDropDownDetails.currentMonth);
            $scope.selectedToMonth = listService.findElementById($scope.toMonths, monthDropDownDetails.currentMonth);
            populateReports();
        }

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);

        $scope.export = function() {
            alasql('SELECT id, FirstName, LastName, DateOfExit, Location, AccruedDuringYear, UtilizedLeave, ClosingBalance INTO XLSX("export.xlsx",{headers:true}) FROM ?', [$scope.employeeExitReportList]);
        };

        $scope.getReport = function() {
          populateReports();
        }

        $scope.displayDateFormat = dateService.getUIDateFormat();

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'leaveBalanceReportCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'adminReportsService', 'dateService', 'calenderService',  'listService', leaveBalanceReportCtrl]);

    function leaveBalanceReportCtrl($scope, $rootScope, $log, adminReportsService, dateService, calenderService, listService) {

        function activate() {
          populateReports();
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred";
        }

        var populateReports = function() {
            populateLeaveBalanceReportGrid();
        }

        var populateLeaveBalanceReportGrid = function() {
            adminReportsService.getLeaveBalanceReport()
                .then(function(response) {
                    $scope.leaveBalanceReportList = response;
                }, handleError);
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            populateReports();
        });

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);

        $scope.exportLeaveSummary = function() {
            alasql('SELECT EmployeeId, FullName as Employee, AvailableBalance, LeaveType as [Leave Type] INTO XLSX("export.xlsx",{headers:true}) FROM ?', [$scope.leaveBalanceReportList]);
        };

        $scope.getReport = function() {
          populateReports();
        }

        $scope.displayDateFormat = dateService.getUIDateFormat();

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'leaveEncashmentReportCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'adminReportsService', 'dateService', 'calenderService',  'listService', leaveEncashmentReportCtrl]);

    function leaveEncashmentReportCtrl($scope, $rootScope, $log, adminReportsService, dateService, calenderService, listService) {

        function activate() {
          populateReports();
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred";
        }

        var populateReports = function() {
            populateLeaveEncashmentReportGrid();
        }

        var populateLeaveEncashmentReportGrid = function() {
            adminReportsService.getLeaveEncashmentReport()
                .then(function(response) {
                    $scope.leaveEncashmentReportList = response;
                }, handleError);
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            populateReports();
        });

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);

        $scope.exportLeaveEncashment = function() {
            alasql('SELECT EmployeeId, FullName as Employee, OpeningBalance, Accrued, TotalLeaves, Utilized, Balance, Expiring, Encashable, BalanceAfterEncashment INTO XLSX("export.xlsx",{headers:true}) FROM ?', [$scope.leaveEncashmentReportList]);
        };

        $scope.getReport = function() {
          populateReports();
        }

        $scope.displayDateFormat = dateService.getUIDateFormat();

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'leaveSummaryReportCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'adminReportsService', 'dateService', 'calenderService',  'listService', leaveSummaryReportCtrl]);

    function leaveSummaryReportCtrl($scope, $rootScope, $log, adminReportsService, dateService, calenderService, listService) {

        function activate() {
            populateMonths();
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred";
        }

        var populateReports = function() {
            populateLeaveSummaryReportGrid();
            populateCompOffSummaryReportGrid();
        }

        var populateLeaveSummaryReportGrid = function() {
            adminReportsService.getLeaveSummaryReport($scope.selectedFromMonth.id, $scope.selectedToMonth.id)
                .then(function(response) {
                    $scope.leaveSummaryReportList = response;
                }, handleError);
        }

        var populateCompOffSummaryReportGrid = function() {
            adminReportsService.getCompOffSummaryReport($scope.selectedFromMonth.id, $scope.selectedToMonth.id)
                .then(function(response) {
                    $scope.compOffSummaryReportList = response;
                }, handleError);
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            populateReports();
        });

        var populateMonths = function() {
            var monthDropDownDetails = calenderService.getMonthDropDownDetails();
            $scope.fromMonths = monthDropDownDetails.months;
            $scope.toMonths = angular.copy($scope.fromMonths);

            $scope.selectedFromMonth = listService.findElementById($scope.fromMonths, monthDropDownDetails.currentMonth);
            $scope.selectedToMonth = listService.findElementById($scope.toMonths, monthDropDownDetails.currentMonth);
            populateReports();
        }

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);

        $scope.exportLeaveSummary = function() {
            alasql('SELECT LeaveDate, LeaveDays, EmployeeId, Name, LeaveType, LeaveStatus INTO XLSX("export.xlsx",{headers:true}) FROM ?', [$scope.leaveSummaryReportList]);
        };

        $scope.exportCompOffSummary = function() {
            alasql('SELECT LeaveDate, LeaveDays, EmployeeId, Name, LeaveType, LeaveStatus INTO XLSX("export.xlsx",{headers:true}) FROM ?', [$scope.compOffSummaryReportList]);
        };

        $scope.getReport = function() {
          populateReports();
        }

        $scope.displayDateFormat = dateService.getUIDateFormat();

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'lossOfPayReportCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'adminReportsService', 'dateService', 'calenderService',  'listService', lossOfPayReportCtrl]);

    function lossOfPayReportCtrl($scope, $rootScope, $log, adminReportsService, dateService, calenderService, listService) {

        function activate() {
            populateMonths();
            populateYears();
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred";
        }

        var populateReports = function() {
            populateLossOfPayReportGrid();
        }

        var populateLossOfPayReportGrid = function() {
            adminReportsService.getLossOfPayReport($scope.selectedFromMonth.id, $scope.selectedToMonth.id, $scope.selectedYear.id)
                .then(function(response) {
                    $scope.lossOfPayReportList = response;
                }, handleError);
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            populateReports();
        });

        var populateMonths = function() {
            var monthDropDownDetails = calenderService.getMonthDropDownDetails();
            $scope.fromMonths = monthDropDownDetails.months;
            $scope.toMonths = angular.copy($scope.fromMonths);

            $scope.selectedFromMonth = listService.findElementById($scope.fromMonths, monthDropDownDetails.currentMonth);
            $scope.selectedToMonth = listService.findElementById($scope.toMonths, monthDropDownDetails.currentMonth);
            
        }

        var populateYears = function(){
            var yearDropDownDetails = calenderService.getYearDropDownDetails();
            $scope.years = yearDropDownDetails.years;

            $scope.selectedYear = listService.findElementById($scope.years, yearDropDownDetails.currentYear);
            populateReports();
        }

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);
        $scope.export = function() {
            alasql('SELECT EmployeeId, FirstName, LastName, FromDate, ToDate, TotalLeaveDays, ApproverName, Location INTO XLSX("export.xlsx",{headers:true}) FROM ?', [$scope.lossOfPayReportList]);
        };

        $scope.getReport = function() {
          populateReports();
        }

        $scope.displayDateFormat = dateService.getUIDateFormat();

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'newJoineeReportCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'adminReportsService', 'dateService', 'calenderService',  'listService', newJoineeReportCtrl]);

    function newJoineeReportCtrl($scope, $rootScope, $log, adminReportsService, dateService, calenderService, listService) {

        function activate() {
            populateMonths();
        }

        var handleError = function(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred";
        }

        var populateReports = function() {
            populateNewJoineeReportGrid();
        }

        var populateNewJoineeReportGrid = function() {
            adminReportsService.getNewJoineeReport($scope.selectedFromMonth.id, $scope.selectedToMonth.id)
                .then(function(response) {
                    $scope.newJoineeReporttList = response;
                }, handleError);
        }

        // hold on to rootscope's event listner as these aren't auto destroyed
        var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
            populateReports();
        });

        var populateMonths = function() {
            var monthDropDownDetails = calenderService.getMonthDropDownDetails();
            $scope.fromMonths = monthDropDownDetails.months;
            $scope.toMonths = angular.copy($scope.fromMonths);

            $scope.selectedFromMonth = listService.findElementById($scope.fromMonths, monthDropDownDetails.currentMonth);
            $scope.selectedToMonth = listService.findElementById($scope.toMonths, monthDropDownDetails.currentMonth);
            populateReports();
        }

        // clean up listener when scope is destroyed
        $scope.$on('$destroy', deregisterLocationUpdated);

        $scope.export = function() {
            alasql('SELECT id, FirstName, LastName, DateOfJoining, Location, AddedBy INTO XLSX("export.xlsx",{headers:true}) FROM ?', [$scope.newJoineeReporttList]);
        };

        $scope.getReport = function() {
          populateReports();
        }

        $scope.displayDateFormat = dateService.getUIDateFormat();

        activate();
    }
})();

(function() {
    'use strict';

    var controllerId = 'impersonateUserCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$route', '$log', 'employeeService', 'authService', 'appDataStoreService', impersonateUserCtrl]);

    function impersonateUserCtrl($scope, $rootScope, $route, $log, employeeService, authService, appDataStoreService) {

        function activate() {
            loadEmployeeDetails();
            $scope.showImpersonation = authService.isAdmin();
            if ($scope.showImpersonation) {
                loadImpersonatedEmployeeDetails();
            }
        };

        var loadImpersonatedEmployeeDetails = function() {
            $scope.impersonatedEmployee = appDataStoreService.getCurrentImpersonatedUserName();
        }

        function loadEmployeeDetails() {
            var loggedInUserId = appDataStoreService.getLoggedInUserId();
            if (loggedInUserId != null) {
                employeeService.getEmployee(loggedInUserId)
                    .then(function(response) {
                        $scope.employee = response;
                    }, handleError);
            }
        }

        function handleError(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred -" + JSON.stringify(err);
        }

        $scope.searchEmployee = function(val) {
            return employeeService.searchEmployee(val).then(function(response) {
                return response;
            }, handleError);
        };

        $rootScope.impersonateUser = function() {
            if ($scope.selectedUser == null) {
                $scope.error = true;
                $scope.errorMessage = "Please select an employee for impersonation in the Search Employee field.";
                return;
            }

            appDataStoreService.setImpersonatedUserId($scope.selectedUser.id);
            appDataStoreService.setImpersonatedUserLocation($scope.selectedUser.LocationId);
            appDataStoreService.setImpersonatedUserName($scope.selectedUser.FirstName + ' ' + $scope.selectedUser.LastName);
            $scope.$emit('impersonatedUser-updated', {});
            $route.reload();                
        };

        $rootScope.clearImpersonation = function() {
            appDataStoreService.setImpersonatedUserId(null);
            appDataStoreService.setImpersonatedUserName(null);
            appDataStoreService.setImpersonatedUserLocation(null);
            $scope.$emit('impersonatedUser-updated', {});
            $route.reload();
        };

        activate();
    };

})();

(function() {
    'use strict';

    var controllerId = 'impersonatedUserBannerCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$log', 'authService', 'appDataStoreService', impersonatedUserBannerCtrl]);

    function impersonatedUserBannerCtrl($scope, $log, authService, appDataStoreService) {

        function activate() {
            $scope.showImpersonation = authService.isAdmin();
            if ($scope.showImpersonation) {
                loadImpersonatedEmployeeDetails();
            }
        };

        var loadImpersonatedEmployeeDetails = function() {
            $scope.impersonatedEmployee = appDataStoreService.getCurrentImpersonatedUserName();
        }

        function handleError(err) {
            $scope.error = true;
            $log.debug(err);
            $scope.errorMessage = "An Internal error occurred -" + JSON.stringify(err);
        }

        activate();
    };

})();

(function() {
    'use strict';

    var controllerId = 'userEntryCtrl';

    angular.module('lms').controller(controllerId, ['$scope', '$routeParams', '$log', 'employeeService', 'referenceDataService', 'locationService', 'dateService', 'dialogService', userEntryCtrl]);

    function userEntryCtrl($scope, $routeParams, $log, employeeService, referenceDataService, locationService, dateService, dialogService) {

      $scope.emailFormat = /^[a-zA-Z][a-zA-Z0-9]+@[Ss][Pp][Ii][Dd][Ee][Rr][Ll][Oo][Gg][Ii][Cc]\.[Cc][Oo][Mm]$/;

        var activate = function() {
            setDropDowns();
            loadEmployeeDetails();
            setDateOptions();
        };

        var setDropDowns = function() {
            setLocationDropDownData();
            setStatusDropDownData();
        }

        var loadEmployeeDetails = function() {
            if ($routeParams.employeeId <= 0) {
                $scope.employee = {
                  OfficialEmailId: "@spiderlogic.com"
                };
                $scope.newEmployee = true;
                initGender();
            } else {
                $scope.newEmployee = false;
                employeeService.getEmployee($routeParams.employeeId)
                    .then(function(response) {
                        $scope.employee = response;
                        initGender();
                    }, handleError);
            }
        }

        var handleError = function(err) {
            $scope.error = true;
            $scope.errorMessage = "An Internal error occurred -" + JSON.stringify(err);
        }

        var redirectToAdmin = function() {
            var nextPage = "#/adminUsers"
            window.location = nextPage;
        }

        var formatEmployee = function(employee) {
            if (employee.DateOfBirth)
                employee.DateOfBirth = dateService.getDateString(employee.DateOfBirth);
            if (employee.DateOfJoining)
                employee.DateOfJoining = dateService.getDateString(employee.DateOfJoining);
            if (employee.DateOfExit)
                employee.DateOfExit = dateService.getDateString(employee.DateOfExit);
            return employee;
        }

        $scope.updateEmployee = function() {
            var employee = formatEmployee($scope.employee);
            var validationErrors = employeeService.validateEmployee(employee);

            if (validationErrors) {
                $scope.error = true;
                $scope.errorMessage = validationErrors;
                dialogService.showAlert(validationErrors);
                return false;
            } else {
                setGender();
                return employeeService.updateEmployee($scope.employee).then(function(response) {
                    redirectToAdmin();
                }, handleError);
            }
        };

        $scope.addEmployee = function() {
            var employee = formatEmployee($scope.employee);
            var validationErrors = employeeService.validateEmployee(employee);

            if (validationErrors) {
                $scope.error = true;
                $scope.errorMessage = validationErrors;
                dialogService.showAlert(validationErrors);
                return false;
            } else {
                setGender();
                return employeeService.addEmployee($scope.employee).then(function(response) {
                    redirectToAdmin();
                }, handleError);
            }
        };

        var setGender = function() {
            if ($scope.employee.Gender == "Male") {
                $scope.employee.IsFemale = false;
            } else {
                $scope.employee.IsFemale = true;
            }
        };

        var initGender = function() {
            if ($scope.employee != null && $scope.employee.IsFemale) {
                $scope.employee.Gender = "Female";
            } else {
                $scope.employee.Gender = "Male";
            }
        }

        var setLocationDropDownData = function() {
            locationService.getLocationList()
                .then(function(response) {
                    $scope.locationOptions = response;
                }, function(err) {
                    handleError(err);
                });
        };

        var setStatusDropDownData = function() {
            referenceDataService.getEmployeeStatuses()
                .then(function(response) {
                    $scope.statusOptions = response;
                }, function(err) {
                    handleError(err);
                });
        };

        $scope.openDatePopup = function($event) {
            switch ($event.currentTarget.id) {
                case 'dateOfBirthBtn':
                    $scope.dateOfBirth.opened = true;
                    break;
                case 'dateOfJoining':
                    $scope.dateOfJoining.opened = true;
                    break;
                case 'dateOfExit':
                    $scope.dateOfExit.opened = true;
                    break;
            }
        };

        var getDefaultDateOptions = function() {
            return {
                minDate: new Date(),
                maxDate: new Date(),
                opened: false
            };
        }

        var setDateOptions = function() {
            $scope.dateOptions = {
                formatYear: 'yyyy',
                startingDay: 1
            };

            $scope.dateOfBirth = getDefaultDateOptions();
            $scope.dateOfJoining = getDefaultDateOptions();
            $scope.dateOfExit = getDefaultDateOptions();
        };

        $scope.dateFormat = dateService.getUIDateFormat();

        activate();
    };

})();

(function() {
  'use strict';

  // Controller name is handy for logging
  var controllerId = 'userManagementCtrl';

  // Define the controller on the module.
  // Inject the dependencies.
  // Point to the controller definition function.
  angular.module('lms').controller(controllerId, ['$scope', '$rootScope', '$log', 'employeeService', 'dateService', userManagementCtrl]);

  function userManagementCtrl($scope, $rootScope, $log, employeeService, dateService) {

    function activate() {
      PopulateUserGrid();
    };

    // hold on to rootscope's event listner as these aren't auto destroyed
    var deregisterLocationUpdated = $rootScope.$on('location-updated', function(event, eventObj) {
        PopulateUserGrid();
    });

    // clean up listener when scope is destroyed
    $scope.$on('$destroy', deregisterLocationUpdated);

    function PopulateUserGrid() {
      employeeService.getEmployees()
        .then(function(response) {
          $scope.userList = response;
        }, function(err) {
          handleError(err);
        });
    }

    function handleError(err) {
      $scope.error = true;
      $scope.errorMessage = "An Internal error occurred -" + JSON.stringify(err);
    }

    $scope.EditEmployee = function(employee) {
      var nextPage = "#/userEntry/" + employee.id
      window.location = nextPage;
    };

    $scope.AddEmployee = function() {
      var nextPage = "#/userEntry/0"
      window.location = nextPage;
    };

    $scope.displayDateFormat = dateService.getUIDateFormat();

    activate();
  };

  angular.module("trNgGrid").directive("dropdownFilter", ["$filter", "$parse",'referenceDataService', function ($filter, $parse,referenceDataService) {
    var templateUrl = "app/admin/users/employeeStatusFilter.html";
    return {
        restrict: "E",
        require: "^trNgGrid",
        scope: true,
        templateUrl: templateUrl,
        link: function (scope, element, attr, table) {
            var field = scope.columnOptions.fieldName;
            var label = (angular.isDefined(scope.columnOptions.displayName) && scope.columnOptions.displayName !== null ? scope.columnOptions.displayName : field);
            referenceDataService.getEmployeeStatuses()
                .then(function(response) {
                    var collection =new Array();
                    for(var i=0;i<response.length;i++)
                    {
                      var item={};
                      item.predicateObject=response[i].Name;
                      collection.push(item);
                    }
                    scope.label = label;
                    scope.items = collection;
                }, function(err) {
                    handleError(err);
                });
          }
      };
  }]);
})();

function dialogController($scope, $mdDialog, validate) {

    var validateAnswer = validate;

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
        if (validateAnswer(answer)) {
            $mdDialog.hide(answer);
        }
    };
}

(function() {
    'use strict';
    var componentId = 'dashboardHolidayComponent';

    angular.module('lms').component(componentId, {
        templateUrl: 'app/components/dashboard/dashboardHoliday.html',
        bindings: {
            holiday: '<'
        },
        controller: function() {
            //var dayPart, datePart, yearPart;
            var activate = function() {
                //splitDateValue();
            }
/*
            var splitDateValue = function() {
                if (holiday.ActualDate) {
                    var dateParts = holiday.ActualDate.split(',');
                    if (dateParts && dateParts.length > 0) {
                        dayPart = dateParts[0];
                        datePart = dateParts[1];
                        yearPart = dateParts[2];
                    }
                }

            }
*/
            activate();
        }
    });

})();

(function() {
    'use strict';
    var componentId = 'dashboardRecentLeaveComponent';

    angular.module('lms').component(componentId, {
        templateUrl: 'app/components/dashboard/dashboardRecentLeave.html',
        bindings: {
            leave: '<'
        },
        controller: function() {
            var activate = function() {}

            activate();
        }
    });

})();

(function() {
    'use strict';
    var componentId = 'dashboardStatisticsComponent';

    angular.module('lms').component(componentId, {
        templateUrl: 'app/components/dashboard/dashboardStatistics.html',
        bindings: {
            statistics: '<'
        },
        controller: function() {
            var activate = function() {}

            activate();
        }
    });

})();

(function() {
    'use strict';
    var componentId = 'holidayComponent';

    angular.module('lms').component(componentId, {
        templateUrl: 'app/components/holidayList/holiday.html',
        bindings: {
            holiday: '<'
        },
        controller: function() {
            var activate = function() {
            }

            activate();
        }
    });

})();

(function() {
    'use strict';
    var componentId = 'leaveTypeKeywordComponent';

    angular.module('lms').component(componentId, {
        templateUrl: 'app/components/keywords/leaveTypeKeyword.html',
        bindings: {
            leavetype: '<'
        },
        controller: function() {
            var ctrl = this;
            var leaveTypeClass = null;

            var activate = function() {
                if (ctrl.leavetype) {
                    ctrl.leaveTypeClass = ctrl.leavetype.replace(/ /g, '');
                }
            }

            activate();
        }
    });

})();

(function() {
    'use strict';
    var componentId = 'statusKeywordComponent';

    angular.module('lms').component(componentId, {
        templateUrl: 'app/components/keywords/statusKeyword.html',
        bindings: {
            status: '<'
        },
        controller: function() {
            var activate = function() {
            }

            activate();
        }
    });

})();
